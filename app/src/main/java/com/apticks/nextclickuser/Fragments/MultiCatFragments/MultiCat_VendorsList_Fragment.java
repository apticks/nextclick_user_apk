package com.apticks.nextclickuser.Fragments.MultiCatFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.MultiCatVendorsAdapter;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.MultiCatVendorsPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MultiCat_VendorsList_Fragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link MultiCat_VendorsList_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MultiCat_VendorsList_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String sub_cat_id;
    private Context mContext;
    private View root;
    private RecyclerView multicatVendorsRecycler;
    private ArrayList<MultiCatVendorsPojo> multiCatVendorsList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MultiCat_VendorsList_Fragment() {
        // Required empty public constructor
    }
    public MultiCat_VendorsList_Fragment(String sub_cat_id) {
        // Required empty public constructor
        this.sub_cat_id = sub_cat_id;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MultiCat_VendorsList_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MultiCat_VendorsList_Fragment newInstance(String param1, String param2) {
        MultiCat_VendorsList_Fragment fragment = new MultiCat_VendorsList_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_multi_cat_vendors_list, container, false);
        mContext = getActivity();
        multicatVendorsRecycler = root.findViewById(R.id.multicat_vendors_recycler);

        vendorsFetcher(sub_cat_id);
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    private void vendorsFetcher(String subCatID){

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST+ "7/10/0/?sub_cat_id=" + subCatID/*"7/10/0/?sub_cat_id=47"*/, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if(response!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if(status && http_code == 200){
                            if(!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())){
                                JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                multiCatVendorsList = new ArrayList<>();
                                for(int i=0;i<resultArray.length();i++){
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    MultiCatVendorsPojo multiCatVendorsPojo = new MultiCatVendorsPojo();
                                    multiCatVendorsPojo.setId(resultObject.getString("id"));
                                    multiCatVendorsPojo.setName(resultObject.getString("name"));
                                    multiCatVendorsPojo.setEmail(resultObject.getString("email"));
                                    multiCatVendorsPojo.setUnique_id(resultObject.getString("unique_id"));
                                    multiCatVendorsPojo.setAddress(resultObject.getString("address"));
                                    multiCatVendorsPojo.setImage(resultObject.getString("image"));
                                    multiCatVendorsList.add(multiCatVendorsPojo);
                                }
                                MultiCatVendorsAdapter multiCatVendorsAdapter = new MultiCatVendorsAdapter(mContext,multiCatVendorsList);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                multicatVendorsRecycler.setLayoutManager(layoutManager);
                                multicatVendorsRecycler.setAdapter(multiCatVendorsAdapter);

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }































   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }






}
