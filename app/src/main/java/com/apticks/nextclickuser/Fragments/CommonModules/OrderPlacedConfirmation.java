package com.apticks.nextclickuser.Fragments.CommonModules;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;

import com.apticks.nextclickuser.R;

public class OrderPlacedConfirmation extends DialogFragment {

    public Button ok_placed;

    private Context mContext;
    private Dialog dialog;

    public OrderPlacedConfirmation(Context mContext){
        this.mContext = mContext;
        //alertDialogOrderPlaced();
    }

    public void alertDialogOrderPlaced() {
        dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_order_placed_confirmation);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(false);

        ok_placed = dialog.findViewById(R.id.ok_placed);

        initView();

        okDialogClicked();
        dialog.show();
    }

    private void okDialogClicked() {
        ok_placed .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                return;
            }
        });
    }

    boolean dialogDismiss() {
        dialog.dismiss();
        return true;
    }

    private void initView() {
        ok_placed = dialog.findViewById(R.id.ok_placed);
    }
}
