package com.apticks.nextclickuser.Fragments.WALLETFRAGMENTS;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.apticks.nextclickuser.Activities.USERWALLET.MyWalletActivity;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;


public class AddMoneyFragment extends Fragment {

    //user
    private static final String TAG_FIRSTNAME = "name";
    //private static final String TAG_USERNAME = "username";
    private static final String TAG_USERNAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mobile";

    private Button addmoney;
    private EditText amount;
    private String email;
    private TextView errorMessage;
    private String name;
    private String number;
    private final String paymentGateway="paytm";
    private String username;

    //Prefrance
    private static PreferenceManager preferenceManager;

    public AddMoneyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceManager = new PreferenceManager(getActivity());


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootViewone = inflater.inflate(R.layout.fragment_addmoney, container, false);
        //sharedPreferences = getSharedPreferences("sdsnsharedpreferences", Context.MODE_PRIVATE);
        amount = (EditText) rootViewone.findViewById(R.id.amountEditText);
        addmoney = (Button) rootViewone.findViewById(R.id.addButton);
        errorMessage = (TextView) rootViewone.findViewById(R.id.errorMessage);

        username = preferenceManager.getString(TAG_USERNAME);
        email = preferenceManager.getString(TAG_EMAIL);
        //name = sharedPreferences.getString(TAG_FIRSTNAME);
        number = preferenceManager.getString(TAG_MOBILE);
//        paymentGateway = preferenceManager.getString("paymentGateway", "paytm");

        addmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String obj = amount.getText().toString();
                if (!obj.isEmpty()) {
                    int amt = Integer.parseInt(obj);
                    if (amt < 1) {
                        errorMessage.setVisibility(View.VISIBLE);
                    } else if (amt > 1 || amt == 1) {
                        errorMessage.setVisibility(View.GONE);

//                        if (paymentGateway.equals("instamojo")) {
//                            ((MyWalletActivity) getActivity()).callInstamojoPay(email, number, obj, "Add Money to Wallet", name);
//                        }

                        if (paymentGateway.equals("paytm")) {
                            ((MyWalletActivity) getActivity()).PaytmAddMoney(email, number, obj, "Add Money to Wallet", username);
                        }
                    }
                } else if (obj.isEmpty()) {
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("Enter minimum Rs 20");
                    errorMessage.setTextColor(Color.parseColor("#ff0000"));
                }
            }
        });

        return rootViewone;
    }

}
