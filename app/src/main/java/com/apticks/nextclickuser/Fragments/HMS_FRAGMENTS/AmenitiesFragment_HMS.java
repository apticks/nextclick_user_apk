package com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AmenitiesFragment_HMS.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link AmenitiesFragment_HMS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AmenitiesFragment_HMS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String vendor_Id;
    private Context context;
    private View root;
    private ArrayList<String> amemnitiesList;
    private ListView amenitiesRecycler;

    private OnFragmentInteractionListener mListener;

    public AmenitiesFragment_HMS() {
        // Required empty public constructor
    }

    public AmenitiesFragment_HMS(String vendor_id) {
        // Required empty public constructor
        this.vendor_Id = vendor_id;

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AmenitiesFragment_HMS.
     */
    // TODO: Rename and change types and number of parameters
    public static AmenitiesFragment_HMS newInstance(String param1, String param2) {
        AmenitiesFragment_HMS fragment = new AmenitiesFragment_HMS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_amenities_hms, container, false);
        context = getActivity();
        amenitiesRecycler = root.findViewById(R.id.aminities_recycler_hms);
        singleVendorDataFetcher();
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/
    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void singleVendorDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_Id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        if(dataJson.get("amenities").getClass().toString().equalsIgnoreCase(jsonObject.getClass().toString())) {
                            JSONObject amenitiesObject = dataJson.getJSONObject("amenities");
                            Iterator x = amenitiesObject.keys();
                            JSONArray amenitiesArray = new JSONArray();

                            while (x.hasNext()) {
                                String key = (String) x.next();
                                amenitiesArray.put(amenitiesObject.get(key));
                            }
                            amemnitiesList = new ArrayList<>();
                            for (int i = 0; i < amenitiesArray.length(); i++) {

                                JSONObject amenityObject = amenitiesArray.getJSONObject(i);
                                amemnitiesList.add(amenityObject.getString("name"));
                            }


                            //amenitiesRecycler.setLayoutManager(layoutManager);

                            amenitiesRecycler.setAdapter(new ArrayAdapter<String>(context,R.layout.amenities_supporter,R.id.amenity_name,amemnitiesList));
                        }
                        //JSONArray amenitiesArray = dataJson.
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        });

        requestQueue.add(stringRequest);

    }

}
