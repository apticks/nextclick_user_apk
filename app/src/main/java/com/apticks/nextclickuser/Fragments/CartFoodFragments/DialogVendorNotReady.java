package com.apticks.nextclickuser.Fragments.CartFoodFragments;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.apticks.nextclickuser.R;

public class DialogVendorNotReady {
    private Dialog dialog;

    void alertDialogNotReady(Context mContext, View.OnClickListener onClickOK){
        dialog = new Dialog(mContext);

        dialog.setContentView(R.layout.vendor_not_ready);
        //dialog.onBackPressed();
        //dialog.getWindow();
        //dialog.
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        //dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        dialog.setCancelable(false);
        Button buttonOk = dialog.findViewById(R.id.ok_notReady);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                onClickOK.onClick(v);
            }
        });

        dialog.show();
    }
}
