package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.apticks.nextclickuser.R;

public class CheckOutCartFragment extends Fragment/*AppCompatActivity*/ {

    public CheckOutCartFragment(Context activity) {
    }

    /*@Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.fragment_check_out_cart);
    
        }*/
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_out_cart,container, false);
        //return super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }
}
