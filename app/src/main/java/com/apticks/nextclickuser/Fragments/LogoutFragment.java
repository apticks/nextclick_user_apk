package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.GroceryCategoryAdapter;
import com.apticks.nextclickuser.Pojo.FoodItemPojo;
import com.apticks.nextclickuser.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.GROCER_CATEGORIES;

public class LogoutFragment extends Fragment {
    Context mContext;
    RecyclerView recycleGroceryCategories, recycleGroceryCategoriesTop;
    AVLoadingIndicatorView loadingBarSingle;
    RelativeLayout relativeLoading;

    ArrayList<FoodItemPojo> foodItemData;
    ArrayList<FoodItemPojo> grocerryTopData;

    public LogoutFragment() {
    }

    public LogoutFragment(Context context) {
        this.mContext = context;
    }

    View root;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_logout, container, false);//Initializing View
        mContext = getActivity();
        initView();

        groceriesAllCategories();
        return root;



    }

    private void initView() {
        recycleGroceryCategories = root.findViewById(R.id.recycle_grocery_categories);
        recycleGroceryCategories.setNestedScrollingEnabled(false);
        recycleGroceryCategoriesTop = root.findViewById(R.id.recycle_grocery_categories_top);
        recycleGroceryCategoriesTop.setNestedScrollingEnabled(false);
        //Recycle View
        RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(mContext);
        recycleGroceryCategoriesTop.setLayoutManager(mlayoutmanager);

        //Loading
        loadingBarSingle = root.findViewById(R.id.loading_bar_single);

        //Relative Layout
        relativeLoading = root.findViewById(R.id.relative_loading);

        //recycleGroceryCategories.setLayoutManager(new GridLayoutManager(mContext, 2,GridLayoutManager.VERTICAL, false));
    }

    private void groceriesAllCategories() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GROCER_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, ""+response, Toast.LENGTH_SHORT).show();
                try {
                    foodItemData= new ArrayList<>();
                    grocerryTopData= new ArrayList<>();

                    //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray dataJson = jsonObject.getJSONArray("data");
                    try {

                        //JSONArray resultArray = dataJson.getJSONArray("result");
                        if (dataJson.length() > 0) {
                            //Toast.makeText(mContext, ""+dataJson.length(), Toast.LENGTH_SHORT).show();


                            for (int i = 0; i < dataJson.length(); i++) {
                                JSONObject resultObject = dataJson.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String desc = resultObject.getString("desc");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");

                                loadingBarSingle.setVisibility(View.GONE);
                                relativeLoading.setVisibility(View.GONE);
                                recycleGroceryCategories.setVisibility(View.VISIBLE);
                                recycleGroceryCategoriesTop.setVisibility(View.VISIBLE);

                                //String price = resultObject.getString("price");
/*
                                int discount=0;
                                int discountPrice =0;
                                if (resultObject.getInt("discount")!=0) {
                                    discount = resultObject.getInt("discount");
                                    discountPrice= resultObject.getInt("discount_price");
                                } else {

                                }*/
                                //Toast.makeText(mContext, ""+id, Toast.LENGTH_SHORT).show();

                                /*int itemType = resultObject.getInt("item_type");*/

                                if (i<2){
                                    FoodItemPojo foodItemPojo = new FoodItemPojo();
                                    foodItemPojo.setName(name);
                                    foodItemPojo.setDesc(desc);
                                    foodItemPojo.setImage(image);
                                    foodItemPojo.setId(id);
                                    grocerryTopData.add(foodItemPojo);
                                }else if (i>=2){
                                    FoodItemPojo foodItemPojo = new FoodItemPojo();

                                    foodItemPojo.setName(name);
                                    foodItemPojo.setDesc(desc);
                                    foodItemPojo.setImage(image);
                                    foodItemPojo.setId(id);
                                    //foodItemPojo.setPrice(price);
                                    //foodItemPojo.setItemType(itemType);
                                    //foodItemPojo.setDeliveryPrice(discountPrice);
                                    //foodItemPojo.setDiscount(discount);
                                    foodItemData.add(foodItemPojo);
                                }

                            }
                            //FoodItemsAdapter groceryCategoryAdapter = new FoodItemsAdapter(mContext, foodItemData);
                            GroceryCategoryAdapter groceryCategoryAdapter = new GroceryCategoryAdapter(getActivity(), foodItemData);
                            GroceryCategoryAdapter groceryCategoryAdapterTop = new GroceryCategoryAdapter(getActivity(), grocerryTopData);

                            //recycleGroceryCategories.setLayoutManager(new GridLayoutManager(mContext, 2,GridLayoutManager.VERTICAL, false));
                            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false) {

                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }

                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }

                            };

                            recycleGroceryCategories.setLayoutManager(layoutManager);
                            recycleGroceryCategories.setAdapter(groceryCategoryAdapter);
                            //recycleGroceryCategoriesTop.setLayoutManager(layoutManagerTop);
                            recycleGroceryCategoriesTop.setAdapter(groceryCategoryAdapterTop);

                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                        Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                }finally {
                    //Toast.makeText(mContext, "Finaly", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, ""+error, Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500,DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    //recycleGroceryCategories.setLayoutManager(new GridLayoutManager(mContext, 2,GridLayoutManager.VERTICAL, false));
}
