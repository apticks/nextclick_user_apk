package com.apticks.nextclickuser.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.SelectyAddressAdapter;
import com.apticks.nextclickuser.R;

//import android.app.Fragment;

public class SelectAddressFragment extends Fragment {
    Context mContext;

    //View
    View view;

    //Recycle Viuew
    RecyclerView selectAddressRecycle;
    //Button
    Button saveSelectAddress;

    public SelectAddressFragment() {
    }

    @SuppressLint("ValidFragment")
    public SelectAddressFragment(Context mContext) {
        this.mContext = mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view= inflater.inflate(R.layout.frag_select_address_fragment, container, false);

        initView();

        seeAddressSaved();

        return view;
    }

    private void seeAddressSaved() {
        SelectyAddressAdapter selectyAddressAdapter = new SelectyAddressAdapter();
        selectAddressRecycle.setAdapter(selectyAddressAdapter);
    }

    private void initView() {
        selectAddressRecycle =view.findViewById(R.id.select_address_recycle);
        //Recycle View
        RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(mContext);
        selectAddressRecycle.setHasFixedSize (true);
        selectAddressRecycle.setLayoutManager (mlayoutmanager);


        saveSelectAddress = view.findViewById(R.id.save_select_address);

    }

}
