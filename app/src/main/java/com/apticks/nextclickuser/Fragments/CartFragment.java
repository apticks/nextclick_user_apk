package com.apticks.nextclickuser.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.ItemsCartEcommerceAdapter;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

//import android.app.Fragment;

@SuppressLint("ValidFragment")
public class CartFragment extends Fragment {
    @SuppressLint("ValidFragment")
    public CartFragment(Context mContext) {
        this.mContext = mContext;
    }
    public CartFragment() {

    }

    //View
    View view;
    //Context
    Context mContext;

    //Recycle View
    RecyclerView recycleCart;
    //Text View
    TextView addressSelect ;
    //Linear Layout
    LinearLayout linearLayout;

    //Fragment
    Fragment selectedFragment = null;

    //Array List ModelData
    ArrayList<CartPojo> data = new ArrayList<>();
    //Boolean
    Boolean storedAddress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_cart_fragment,container,false);

        iniView();

        addressSelectClick();
        recycleCartClick();
        recycleViewCart();
        return view;
    }

    private void recycleViewCart() {
        {

          /*  RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //JSONObject dataJson = jsonObject.getJSONObject("foodMenuData");
                            JSONArray resultArray = jsonObject.getJSONArray("data");
                            if (resultArray.length() > 0) {*/
            for (int i = 0; i < 3/*resultArray.length()*/; i++) {
                                    /*JSONObject resultObject = resultArray.getJSONObject(i);
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("name");
                                    String image = resultObject.getString("image");*/
                CartPojo categoriesData = new CartPojo();
                categoriesData.setId(String.valueOf(i));
                categoriesData.setName("name");
                categoriesData.setPrice("250");
                categoriesData.setSpecification("jkl");
                //categoriesData.setImage(image);
                data.add(categoriesData);

            }

            ItemsCartEcommerceAdapter allCategoriesAdapter = new ItemsCartEcommerceAdapter(mContext, data);
            recycleCart.setAdapter(allCategoriesAdapter);
                         /*} catch (Exception e) {
                            e.printStackTrace();

                        }


                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(mContext, error.toString());
                    Log.d("error", error.toString());
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");

                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);*/
        }
    }

    private void recycleCartClick() {
        {

            recycleCart.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    int action = motionEvent.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch motionEvents.
                            view.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch motionEvents.
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    // Handle ListView touch motionEvents.
                    view.onTouchEvent(motionEvent);
                    return true;
                }
            });


        }

    }

    private void addressSelectClick() {
        {
            addressSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                /*// create a FragmentManager
                FragmentManager fm = getFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
                AddAddressFragment addAddressFragment=new AddAddressFragment();
                fragmentTransaction.replace(R.id.frag_container, addAddressFragment);
                fragmentTransaction.commit(); // save the changes*/


                    if(storedAddress=true){
                        /*
                         * For Old PAckage
                         * Package Name : import android.app.Fragment;
                         * Import this PAckage If want To use down Fragment Codes
                         *
                         * */
                    /*SelectAddressFragment selectAddressFragment=new SelectAddressFragment(mContext);
                    FragmentTransaction ft=getFragmentManager().beginTransaction();
                    //SecondFrag secondFrag=new SecondFrag();
                    ft.addToBackStack("selectFrag");
                    ft.replace(R.id.frag_container,selectAddressFragment);
                    ft.commit();*/

                        selectedFragment = new SelectAddressFragment(mContext);
                        getFragmentManager().beginTransaction().replace(R.id.frag_container,selectedFragment).commit();

                    }else {
                        /*
                         * For Old PAckage
                         * Package Name : import android.app.Fragment;
                         * Import this PAckage If want To use down Fragment Codes
                         *
                         * */
                    /*AddAddressFragment addAddressFragment=new AddAddressFragment();
                    FragmentTransaction ft=getFragmentManager().beginTransaction();
                    //SecondFrag secondFrag=new SecondFrag();
                    ft.addToBackStack("addAddressFrag");
                    ft.replace(R.id.frag_container,addAddressFragment);
                    ft.commit();*/
                        selectedFragment = new AddAddressFragment(mContext);
                        getFragmentManager().beginTransaction().replace(R.id.frag_container,selectedFragment).commit();


                    }


                    //linearLayout.setVisibility(View.GONE);
                    //fragmentTransaction.fragmentFactory.instantiate(ClassLoader.getSystemClassLoader(), addAddressFragment);

                /*Utils.switchFragmentWithAnimation(R.id.frag_container,
                        new AddAddressFragment(),  ConfirmPayment.this, Utils.ADD_ADDRESS_FRAGMENT,
                        Utils.AnimationType.SLIDE_UP);*/
                /*Utils.switchContent(R.id.frag_container,
                        Utils.ADD_ADDRESS_FRAGMENT,
                        ConfirmPayment.this, Utils.AnimationType.SLIDE_UP);*/

                    //Toast.makeText(mContext, "Toasting", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void iniView() {
        recycleCart = view.findViewById(R.id.recycle_cart);
        addressSelect = view.findViewById(R.id.address_select);

        //Linear Layout
        linearLayout = view.findViewById(R.id.linear_layout);

        RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(mContext);
        recycleCart.setHasFixedSize (true);
        recycleCart.setLayoutManager (mlayoutmanager);
    }
}
