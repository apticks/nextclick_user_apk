package com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.SliderAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OverViewFragment_HMS.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link OverViewFragment_HMS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverViewFragment_HMS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    View root;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private  String vendor_id;
    //private static Integer[] IMAGES = {R.drawable.sun, R.drawable.sun, R.drawable.sun, R.drawable.sun};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private  CirclePageIndicator indicator;

    private TextView vendor_name,vendor_location,vendor_contact;
    private RatingBar vendor_rating;

    private OnFragmentInteractionListener mListener;

    public OverViewFragment_HMS() {
        // Required empty public constructor
    }
    public OverViewFragment_HMS(String vendor_id) {
        // Required empty public constructor
        this.vendor_id = vendor_id;
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OverViewFragment_HMS.
     */
    // TODO: Rename and change types and number of parameters
    public static OverViewFragment_HMS newInstance(String param1, String param2) {
        OverViewFragment_HMS fragment = new OverViewFragment_HMS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root=inflater.inflate(R.layout.fragment_overview_hms, container, false);
        context = getActivity();
        initView();
        //initSlider();

        singleVendorDataFetcher();
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void initView(){
        vendor_name = root.findViewById(R.id.singlevendoritemname);
        vendor_location = root.findViewById(R.id.singlevendoritemlocation);
        vendor_contact = root.findViewById(R.id.singlevendoritemcontact);
        vendor_rating = root.findViewById(R.id.singlevendoritemratingbar);
        mPager = (ViewPager) root.findViewById(R.id.singlevendorItemPager);
        indicator = root.findViewById(R.id.singlevendorItemPagerIndicator);
    }

    private void initSlider(ArrayList<String> IMAGES) {
        for (int i = 0; i < IMAGES.size(); i++)
            ImagesArray.add(i);




        mPager.setAdapter(new SliderAdapter(context, IMAGES));




        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }



    public void singleVendorDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        vendor_name.setText(dataObject.getString("name"));
                        vendor_location.setText(dataObject.getString("address"));
                        vendor_contact.setText(dataObject.getJSONArray("contacts").getJSONObject(0).getString("number"));
                        ArrayList<String> banners_List = new ArrayList<>();
                        JSONObject bannersObject = dataObject.getJSONObject("banners");
                        for(int i=1;i<=bannersObject.length();i++){
                            banners_List.add(bannersObject.getString(i+"".trim()));
                        }
                        initSlider(banners_List);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        });

        requestQueue.add(stringRequest);

    }

}
