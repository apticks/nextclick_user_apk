package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.VendorsCompleteListAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VendorsListFragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link VendorsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VendorsListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    View root;
    ImageView back, banner;
    RecyclerView completListRecycler;
    ArrayList<DisplayCategory> data;
    String id = "0";

    private OnFragmentInteractionListener mListener;

    public VendorsListFragment(Context applicationContext, String id) {
        this.mContext = applicationContext;
        this.id = id;
    }

    public VendorsListFragment(String id) {

        this.id = id;
    }


    public VendorsListFragment() {
        // Required empty public constructor
    }

    public VendorsListFragment(Context context) {
        this.mContext = context;
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VendorsListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VendorsListFragment newInstance(String param1, String param2) {
        VendorsListFragment fragment = new VendorsListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_vendors_list, container, false);//Initializing View
        Window window = getActivity().getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.default_));
        }
        initView();
        dataFetcher();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Dasboard_Fragment(getActivity(), "0");
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

   /* @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public void dataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST /* + id+"/10/0" */, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            data = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String vendor_id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String address = resultObject.getString("address");
                                int Vender_user_id = resultObject.getInt("vendor_user_id");

                                Log.e("Kishore",""+Integer.toString(Vender_user_id));



                                DisplayCategory displayCategory = new DisplayCategory();
                                displayCategory.setId(vendor_id);
                                displayCategory.setName(name);
                                displayCategory.setImage(image);
                                displayCategory.setLocation(address);
                                displayCategory.setVendor_user_Id(Vender_user_id);
                                data.add(displayCategory);


                            }
                        }
                        VendorsCompleteListAdapter vendorsCompleteListAdapter = new VendorsCompleteListAdapter(mContext, data, id);
                        /*completListRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));*/
                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        completListRecycler.setLayoutManager(layoutManager);
                        completListRecycler.setAdapter(vendorsCompleteListAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });

        requestQueue.add(stringRequest);

    }

    public void initView() {
        completListRecycler = (RecyclerView) root.findViewById(R.id.vendorslistrecycler);
        back = root.findViewById(R.id.completelistback);
        banner = root.findViewById(R.id.bannerImage_vendors);
        if (id.equalsIgnoreCase("4")) {
            Picasso.get().load("http://cineplant.com/nextclick/uploads/sliders_image/sliders_4.png").into(banner);
        }
        if (id.equalsIgnoreCase("1")) {
            Picasso.get().load("https://californiahealthline.org/wp-content/uploads/sites/3/2018/09/hospital-merger.jpg?w=1024").into(banner);
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
