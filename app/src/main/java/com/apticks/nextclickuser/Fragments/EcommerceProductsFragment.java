package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devsmart.android.ui.HorizontalListView;
import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.Adapters.ProductListAdapter;
import com.apticks.nextclickuser.Adapters.SubSubCategoriesAdapter;
import com.apticks.nextclickuser.Adapters.EcommAdapter.SubCategoryPagerAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.ProductsPojo;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.Pojo.SubSubCategoryPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.ECOM_ALL_SUB_CATEGORIES;
import static com.apticks.nextclickuser.Config.Config.ECOM_CATEGORIES;
import static com.apticks.nextclickuser.Config.Config.ECOM_PRODUCTS_BASED_ON_SUBCATEGORY;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EcommerceProductsFragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link EcommerceProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EcommerceProductsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View root;
    Context mContext;
    RecyclerView productsrecycler;
    HorizontalListView sub_sub_listview;
    ViewPager viewPagerSubCategory;
    String sub_cat_id,ecomCategories;
    ArrayList<ProductsPojo> productList = new ArrayList<>();

    ArrayList<SubSubCategoryPojo> subsubcatListoductList = new ArrayList<>();
    ArrayList<SubCategoryItemPojo> ecomAllSubCategoriesData = new ArrayList<>();
    TabLayout subCategoryListTabLayout;

    public static int tabPosition;


    private OnFragmentInteractionListener mListener;

    public EcommerceProductsFragment() {
        // Required empty public constructor
        this.mContext = getActivity();
    }
    public EcommerceProductsFragment(Context context,String sub_cat_id, String ecomCategories) {
        this.mContext = context;
        this.sub_cat_id = sub_cat_id;
        this.ecomCategories = ecomCategories;

        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EcommerceProductsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EcommerceProductsFragment newInstance(String param1, String param2) {
        EcommerceProductsFragment fragment = new EcommerceProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_ecommerce_products, container, false);
        initView();
        productsFetcher(sub_cat_id);
        //sub_sub_categories_fetcher(sub_cat_id);

        ecomAllSubCategoriesDataFetcher(ecomCategories);//For AboveTAB

        onTabClicked();

        return root;
    }

    private void onTabClicked() {
        //subCategoryListTabLayout.setOnClickListener(new );
        //subCategoryListTabLayout.setScrollPosition(3,3,false);

        subCategoryListTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //productsrecycler.setVisibility(View.GONE);
                    /*productList.clear();
                ecomAllSubCategoriesData.get(tab.getPosition()).getId();
                productsFetcher(ecomAllSubCategoriesData.get(tab.getPosition()).getId());*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void ecomAllSubCategoriesDataFetcher(String ecomCategories) {
        {
            SubCategoryPagerAdapter.ecomCategoriesList.clear();

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_CATEGORIES +/*"1"*/ecomCategories, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {

                        try {
                            {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject dataJson = jsonObject.getJSONObject("data");
                                JSONArray resultArray = dataJson.getJSONArray("ecom_sub_categories");
                                if (resultArray.length() > 0) {
                                    for (int i = 0; i < resultArray.length(); i++) {
                                        JSONObject resultObject = resultArray.getJSONObject(i);
                                        String name = resultObject.getString("name");
                                        String image = resultObject.getString("image");
                                        String id = resultObject.getString("id");
                                        //String desc = resultObject.getString("desc");
                                        SubCategoryItemPojo subCategoryItemPojo = new SubCategoryItemPojo();

                                        subCategoryItemPojo.setName(name);
                                        subCategoryItemPojo.setImage(image);
                                        //subCategoryItemPojo.setImage(image);
                                        subCategoryItemPojo.setId(id);
                                        //ecomSubCategoriesData.add(subCategoryItemPojo);

                                        ecomAllSubCategoriesData.add(subCategoryItemPojo);
                                        SubCategoryPagerAdapter.ecomCategoriesList.add(subCategoryItemPojo);



                                    }
                                }
                            }
                            /*
                            JSONObject jsonObject = new JSONObject(response);
                            //JSONObject dataJson = jsonObject.getJSONObject("data");
                            JSONArray resultArray = jsonObject.getJSONArray("data");
                            if (resultArray.length() > 0) {
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String name = resultObject.getString("name");
                                    String image = resultObject.getString("image");
                                    String id = resultObject.getString("id");
                                    String desc = resultObject.getString("desc");
                                    EcomCategories ecomCategories = new EcomCategories();

                                    ecomCategories.setName(name);
                                    ecomCategories.setImage(image);
                                    ecomCategories.setId(id);
                                    ecomCategories.setDesc(desc);
                                    ecomAllSubCategoriesData.add(ecomCategories);
                                    SubCategoryPagerAdapter.ecomCategoriesList.add(ecomCategories);
                                }
                            }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                        }

                        //EcomSubCategoriesAdapter ecomSubCategoriesAdapter = new EcomSubCategoriesAdapter(getActivity(), ecomAllSubCategoriesData);
                        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        SubCategoryPagerAdapter sectionsPagerAdapter = new SubCategoryPagerAdapter(getFragmentManager());
                        viewPagerSubCategory.setAdapter(sectionsPagerAdapter);
                        //foodiemenutablayout.setupWithViewPager(viewpager);
                        subCategoryListTabLayout.setupWithViewPager(viewPagerSubCategory);
                        /*ecom_sub_categories_recycler.setLayoutManager(layoutManager);
                        ecom_sub_categories_recycler.setAdapter(ecomSubCategoriesAdapter);*/

                        subCategoryListTabLayout.setScrollX(subCategoryListTabLayout.getWidth());
                        subCategoryListTabLayout.getTabAt(tabPosition).select();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(getActivity(), error.toString());
                    Log.d("error", error.toString());
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void productsFetcher(String id){

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_PRODUCTS_BASED_ON_SUBCATEGORY+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String mrp = resultObject.getString("mrp");
                                String offer_price = resultObject.getString("offer_price");
                                String image = resultObject.getString("image");

                                ProductsPojo product = new ProductsPojo();

                                product.setId(id);
                                product.setName(name);
                                product.setMrp(mrp);
                                product.setOffer_price(offer_price);
                                product.setImage(image);
                                productList.add(product);



                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    ProductListAdapter productListAdapter = new ProductListAdapter(mContext, productList);
                    GridLayoutManager layoutManager = new GridLayoutManager(mContext,2,GridLayoutManager.VERTICAL,false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };

                    productsrecycler.setLayoutManager(layoutManager);
                    productsrecycler.setAdapter(productListAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void sub_sub_categories_fetcher(String id){

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_ALL_SUB_CATEGORIES+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("ecom_sub_sub_categories");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String sub_cat_id = resultObject.getString("sub_cat_id");
                                String name = resultObject.getString("name");
                                String id = resultObject.getString("id");

                                SubSubCategoryPojo product = new SubSubCategoryPojo();

                                product.setId(id);
                                product.setName(name);
                                product.setSub_cat_id(sub_cat_id);
                                subsubcatListoductList.add(product);



                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    SubSubCategoriesAdapter subSubCategoriesAdapter = new SubSubCategoriesAdapter(mContext, R.layout.sub_sub_category_supporter,subsubcatListoductList);
                    GridLayoutManager layoutManager = new GridLayoutManager(mContext,2,GridLayoutManager.VERTICAL,false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };


                    sub_sub_listview.setAdapter(subSubCategoriesAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }



    public void initView(){

        productsrecycler = (RecyclerView) root.findViewById(R.id.ecom_products_recycler);
        sub_sub_listview = (HorizontalListView) root.findViewById(R.id.sub_sub_category_list);
        subCategoryListTabLayout = (TabLayout) root.findViewById(R.id.sub_sub_category_list_tablayout);
        subCategoryListTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        viewPagerSubCategory = root.findViewById(R.id.viewpager_sub_category);

    }

}
