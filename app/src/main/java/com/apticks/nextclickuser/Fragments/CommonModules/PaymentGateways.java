package com.apticks.nextclickuser.Fragments.CommonModules;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.apticks.nextclickuser.Fragments.OrderHistory.MainHistoryActivity;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.Constants.Constants;
import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.view.Gravity.CENTER;
import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Fragments.CartFoodFragments.FoodCartFragment.*;


//public class PaymentGateways extends Fragment {
public class PaymentGateways extends AppCompatActivity implements PaymentResultListener {
    View view;
    private TextView total_cost;
    private Toolbar tool_bar_payment;
    private Button place_order;
    private RadioGroup radio_group_payment;
    private RadioButton radio_cash_onDelivery, radio_credit, radio_net_banking;
    private RelativeLayout relative_layout_;
    private OrderPlacedConfirmation orderPlacedConfirmation;
    private PreferenceManager preferenceManager;
    /*public PaymentGateways(){
        //super();
    }*/
    Context mContext;
    Bundle bundle;

    private DataBaseHelper dataBaseHelper;//Database Helper
    private CartDBHelper cartDBHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment_gateway);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        dataBaseHelper = new DataBaseHelper(mContext);
        cartDBHelper = new CartDBHelper(mContext);
        orderPlacedConfirmation = new OrderPlacedConfirmation(mContext);
        preferenceManager = new PreferenceManager(mContext);


        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }



        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.setStatusBarColor(ContextCompat.getColor(mContext, R.color.orange));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(mContext, R.color.orange));
            }
        }*/
        initView();//Text View
        bundle = getIntent().getExtras();
        //bundle.getString("mainFoodOrder");
        initTotatlCost();//Setting Total Price
        PlaceOrderClick();
        radio_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio_credit.setChecked(false);

                UImsgs.showToast(mContext,"Will be available soon");
                validRadioSelected(v);
            }
        });
    }

    private void PlaceOrderClick() {
        place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validRadioSelected(v)) {

                    if (bundle.getString(
                            "mainFoodOrder") != null) {
                        //UImsgs.showToast(mContext, bundle.getString("mainFoodOrder"));
                        int selectedId = radio_group_payment.getCheckedRadioButtonId();
                        if (radio_cash_onDelivery.isChecked()) {
                            String data = bundle.getString("mainFoodOrder");
                            String totatlAmount = bundle.getString("totatlpayamount");
                            proceedtoPayClick(data, v);
                        } else if (radio_credit.isChecked()) {
                            /*String totatlAmount = bundle.getString("totatlpayamount");
                            razorPayPayment(totatlAmount);*/
                            radio_credit.setChecked(false);

                            UImsgs.showToast(mContext,"Will be available soon");
                        } else if (radio_net_banking.isChecked()) {

                        }
                        //UImsgs.showToast(mContext, String.valueOf(selectedId));
                    }
                }
            }
        });

    }

    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            orderRequest.put("amount", totatlAmount+"00"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);

            //Order order = razorpay.Orders.create(orderRequest);
            co.open((Activity) activity, orderRequest);
        }/* catch (RazorpayException e) {
            // Handle Exception
            System.out.println(e.getMessage());
        }*/

        catch (JSONException e) {
            e.printStackTrace();
            UImsgs.showToast(mContext, String.valueOf(e));
        } catch (Exception e) {
            e.printStackTrace();
            UImsgs.showToast(mContext, String.valueOf(e));
        }
    }

    private void proceedtoPayClick(final String data, View v) {
        {
            //final String data = json.toString();
            Log.d("PaymentData", "" + data);

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, FoodOrder, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            //
                            int http_code = jsonObject.getInt("http_code");
                            if (http_code == 201) {
                                //Toast.makeText(mContext, R.string.order_sucessfully, Toast.LENGTH_SHORT).show();
                                cartDBHelper.deleteCartTable();
                                cartDBHelper.deleteSectCartTable();
                                foodOrderPostItemArrayHashMap.clear();
                                foodOrderPostModelsArrayHashMap.clear();
                                relative_layout_.setVisibility(View.GONE);

                                succesDialog();

                                /*mContext = getApplicationContext();
                                orderPlacedConfirmation = new OrderPlacedConfirmation(mContext);*/

                                //orderPlacedConfirmation.alertDialogOrderPlaced();
                                /*orderPlacedConfirmation.ok_placed.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //orderPlacedConfirmation.dialogDismiss();
                                        UImsgs.showToast(mContext, String.valueOf(orderPlacedConfirmation.dialogDismiss()));
                                    }
                                });*/

                            }
                            /*String message = jsonObject.getString("message");
                            int data = jsonObject.getInt("data");*/
                            //UImsgs.showToast(mContext,String.valueOf(data));


                            //getActivity().getFragmentManager().popBackStackImmediate();
                            //getActivity().getSupportFragmentManager().popBackStackImmediate();
                            //getActivity().onBackPressed();

                            //getChildFragmentManager().beginTransaction().replace(R.id.cart_nav_host_fragment, selectedFragment).commit();

                            //getFragmentManager().beginTransaction().replace(R.id.cart_nav_host_fragment,selectedFragment,"").commit();
                            //FragmentActivity fragmentManager = ((FoodCartFragment) (getContext())).getSupportFragmentManager();
                            //getSupportFragmentManager
                            /*FragmentManager mFragmentManager = getFragmentManager();
                            FragmentTransaction transaction = mFragmentManager.beginTransaction();*/

                            //getFragmentManager().beginTransaction().replace(R.id.food_nav_host_fragment,selectedFragment,"").commit();
                            //transaction.add(R.id.food_nav_host_fragment,selectedFragment,"").commit();

                            //setTargetFragment(selectedFragment,R.id.cart_nav_host_fragment);
                        } else {
                            UImsgs.showSnackBar(v, IErrors.ORDER_NOT_PLACED);
                            Toast.makeText(mContext, " + " + response , Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "Catch "+e, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        }
    }

    private void succesDialog(){
        ImageView success_gif;
        Button success_ok_btn;
        final Dialog dialog = new Dialog(PaymentGateways.this);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.order_success_layout);
        dialog.setCancelable(false);
        window.setGravity(CENTER);

        success_ok_btn = dialog.findViewById(R.id.success_ok_btn);
        success_gif = dialog.findViewById(R.id.success_gif);
        Glide.with(PaymentGateways.this)
                .load(R.drawable.ordersuccess)
                .into(success_gif);

        success_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(mContext, MainHistoryActivity.class);
                startActivity(intent);
                finish();
            }
        });


        window.setGravity(CENTER);
        dialog.show();
    }

    private boolean validRadioSelected(View v) {
        if (radio_group_payment.getCheckedRadioButtonId() == -1) {
            // no radio buttons are checked
            UImsgs.showSnackBar(v, getResources().getString(R.string.select_mop));
            return false;
        } else {
            // one of the radio buttons is checked
            //Toast.makeText(mContext, "OK Selected", Toast.LENGTH_SHORT).show();

            return true;
        }


    }

    private void initTotatlCost() {
        String total_costStr =/*Long.valueOf*/(String.valueOf(dataBaseHelper.getTotalCartAmount()));
        /*String sellCostString = Money.rupees(
                BigDecimal.valueOf(Long.valueOf(total_costStr
                ))).toString()
                + "      ";*/
        String totatlAmount = bundle.getString("totatlpayamount");
        total_cost.setText(totatlAmount);
    }

    private void initView() {
        //ToolBar
        tool_bar_payment = findViewById(R.id.tool_bar_payment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar_payment.setTitle(Constants.PAYMENT);
            tool_bar_payment.setBackgroundColor(getResources().getColor(R.color.Iconblue));
        } else {
            //tool_bar_payment.
        }
        //Text view
        total_cost = findViewById(R.id.total_cost);
        //Button
        place_order = findViewById(R.id.place_order);
        //Radio Group
        radio_group_payment = findViewById(R.id.radio_group_payment);

        //Radio Button
        radio_cash_onDelivery = findViewById(R.id.radio_cash_onDelivery);
        radio_credit = findViewById(R.id.radio_credit);
        radio_net_banking = findViewById(R.id.radio_net_banking);

        //Relative Layout
        relative_layout_ = findViewById(R.id.relative_layout_);
    }

    @Override
    public void onPaymentSuccess(String s) {
        //UImsgs.showToast(mContext,String.valueOf("Sucess  "+ s));
        relative_layout_.setVisibility(View.VISIBLE);
        View v = getWindow().getDecorView().getRootView();
        UImsgs.showSnackBar(v, IErrors.ORDER_PLACED);
        String data = bundle.getString("mainFoodOrder");
        proceedtoPayClick(data, v);
    }

    @Override
    public void onPaymentError(int i, String s) {
        UImsgs.showToast(mContext, String.valueOf("Error " + "  " + i + " " + s));
    }

    /*@Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_payment_gateway, container,false);
        return view;
        //return inflater.inflate(R.layout.fragment_payment_gateway, container,false);
    }*/


}
