package com.apticks.nextclickuser.Fragments.OrderHistory;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.UserHistory.PresentOrderOuterAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.OrderHistory.CurrentOrder;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class PresentOrder extends Fragment {
    RecyclerView recyclerViewUpcomingOrder;
    ArrayList<CurrentOrder> currentOrdersArrayList;
    View view;
    Context mContext;
    PreferenceManager preferenceManager;
    String token;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);
        initView();

        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);

//Setting Upcoming Order Details
        orderComingDetails();

        return view;
    }

    private void orderComingDetails() {
        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        String user_id = preferenceManager.getString("user_id");
        String tempUrl = Food_Orders_History_Past_Upcoming_cancelled + "upcoming/" + user_id;
        Log.d("presentorderurl", tempUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("PresentOrder", response.toString());
                if (response != null) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("status")) {

                            JSONArray jsonArrayData = jsonObject.getJSONArray("data");
                            if (jsonArrayData.length() > 0) {
                                currentOrdersArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonArrayData.length(); i++) {
                                    JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);

                                    CurrentOrder currentOrder = new CurrentOrder();
                                    currentOrder.setOrder_id(jsonObjectData.getString("id"));
                                    currentOrder.setDiscount(jsonObjectData.getInt("discount"));
                                    currentOrder.setDeliveryPrice(jsonObjectData.getInt("delivery_fee"));
                                    currentOrder.setOtp(jsonObjectData.getString("otp"));
                                    currentOrder.setTax(jsonObjectData.getInt("tax"));
                                    currentOrder.setTotal(jsonObjectData.getInt("total"));
                                    currentOrder.setOrder_track(jsonObjectData.getString("order_track"));

                                    currentOrder.setOrderStatus(jsonObjectData./*getJSONObject("order_status").*//*getString("name")*/getString("order_status"));
                                    currentOrder.setCreated_at(jsonObjectData.getString("created_at"));
                                    try {
                                        JSONObject jsonObjectVendor = jsonObjectData.getJSONObject("vendor");
                                        currentOrder.setName(jsonObjectVendor.getString("name"));
                                        currentOrder.setVendorEmailId(jsonObjectVendor.getString("email"));

                                        JSONArray jsonArrayNumber = jsonObjectVendor.getJSONArray("numbers");
                                        if (jsonArrayNumber.length() >= 1) {
                                            JSONObject jsonObjectNumber = jsonArrayNumber.getJSONObject(0);
                                            String number = jsonObjectNumber.getString("number");
                                            currentOrder.setVendorMobileNumber(number);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        JSONArray jsonArrayOrderItems = jsonObjectData.getJSONArray("order_items");
                                        ArrayList<CurrentOrder> currentOrdersArrayListInnerItems = new ArrayList<>();
                                        for (int j = 0; j < jsonArrayOrderItems.length(); j++) {
                                            JSONObject jsonObjectOrderItems = jsonArrayOrderItems.getJSONObject(j);
                                            CurrentOrder currentOrderInnerItems = new CurrentOrder();
                                            currentOrderInnerItems.setPriceOrderItems(jsonObjectOrderItems.getInt("price"));
                                            currentOrderInnerItems.setQuantityOrderItems(jsonObjectOrderItems.getInt("quantity"));

                                            currentOrdersArrayListInnerItems.add(currentOrderInnerItems);
                                        }
                                        currentOrder.setCurrentOrdersArrayListInnerItems(currentOrdersArrayListInnerItems);

                                    } catch (JSONException e) {
                                        Log.e("JsonException", "" + e.toString());

                                    }
                                    try {
                                        JSONArray jsonArrayOrderItems = jsonObjectData.getJSONArray("sub_order_items");
                                        //Toast.makeText(mContext, String.valueOf(jsonObjectData.getJSONArray("order_items")), Toast.LENGTH_LONG).show();
                                        ArrayList<CurrentOrder> currentOrdersArrayListInnerSubItems = new ArrayList<>();
                                        for (int j = 0; j < jsonArrayOrderItems.length(); j++) {
                                            JSONObject jsonObjectOrderItems = jsonArrayOrderItems.getJSONObject(j);
                                            CurrentOrder currentOrderInnerItems = new CurrentOrder();
                                            currentOrderInnerItems.setPriceOrderItems(jsonObjectOrderItems.getInt("price"));
                                            currentOrderInnerItems.setQuantityOrderItems(jsonObjectOrderItems.getInt("quantity"));
                                            currentOrdersArrayListInnerSubItems.add(currentOrderInnerItems);
                                        }
                                        currentOrder.setCurrentOrdersArrayListInnerSubItems(currentOrdersArrayListInnerSubItems);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    currentOrdersArrayList.add(currentOrder);

                                }
                                PresentOrderOuterAdapter presentOrderOuterAdapter = new PresentOrderOuterAdapter(currentOrdersArrayList, "present");
                                recyclerViewUpcomingOrder.setAdapter(presentOrderOuterAdapter);
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("Error :", "" + e);
                        progressDialog.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void initView() {
        recyclerViewUpcomingOrder = view.findViewById(R.id.recycler_view_upcoming_order);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }
                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        recyclerViewUpcomingOrder.setLayoutManager(linearLayoutManager);
    }
}

