package com.apticks.nextclickuser.Helpers.FastScrollingRecycler;

import java.util.HashMap;

/**
 * Created by flaviusmester on 23/02/15.
 */
public interface FastScrollRecyclerViewInterface {
    public HashMap<String,Integer> getMapIndex();
}