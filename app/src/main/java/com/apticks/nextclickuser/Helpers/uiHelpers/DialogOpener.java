package com.apticks.nextclickuser.Helpers.uiHelpers;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.apticks.nextclickuser.R;

import static android.view.Gravity.CENTER;

public class DialogOpener {

    public static Dialog dialog;
    public static void dialogOpener(Context context){
        ImageView loader_gif;

        dialog = new Dialog(context);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gif_dilog);
        dialog.setCancelable(false);
        window.setGravity(CENTER);


        loader_gif = dialog.findViewById(R.id.loader_gif);
        Glide.with(context)
                .load(R.drawable.loader_gif)
                .into(loader_gif);
        window.setGravity(CENTER);
        dialog.show();
    }
}
