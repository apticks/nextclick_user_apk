package com.apticks.nextclickuser.Activities;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.andremion.counterfab.CounterFab;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.apticks.nextclickuser.Adapters.ProductListAdapter;
import com.apticks.nextclickuser.Adapters.ProductSlidingAdapter;
import com.apticks.nextclickuser.Adapters.EcommAdapter.EcommDescExpandableListAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.ProductsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.viewpagerindicator.CirclePageIndicator;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class IndividualEcommProductActivity extends AppCompatActivity {

    TextView /*individual_item_name_header,*/ individual_product_price, individual_product_mrp_price, individual_product_discount,
            product_decription, individual_item_name, product_add_to_cart, product_buy_now,piecesleft;
    Toolbar individual_item_name_header;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton floatBuyNow;
    CounterFab btnCartCounterFab;
    AVLoadingIndicatorView loading_bar_single;;

    Context mContext;
    RecyclerView relatedItemsRecycler;
    ArrayList<ProductsPojo> productList = new ArrayList<>();
    ArrayList<String> imageArray = new ArrayList<>();

    RecyclerView productsrecycler;

    EcommDescExpandableListAdapter ecommDescExpandableListAdapter;

    ExpandableListView lvDescCategory;

    int currentPage = 0;
    int NUM_PAGES = 0;
    ViewPager sliderPager;
    PreferenceManager preferenceManager;
    String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_individual_product);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        String id = getIntent().getStringExtra("product_id");
        init();
        productFetcher(id);//Sliding
        relatedproductsFetcher(id);//Related Products

        product_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product_add_to_cart.getText().toString().trim().equalsIgnoreCase("Add To Cart")){
                    product_add_to_cart.setText("Added");
                    Toast.makeText(mContext, product_add_to_cart.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                    addToCartFragment();
                }
                else {
                    product_add_to_cart.setText("Add To Cart");}
                Toast.makeText(mContext, product_add_to_cart.getText().toString().trim(), Toast.LENGTH_SHORT).show();
            }
        });


        Map<String, Object> mainData = new HashMap<>();
        mainData.put("discount","10");
        mainData.put("tax","12");
        mainData.put("total","18");
        mainData.put("coupon_id","12");
        mainData.put("payment_method_id","12");
        mainData.put("address_id","1");
        mainData.put("name", "");
        mainData.put("email", "");
        mainData.put("mobile", "");
        mainData.put("address", "");
        //mainData.put("products", ecomOrderPostModelsArray);
        /*{
            "discount":"10",
                "tax":"18",
                "total":"256",
                "coupon_id":"12",
                "payment_method_id":"1",
                "address_id":"",
                "name": "mehar",
                "email": "trupti@gmail.com",
                "mobile": "8522808784",
                "address": "sdfas,asdfadsf,asdfasdf,asf",
                "products": [
            {
                "product_id": 1,
                    "qty": "1",
                    "price": "100"
            },
            {
                "product_id": 2,
                    "qty": "2",
                    "price": "5200"
            }
]
        }*/

        buyNowClick();
        floatBuyNowClick(id);//Buy Now
        btnCartCounterFabClick(id);//Btn Cart Click Listener

        //relatedItemsRecycleView("9"/*idRelatedTemp*/);

    }

    private void btnCartCounterFabClick(String id) {
        btnCartCounterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> mainData = new HashMap<>();
                //mainData.put("id",id);
                mainData.put("product_id",id);
                mainData.put("qty","1");
                JSONObject json = new JSONObject(mainData);
                addToCartEcommProduct(json);//Calling JSON API
            }
        });
    }

    private void addToCartEcommProduct(JSONObject json) {
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_CART_ADD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //UImsgs.showToast(mContext,response);

                Intent intent = new Intent(mContext, CartActivity.class);
                intent.putExtra("Type","Ecom");
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, ""+error, Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void floatBuyNowClick(String id) {
        floatBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "BUY NOW", Toast.LENGTH_SHORT).show();
                apiHit(id);
            }
        });
    }

    private void apiHit(String id) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        ArrayList<String> stringsBuyNow = new ArrayList<>();

        stringsBuyNow.add(id);
        stringsBuyNow.add("1");
        stringsBuyNow.add("100");

        stringObjectHashMap.put("discount","10");
        stringObjectHashMap.put("tax","12");
        stringObjectHashMap.put("total","19080");
        stringObjectHashMap.put("coupon_id","12");
        stringObjectHashMap.put("payment_method_id","12");
        stringObjectHashMap.put("address_id","1");
        stringObjectHashMap.put("name", "mehar");
        stringObjectHashMap.put("email", "nextclick@nextclick.com");
        stringObjectHashMap.put("mobile", "9876543210");
        stringObjectHashMap.put("address", "nextclick next click Address");
        stringObjectHashMap.put("products", stringsBuyNow);
        JSONObject json = new JSONObject(stringObjectHashMap);
        proceedToBuyNow(json);
    }

    private void proceedToBuyNow(JSONObject json) {
        {
            final String data = json.toString();
                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_PRODUCT_CHECK_OUT, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                            //Toast.makeText(mContext, "Your Order Has Been Placed Successfully", Toast.LENGTH_SHORT).show();




                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(mContext, ""+error, Toast.LENGTH_SHORT).show();
                        }
                    }){

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("Content-Type", "application/json");
                            map.put("X_AUTH_TOKEN", token);
                            return map;
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return data == null ? null : data.getBytes("utf-8");

                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);
                }
    }

    private void relatedItemsRecycleView(String idRelatedTemp) {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_PRODUCTS_BASED_ON_SUBCATEGORY +idRelatedTemp, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject dataJson = jsonObject.getJSONObject("data");
                            JSONArray resultArray = dataJson.getJSONArray("result");
                            if (resultArray.length() > 0) {
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("name");
                                    String mrp = resultObject.getString("mrp");
                                    String offer_price = resultObject.getString("offer_price");
                                    String image = resultObject.getString("image");

                                    ProductsPojo product = new ProductsPojo();

                                    product.setId(id);
                                    product.setName(name);
                                    product.setMrp(mrp);
                                    product.setOffer_price(offer_price);
                                    product.setImage(image);
                                    productList.add(product);

                                    loading_bar_single.setVisibility(View.GONE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                        }
                        ProductListAdapter productListAdapter = new ProductListAdapter(mContext, productList);
                        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        productsrecycler.setLayoutManager(layoutManager);
                        productsrecycler.setAdapter(productListAdapter);
                    }
                }}, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_SHORT).show();
                }
            }
            );
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }

    }

    private void buyNowClick() {
        product_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*{
                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, FoodOrder, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                            Toast.makeText(mContext, "Your Order Has Been Placed Successfully", Toast.LENGTH_SHORT).show();

                            //getActivity().getFragmentManager().popBackStackImmediate();
                            //getActivity().getSupportFragmentManager().popBackStackImmediate();


                            //getChildFragmentManager().beginTransaction().replace(R.id.cart_nav_host_fragment, selectedFragment).commit();

                            //getFragmentManager().beginTransaction().replace(R.id.cart_nav_host_fragment,selectedFragment,"").commit();
                            //FragmentActivity fragmentManager = ((FoodCartFragment) (getContext())).getSupportFragmentManager();
                            //getSupportFragmentManager






                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }){

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("Content-Type", "application/json");
                            map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzQ3Nzk3ODJ9.whQ2aquLA-MWPbZdRC7O87aqjvq1vV6bnMuM_dzacfw");
                            return map;
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return data == null ? null : data.getBytes("utf-8");

                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);
                }*/
            }
        });
    }

    private void addToCartFragment() {

    }

    public void productFetcher(String id) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_PRODUCT_ON_PRODUCTID + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");


                        //JSONObject resultObject = resultArray.getJSONObject(i);
                        String id = dataJson.getString("id");
                        String name = dataJson.getString("name");
                        String desc = dataJson.getString("desc");
                        String mrp = dataJson.getString("mrp");
                        String offer_price = dataJson.getString("offer_price");
                        String image = dataJson.getString("image");
                        String pieces = dataJson.getString("qty");
                        //Toast.makeText(IndividualEcommProductActivity.this, id+name+desc+mrp+offer_price, Toast.LENGTH_SHORT).show();
                        //Toast.makeText(IndividualEcommProductActivity.this, desc, Toast.LENGTH_SHORT).show();
                        imageArray.add(image);
                        imageArray.add(image);
                        imageArray.add(image);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            individual_item_name_header.setTitle(name);
                        }
                        collapsingToolbarLayout.setTitle(name);
                        individual_product_price.setText(offer_price);
                        product_decription.setText(Html.fromHtml(desc));
                        individual_product_mrp_price.setText(mrp);
                        piecesleft.setText(pieces);

                        individual_item_name.setText(name);

                        initView(imageArray);

                        JSONObject subCategoryJson = dataJson.getJSONObject("sub_category");
                        String subCategoryId = subCategoryJson.getString("id");
                        //UImsgs.showToast(mContext,subCategoryId);
                        relatedItemsRecycleView(subCategoryId/*idRelatedTemp*/);//Related Items Product

                        ecommDescExpandableListAdapter= new EcommDescExpandableListAdapter(getApplicationContext(),"Description",desc);
                        lvDescCategory.setAdapter(ecommDescExpandableListAdapter);

                        ViewGroup.LayoutParams params = lvDescCategory.getLayoutParams();
                        params.height = 1* 50;
                        lvDescCategory.setLayoutParams(params);
                        lvDescCategory.requestLayout();

                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(mContext, ""+e, Toast.LENGTH_SHORT).show();

                    }finally {


                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void relatedproductsFetcher(String id) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_PRODUCTS_BASED_ON_SUBCATEGORY + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String mrp = resultObject.getString("mrp");
                                String offer_price = resultObject.getString("offer_price");
                                String image = resultObject.getString("image");

                                ProductsPojo product = new ProductsPojo();

                                product.setId(id);
                                product.setName(name);
                                product.setMrp(mrp);
                                product.setOffer_price(offer_price);
                                product.setImage(image);
                                productList.add(product);


                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    ProductListAdapter productListAdapter = new ProductListAdapter(mContext, productList);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };

                    relatedItemsRecycler.setLayoutManager(layoutManager);
                    relatedItemsRecycler.setAdapter(productListAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void init() {

        //individual_item_name_header = findViewById(R.id.individual_item_name_header);
        individual_item_name_header = findViewById(R.id.individual_item_name_header_toolbar);
        individual_product_price = findViewById(R.id.individual_product_price);
        individual_product_mrp_price = findViewById(R.id.individual_product_mrp_price);
        individual_product_discount = findViewById(R.id.individual_product_discount);
        individual_item_name = findViewById(R.id.individual_item_name);
        product_decription = findViewById(R.id.product_description);
        product_add_to_cart = findViewById(R.id.product_add_to_cart);
        product_buy_now = findViewById(R.id.product_buy_now);
        piecesleft = findViewById(R.id.pcs_left);

        //Collapsing ToolBar
        collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        //Floating Action Button
        floatBuyNow = findViewById(R.id.float_buy_now);
        btnCartCounterFab = findViewById(R.id.btn_cart_counter_fab);

        relatedItemsRecycler = findViewById(R.id.relatedItems);
        sliderPager = (ViewPager) findViewById(R.id.individual_product_image_pager);

        //Recycle View
        productsrecycler = (RecyclerView) findViewById(R.id.ecom_related_products_recycler);

        //Expandable List View
        lvDescCategory = findViewById(R.id.lv_desc_category);

        //Loading
        loading_bar_single = findViewById(R.id.loading_bar_single);
    }

    public void initView(ArrayList<String> imageList) {


        //sliderView();

        ProductSlidingAdapter productSlidingAdapter = new ProductSlidingAdapter(mContext, imageList);
        sliderPager.setAdapter(productSlidingAdapter);
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.individual_product_image_indicator);

        indicator.setViewPager(sliderPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = imageList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                sliderPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }


}
