package com.apticks.nextclickuser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.DialogTrackOrderAdapter;
import com.apticks.nextclickuser.Adapters.UserHistory.DialogTrackPresentOrder;
import com.apticks.nextclickuser.Pojo.OrderPojo.OrderTrackPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apticks.nextclickuser.Config.Config.SINGLE_ORDER_DETAILS;

public class OrderStatusActivity extends AppCompatActivity {


    private Context mContext;
    private ImageView back_imageView;
    private TextView order_id,listing_name,listing_location,
            listing_distance,payment_id,text_order_confirmed,
            text_delivery_pickup,text_order_delivered;
    List<OrderTrackPojo> orderTrackPojoList = new ArrayList<>();
    private RecyclerView recycler_order_track;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void init() {
        mContext = OrderStatusActivity.this;
        back_imageView = findViewById(R.id.back_imageView);
        order_id = findViewById(R.id.order_id);
        listing_name = findViewById(R.id.listing_name);
        listing_location = findViewById(R.id.listing_location);
        listing_distance = findViewById(R.id.listing_distance);
        recycler_order_track = findViewById(R.id.recycler_order_track);

        order_id.setText(getIntent().getStringExtra("order_track_id"));
        fetItemTrackDetail(getIntent().getStringExtra("order_id"));
    }


    private void fetItemTrackDetail(String order_id) {
        Log.d("single_order_url",SINGLE_ORDER_DETAILS+order_id);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SINGLE_ORDER_DETAILS+order_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if(response!=null){
                    Log.d("single_order_response",response);
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                        listing_name.setText(jsonObjectData.getJSONObject("vendor").getString("name"));
                        listing_location.setText(jsonObjectData.getJSONObject("vendor").getString("address")+"\n"+jsonObjectData.getJSONObject("vendor").getString("landmark"));

                        JSONArray jsonArrayOrderItems = jsonObjectData.getJSONObject("ord_status").getJSONArray("status_list");
                        for (int i =0; i<jsonArrayOrderItems.length();i++){
                            JSONObject jsonObjectValue = jsonArrayOrderItems.getJSONObject(i);
                            OrderTrackPojo orderTrackPojo = new OrderTrackPojo();

                            orderTrackPojo.setId(jsonObjectValue.getInt("id"));
                            orderTrackPojo.setName(jsonObjectValue/*.getJSONObject("name")*/.getString("name"));
                            //orderTrackPojo.setStatus(jsonObjectValue.getInt("status"));
                            //if(jsonObjectValue.getInt("id")<5) {

                            if(i!=0  && i!=7) {
                                orderTrackPojoList.add(orderTrackPojo);
                                //}
                            }
                                if(jsonObjectData.getInt("order_status")==0){
                                    orderTrackPojoList.clear();
                                    orderTrackPojo.setId(0);
                                    orderTrackPojo.setName("Rejected");
                                    orderTrackPojoList.add(orderTrackPojo);
                                }if(jsonObjectData.getInt("order_status")==7){
                                    orderTrackPojoList.clear();
                                    orderTrackPojo.setId(7);
                                    orderTrackPojo.setName("Cancelled");
                                    orderTrackPojoList.add(orderTrackPojo);
                                }

                        }

                        if (orderTrackPojoList.size()!=0){
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }
                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }
                            };
                            recycler_order_track.setLayoutManager(linearLayoutManager);

                            DialogTrackOrderAdapter dialogTrackOrderAdapter = new DialogTrackOrderAdapter(orderTrackPojoList,jsonObjectData.getInt("order_status") );
                            recycler_order_track.setAdapter(dialogTrackOrderAdapter);
                        }else {
                            //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        //Toast.makeText(mContext, ""+ex, Toast.LENGTH_SHORT).show();
                    } catch (Exception e){

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}
