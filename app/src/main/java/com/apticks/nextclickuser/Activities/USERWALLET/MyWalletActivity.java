package com.apticks.nextclickuser.Activities.USERWALLET;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import com.apticks.nextclickuser.Activities.USERPROFILE.UserProfileActivity;
import com.apticks.nextclickuser.Config.Config;
import com.apticks.nextclickuser.Fragments.WALLETFRAGMENTS.AddMoneyFragment;
import com.apticks.nextclickuser.Fragments.WALLETFRAGMENTS.TransactionsFragment;
import com.apticks.nextclickuser.Helpers.JSONParser;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


import static com.apticks.nextclickuser.Config.Config.GENERATE_CHECKSUM;
import static com.apticks.nextclickuser.Constants.Constants.NAME;
import static com.apticks.nextclickuser.Constants.Constants.UNIQUE_ID;
import static com.apticks.nextclickuser.Constants.Constants.WALLET;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class MyWalletActivity extends AppCompatActivity /*implements PaytmPaymentTransactionCallback*/ {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    private JSONParser jsonParser ;


    PreferenceManager preferenceManager;
    String storedUserToken;

    // url to get all products list
    private static final String url = Config.BASE_URL + "payment.php";
    private static final String urlpaytmchecksum = Config.paytmchecksum + "gen_checksum";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    //user
    private static final String TAG_USERID = "userid";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mobile";

    //balance
    private static final String TAG_USERBALANCE = "balance";

    //instamojo
    private static final String TAG_INSTA_ORDERID = "instaorderid";
    private static final String TAG_INSTA_TXNID = "instatxnid";
    private static final String TAG_INSTA_PAYMENTID = "instapaymentid";
    private static final String TAG_INSTA_TOKEN = "instatoken";

    private Integer balance;
    private String email;
    private LinearLayout main;
    private String number;
    private TabLayout tabLayout;
    private String username;
    private ViewPager viewPager;
    private TextView walletBalance,wallet_back;

    //Prefrance


    //paytm
    private String paytmemail;
    private String paytmphone;
    private String paytmamount;
    private String paytmpurpose;
    private String paytmbuyername;
    private String paytmorder_id;
    private String paytmchecksumhash;

    //instamojo
//    InstapayListener listener;
//    InstamojoPay instamojoPay;

    private String addamount;
    private String instaorderid;
    private String instatoken;
    private String instapaymentid;
    private String instatxnid;

    private int success;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mywallet);
        getSupportActionBar().hide();


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        jsonParser = new JSONParser(getApplicationContext());

        // Call the function callInstamojo to start payment here

        preferenceManager = new PreferenceManager(MyWalletActivity.this);

        wallet_back = (TextView) findViewById(R.id.wallet_back);
        walletBalance = (TextView) findViewById(R.id.walletBalance);
        main = (LinearLayout) findViewById(R.id.mainLayout);
        balance = preferenceManager.getInt(WALLET);
        username = preferenceManager.getString(NAME);
        email = preferenceManager.getString(TAG_EMAIL);
        number = preferenceManager.getString(TAG_MOBILE);
        if(balance!=null){
            walletBalance.setText("₹ "+balance);
        }
        else{
            walletBalance.setText("₹ 0");
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        wallet_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    public void PaytmAddMoney(String email, String phone, String amount, String purpose, String buyername) {

        paytmemail = email;
        paytmphone = phone;
        paytmamount = amount;
        paytmpurpose = purpose;
        paytmbuyername = buyername;

        storedUserToken = preferenceManager.getString(TOKEN_KEY);


        final int min = 1000;
        final int max = 10000;
        final int random = new Random().nextInt((max - min) + 1) + min;
        paytmorder_id = preferenceManager.getString(UNIQUE_ID)+"-" +random;

        // Join Player in Match in Background Thread
        new GetChecksum().execute();

    }

    class GetChecksum extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MyWalletActivity.this);
            pDialog.setMessage("Loading Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url
         * */
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected String doInBackground(String... args) {
            // Building Parameters
            Map<String, String> params = new HashMap<>();
            params.put( "MID" , Config.MID);
            params.put( "ORDER_ID" , paytmorder_id);
            params.put( "CUST_ID" , preferenceManager.getString(UNIQUE_ID));
            params.put( "MOBILE_NO" , paytmphone);
            params.put( "EMAIL" , paytmemail);
            params.put( "CHANNEL_ID" , "WAP");
            params.put( "TXN_AMOUNT" , paytmamount);
            params.put( "WEBSITE" , Config.WEBSITE);
            params.put( "INDUSTRY_TYPE_ID" , Config.INDUSTRY_TYPE_ID);
            params.put( "CALLBACK_URL", Config.CALLBACK_URL + paytmorder_id);

            /*// getting JSON string from URL
            JSONObject json = jsonParser.makeHttpRequest(urlpaytmchecksum, "POST", params);

            // Check your log cat for JSON reponse

            if(json != null){
                try {

                    paytmchecksumhash=json.has("CHECKSUMHASH")?json.getString("CHECKSUMHASH"):"";

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/
            JSONObject json = new JSONObject(params);
            Log.d("raw",json.toString());

            final String data = json.toString();
            RequestQueue requestQueue = Volley.newRequestQueue(MyWalletActivity.this);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, GENERATE_CHECKSUM, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if(response!=null){
                        try{

                            JSONObject jsonObject = new JSONObject(response);
                            paytmchecksumhash = jsonObject.getString("CHECKSUMHASH");
                            //Toast.makeText(MyWalletActivity.this, paytmchecksumhash, Toast.LENGTH_SHORT).show();
                            Log.d("check_sum",paytmchecksumhash);
                            psExec(paytmchecksumhash);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("checksum_error",error.toString());
                    pDialog.dismiss();
                    UImsgs.showToast(MyWalletActivity.this,"Sorry for inconvenience");
                }
            }){

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type","application/json");
                    map.put("X_AUTH_TOKEN", storedUserToken);

                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");

                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);



            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void psExec(final String check_sum) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /*
                      Updating parsed JSON data into ListView
                     */
                    try {

                        Map<String, String> paramMap = new HashMap<>();
                        paramMap.put( "MID" , Config.MID);
                        // Key in your staging and production MID available in your dashboard

                        paramMap.put( "ORDER_ID" , paytmorder_id);
                        paramMap.put( "CUST_ID" ,preferenceManager.getString(UNIQUE_ID));
                        paramMap.put( "MOBILE_NO" , paytmphone);
                        paramMap.put( "EMAIL" , paytmemail);
                        paramMap.put( "CHANNEL_ID" , "WAP");
                        paramMap.put( "TXN_AMOUNT" , paytmamount);
                        paramMap.put( "WEBSITE" , Config.WEBSITE);
                        paramMap.put( "INDUSTRY_TYPE_ID" , Config.INDUSTRY_TYPE_ID);
                        paramMap.put( "CALLBACK_URL", Config.CALLBACK_URL + paytmorder_id);
                        paramMap.put( "CHECKSUMHASH" , check_sum);

                        JSONObject json = new JSONObject(paramMap);
                        Log.d("ps_data",json.toString());
                        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);

                         //For Staging environment:
                        PaytmPGService Service = PaytmPGService.getStagingService();

                        // For Production environment:
                        /* PaytmPGService Service = PaytmPGService.getProductionService();*/

                        Service.initialize(Order, null);

                        Service.startPaymentTransaction(MyWalletActivity.this, true, true, new PaytmPaymentTransactionCallback() {
                            /*Call Backs*/
                            @Override
                            public void someUIErrorOccurred(String inErrorMessage) {
                                Toast.makeText(getApplicationContext(), "UI Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onTransactionResponse(Bundle inResponse) {

                                /*
                                [{STATUS=TXN_SUCCESS,
                                * CHECKSUMHASH=I+y2yA66V0M392XFC+N8yyE6zaxuNlBBH0qfwV0W+7NIQ5y+mIhzJvjLqwMuzxnWrSwcHR8GU72YuyP3Y8krqr70L5fm/YA7uNWi1pqTGFI=,
                                * BANKNAME=Kotak Mahindra Bank,
                                * ORDERID=SDSM0147-8504,
                                * TXNAMOUNT=1.00,
                                * TXNDATE=2019-11-21 16:04:57.0,
                                * MID=XuKyiM22934247445508,
                                * TXNID=20191121111212800110168215401013951,
                                * RESPCODE=01,
                                * PAYMENTMODE=DC,
                                * BANKTXNID=777001262818446,
                                * CURRENCY=INR,
                                * GATEWAYNAME=HDFC,
                                * RESPMSG=Txn Success}]
                                */

                                // getting JSON string from URL
                                Log.d("Response" , inResponse.toString());
                                JSONObject json = null;
                                try {
                                    String resstatus=inResponse.getString("STATUS");
                                    Toast.makeText(MyWalletActivity.this, resstatus, Toast.LENGTH_LONG).show();
                                    if(resstatus.equalsIgnoreCase("TXN_SUCCESS")) {

                                        instaorderid = inResponse.getString("ORDERID");
                                        instatxnid = inResponse.getString("TXNID");
                                        addamount = inResponse.getString("TXNAMOUNT");
                                        instapaymentid = inResponse.getString("CHECKSUMHASH");
                                        instatoken = inResponse.getString("MID");

                                        Map<String,String> responseMap = new HashMap<>();
                                        responseMap.put("TXNID",inResponse.getString("TXNID"));
                                        responseMap.put("BANKTXNID",inResponse.getString("BANKTXNID"));
                                        responseMap.put("ORDERID",inResponse.getString("ORDERID"));
                                        responseMap.put("TXNAMOUNT",inResponse.getString("TXNAMOUNT"));
                                        responseMap.put("STATUS",inResponse.getString("STATUS"));
                                        responseMap.put("TXNTYPE"," ");
                                        responseMap.put("GATEWAYNAME",inResponse.getString("GATEWAYNAME"));
                                        responseMap.put("RESPCODE",inResponse.getString("RESPCODE"));
                                        responseMap.put("RESPMSG",inResponse.getString("RESPMSG"));
                                        responseMap.put("BANKNAME",inResponse.getString("BANKNAME"));
                                        responseMap.put("MID",inResponse.getString("MID"));
                                        responseMap.put("PAYMENTMODE",inResponse.getString("PAYMENTMODE"));
                                        responseMap.put("REFUNDAMT","0.0");
                                        responseMap.put("TXNDATE",inResponse.getString("TXNDATE"));
                                        responseMap.put("DESC","Adding to SDSN Wallet");

                                        JSONObject jsonObject = new JSONObject(responseMap);
                                        paymentSuccess(jsonObject);

                                        /*
                                        * {
                                            "TXNID":"20180926111212800110168766100018551",
                                            "BANKTXNID":"5583250",
                                            "ORDERID":"order1",
                                            "TXNAMOUNT":"100.12",
                                            "STATUS":"TXN_SUCCESS",
                                            "TXNTYPE":"SALE",
                                            "GATEWAYNAME":"WALLET",
                                            "RESPCODE":"01",
                                            "RESPMSG":"Txn Success",
                                            "BANKNAME":"WALLET",
                                            "MID":"rxazcv89315285244163",
                                            "PAYMENTMODE":"PPI",
                                            "REFUNDAMT":"0.00",
                                            "TXNDATE":"2018-09-26 13:50:57.0"
                                            "DESC" : "sasdf"
                                            }
                                        * */

                                        // Loading jsonarray in Background Thread
                                       // new OneLoadAllProducts().execute();
                                       // Toast.makeText(MyWalletActivity.this, resstatus, Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(MyWalletActivity.this, e+"", Toast.LENGTH_SHORT).show();

                                }

//        Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void networkNotAvailable() {
                                Toast.makeText(getApplicationContext(), "Network connection error: Check your internet connectivity", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void clientAuthenticationFailed(String inErrorMessage) {
                                Toast.makeText(getApplicationContext(), "Authentication failed: Server error" + inErrorMessage, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                                Toast.makeText(getApplicationContext(), "Unable to load webpage " + inErrorMessage, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onBackPressedCancelTransaction() {
                                Toast.makeText(getApplicationContext(), "Transaction cancelled", Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                                Toast.makeText(getApplicationContext(), "Transaction Cancelled" + inResponse.toString(), Toast.LENGTH_LONG).show();
                                Log.d("Transaction Cancelled" , inResponse.toString());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        }

    }


    private void paymentSuccess(JSONObject json){
        final String data = json.toString();

        Log.d("Succes_json",data);

        RequestQueue requestQueue = Volley.newRequestQueue(MyWalletActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, /*PAYMENT_SUCCESS*/"SUBSCRIBE", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("Success_mss",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status_code");
                    if(status.equalsIgnoreCase("200")){
                        String message = jsonObject.getString("message");
                        Toast.makeText(MyWalletActivity.this, message, Toast.LENGTH_LONG).show();
                        walletBalance.setText("₹ "+jsonObject.getString("data"));
                        preferenceManager.putString(WALLET,jsonObject.getString("data"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MyWalletActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", storedUserToken);

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }




    /*@Override
    public void someUIErrorOccurred(String inErrorMessage) {
        Toast.makeText(getApplicationContext(), "UI Error " + inErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionResponse(Bundle inResponse) {

        // getting JSON string from URL
        JSONObject json = null;
        try {
            String resstatus=inResponse.getString("STATUS");

            if(resstatus.equalsIgnoreCase("TXN_SUCCESS")) {

                instaorderid = inResponse.getString("ORDERID");
                instatxnid = inResponse.getString("TXNID");
                addamount = inResponse.getString("TXNAMOUNT");
                instapaymentid = inResponse.getString("CHECKSUMHASH");
                instatoken = inResponse.getString("MID");

                // Loading jsonarray in Background Thread
                new OneLoadAllProducts().execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

//        Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(getApplicationContext(), "Network connection error: Check your internet connectivity", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String inErrorMessage) {
        Toast.makeText(getApplicationContext(), "Authentication failed: Server error" + inErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
        Toast.makeText(getApplicationContext(), "Unable to load webpage " + inErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Toast.makeText(getApplicationContext(), "Transaction cancelled", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
        Toast.makeText(getApplicationContext(), "Transaction Cancelled" + inResponse.toString(), Toast.LENGTH_LONG).show();
    }*/

//    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
//        final Activity activity = this;
//        instamojoPay = new InstamojoPay();
//        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
//        registerReceiver(instamojoPay, filter);
//        JSONObject pay = new JSONObject();
//        try {
//            pay.put("email", email);
//            pay.put("phone", phone);
//            pay.put("purpose", purpose);
//            addamount=amount;
//            pay.put("amount", amount);
//            pay.put("name", buyername);
//            pay.put("send_sms", true);
//            pay.put("send_email", true);
//        } catch (JSONException e) {
//            e.printStackTrace();
//            System.out.println("Rjn_instamojo_error"+e.getMessage());
//        }
//        initListener();
//        instamojoPay.start(activity, pay, listener);
//    }
//
//    private void initListener() {
//        listener = new InstapayListener() {
//            @Override
//            public void onSuccess(String response) {
//                System.out.println("Rjn_payment"+response);
//
//                String[] str = response.split(":");
//                String[] split = str[1].split("=");
//                instaorderid = split[1];
//                split = str[2].split("=");
//                instatxnid = split[1];
//                split = str[3].split("=");
//                instapaymentid = split[1];
//                str = str[4].split("=");
//                instatoken = str[1];
//
//                // Loading jsonarray in Background Thread
//                new OneLoadAllProducts().execute();
//            }
//
//            @Override
//            public void onFailure(int code, String reason) {
//                System.out.println("Rjn_payment_error"+"code:"+code+"reason:"+reason);
//                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
//                        .show();
//            }
//        };
//    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddMoneyFragment(), "Add Money");
        //adapter.addFragment(new WithdrawFragment(), "Withdraw");
        adapter.addFragment(new TransactionsFragment(), "Transactions");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    class OneLoadAllProducts extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MyWalletActivity.this);
            pDialog.setMessage("Loading Please wait...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url
         * */
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected String doInBackground(String... args) {
            // Building Parameters
            Map<String, String> params = new HashMap<>();
            params.put(TAG_USERID, preferenceManager.getString(TAG_USERID));
            params.put("addamount", addamount);
            params.put(TAG_INSTA_ORDERID, instaorderid);
            params.put(TAG_INSTA_TXNID, instatxnid);
            params.put(TAG_INSTA_PAYMENTID, instapaymentid);
            params.put(TAG_INSTA_TOKEN, instatoken);
            params.put("status", "Add Money Success");

            // getting JSON string from URL
            JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);

            // Check your log cat for JSON reponse
//            Log.d("All jsonarray: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                success = json.getInt(TAG_SUCCESS);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /*
                      Updating parsed JSON data into ListView
                     */
                    if (success == 1) {
                        // jsonarray found
                        // Getting Array of jsonarray
                        String s = addamount;
                        double d = Double.parseDouble(s);
                        int i = (int) d;

                        int bal = Integer.parseInt(preferenceManager.getString(TAG_USERBALANCE))+ i;
                        preferenceManager.putString(TAG_USERBALANCE, Integer.toString(bal));

                        Intent intent = new Intent(MyWalletActivity.this, UserProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);


                        Toast.makeText(MyWalletActivity.this,"Payment done. Now join match", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MyWalletActivity.this,"Something went wrong. Try again!", Toast.LENGTH_LONG).show();

                    }

                }
            });

        }

    }

}
