package com.apticks.nextclickuser.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse.BookingsDoctorsHistoryResponse;
import com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse.BookingItemDetails;
import com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse.ServiceItem;
import com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse.ServiceTimings;
import com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse.ViewDoctorBookingDetails;
import com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse.ViewDoctorBookingsResponse;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.VIEW_BOOKINGS_HISTORY_DETAILS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ViewDoctorBookingActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBackArrow;
    private ServiceTimings serviceTimings;
    private PreferenceManager preferenceManager;
    private TextView tvBookingId, tvBookingStatus, tvQty, tvTotal, tvAppointmentDate, tvServiceTimings, tvName, tvDescription;

    private LinkedList<BookingItemDetails> listOfBookingItemDetails;

    private String bookingId = "";
    private String serviceId = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_view_doctor_booking);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.booking_id)))
                bookingId = bundle.getString(getString(R.string.booking_id));
            if (bundle.containsKey(getString(R.string.service_id)))
                serviceId = bundle.getString(getString(R.string.service_id));
        }
    }

    private void initializeUi() {
        tvQty = findViewById(R.id.tvQty);
        tvName = findViewById(R.id.tvName);
        tvTotal = findViewById(R.id.tvTotal);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvBookingId = findViewById(R.id.tvBookingId);
        tvDescription = findViewById(R.id.tvDescription);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        tvServiceTimings = findViewById(R.id.tvServiceTimings);
        tvAppointmentDate = findViewById(R.id.tvAppointmentDate);

        preferenceManager = new PreferenceManager(this);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        DialogOpener.dialogOpener(this);
        String token = preferenceManager.getString(TOKEN_KEY);
        String url = VIEW_BOOKINGS_HISTORY_DETAILS + "" + bookingId;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service_id", serviceId);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        ViewDoctorBookingsResponse viewDoctorBookingsResponse = new Gson().fromJson(response.toString(), ViewDoctorBookingsResponse.class);
                        if (viewDoctorBookingsResponse != null) {
                            boolean status = viewDoctorBookingsResponse.getStatus();
                            String message = viewDoctorBookingsResponse.getMessage();
                            if (status) {
                                ViewDoctorBookingDetails viewDoctorBookingDetails = viewDoctorBookingsResponse.getViewDoctorBookingDetails();
                                if (viewDoctorBookingDetails != null) {
                                    int bookingStatus = viewDoctorBookingDetails.getBookingStatus();
                                    int total = viewDoctorBookingDetails.getTotal();
                                    int trackId = viewDoctorBookingDetails.getTrackId();
                                    listOfBookingItemDetails = viewDoctorBookingDetails.getListOfBookingItemDetails();

                                    if (listOfBookingItemDetails != null) {
                                        if (listOfBookingItemDetails.size() != 0) {
                                            BookingItemDetails bookingItemDetails = listOfBookingItemDetails.get(0);
                                            ServiceItem serviceItem = bookingItemDetails.getServiceItem();

                                            String name = serviceItem.getName();
                                            String description = serviceItem.getDesc();
                                            tvQty.setText(String.valueOf(bookingItemDetails.getQty()));
                                            serviceTimings = bookingItemDetails.getServiceTimings();
                                            String startTime = serviceTimings.getStartTime();
                                            String endTime = serviceTimings.getEndTime();
                                            String appointmentDate = bookingItemDetails.getBookingDate();

                                            if (bookingStatus == 0) {
                                                tvBookingStatus.setText("Cancelled");
                                            } else if (bookingStatus == 1) {
                                                tvBookingStatus.setText("Received");
                                            } else if (bookingStatus == 2) {
                                                tvBookingStatus.setText("Accepted");
                                            } else if (bookingStatus == 3) {
                                                tvBookingStatus.setText("Servicing");
                                            } else if (bookingStatus == 4) {
                                                tvBookingStatus.setText("Completed");
                                            } else if (bookingStatus == 5) {
                                                tvBookingStatus.setText("Rejected");
                                            }

                                            tvName.setText(name);
                                            tvDescription.setText(description);
                                            tvBookingId.setText(String.valueOf(trackId));
                                            tvTotal.setText(String.valueOf(total));
                                            tvServiceTimings.setText(startTime + " - " + endTime);
                                            tvAppointmentDate.setText(appointmentDate);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(ViewDoctorBookingActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    DialogOpener.dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DialogOpener.dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("X_AUTH_TOKEN", token);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            RetryPolicy policy = new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
