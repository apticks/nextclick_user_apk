package com.apticks.nextclickuser.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.BookingsTabAdapter;
import com.apticks.nextclickuser.Fragments.DoctorsBookingHistoryFragment;
import com.apticks.nextclickuser.Fragments.OnDemandServiceBookingHistoryFragment;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.getServicesResponse.GetServiceDetails;
import com.apticks.nextclickuser.Pojo.getServicesResponse.GetServicesResponse;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utils.UserData;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.apticks.nextclickuser.Config.Config.GET_SERVICES;

public class BookingHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabLayout;
    private ImageView ivBackArrow;

    private LinkedList<GetServiceDetails> listOfGetServiceDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_booking_history);
        initializeUi();
        initializeListeners();
        prepareServicesDetails();
    }

    private void initializeUi() {
        tabLayout = findViewById(R.id.tabLayout);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareServicesDetails() {
        DialogOpener.dialogOpener(this);
        String url = GET_SERVICES;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    GetServicesResponse getServicesResponse = new Gson().fromJson(response, GetServicesResponse.class);
                    if (getServicesResponse != null) {
                        boolean status = getServicesResponse.getStatus();
                        String message = getServicesResponse.getMessage();
                        if (status) {
                            listOfGetServiceDetails = getServicesResponse.getListOfGetServiceDetails();
                            if (listOfGetServiceDetails != null) {
                                if (listOfGetServiceDetails.size() != 0) {
                                    UserData.getInstance().setListOfGetServiceDetails(listOfGetServiceDetails);
                                    initializeTabs();
                                } else {
                                    initializeTabs();
                                }
                            } else {
                                initializeTabs();
                            }
                        } else {
                            Toast.makeText(BookingHistoryActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                DialogOpener.dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void initializeTabs() {
        BookingsTabAdapter bookingsTabAdapter = new BookingsTabAdapter(this, getSupportFragmentManager(), tabLayout, R.id.fragmentContainer);
        bookingsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.doctors)).setTag(getResources().getString(R.string.doctors)), DoctorsBookingHistoryFragment.class, null);
        bookingsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.on_demand_service)).setTag(getResources().getString(R.string.on_demand_service)), OnDemandServiceBookingHistoryFragment.class, null);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
