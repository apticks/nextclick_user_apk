package com.apticks.nextclickuser.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Adapters.ServiceTimingsAdapter;
import com.apticks.nextclickuser.Constants.Constants;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.Data;
import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.DoctorDetailsResponse;
import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.ServiceTiming;
import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.Speciality;
import com.apticks.nextclickuser.Pojo.onDemandServicesResponse.OdCategory;
import com.apticks.nextclickuser.Pojo.onDemandServicesResponse.OnDemandServicesResponse;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.interfaces.SelectedServiceTimingsCallBack;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.BOOKINGS_CHECK_OUT;
import static com.apticks.nextclickuser.Config.Config.GET_DOCTORS;
import static com.apticks.nextclickuser.Config.Config.GET_ONDEMAND_SERVICE_PERSONS;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class GetAppointmentAndGetServiceActivity extends AppCompatActivity implements View.OnClickListener, SelectedServiceTimingsCallBack, DatePickerDialog.OnDateSetListener {

    private RecyclerView rvTimeSlots;
    private Transformation transformation;
    private ImageView ivBackArrow, ivDoctorPic;
    private PreferenceManager preferenceManager;
    private LinearLayout llAppointment, llServiceTimings, llHolidays;
    private TextView tvDoctorName, tvDoctorExperience, tvDoctorQualification, tvDoctorSpecialization, tvPrice, tvStrikePrice, tvLanguages, tvHolidays, tvAppointmentDate, tvSubTotal, tvTax, tvTotal, tvBookNow;

    private LinkedList<Integer> listOfHolidays;
    private LinkedList<ServiceTiming> listOfServiceTimings;

    private String id = "";
    private String type = "";
    private String token = "";
    private String vendorId = "";
    private String serviceId = "";
    private String specialityId = "";
    private String appointmentDate = "";

    private int selectedServiceTimeId = -1;

    private int fee;
    private double doctorFee;
    private double totalAmount;
    private double discountPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_appointment_and_get_service);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra(VENDOR_ID))
            vendorId = intent.getStringExtra(VENDOR_ID);
        if (intent.hasExtra("s_id"))
            id = intent.getStringExtra("s_id");
        if (intent.hasExtra("od_cat_id"))
            specialityId = intent.getStringExtra("od_cat_id");
        if (intent.hasExtra("type"))
            type = intent.getStringExtra("type");
        if (intent.hasExtra(getString(R.string.service_id)))
            serviceId = intent.getStringExtra(getString(R.string.service_id));
    }

    private void initializeUi() {
        tvTax = findViewById(R.id.tvTax);
        tvTotal = findViewById(R.id.tvTotal);
        tvPrice = findViewById(R.id.tvPrice);
        tvBookNow = findViewById(R.id.tvBookNow);
        tvHolidays = findViewById(R.id.tvHolidays);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        llHolidays = findViewById(R.id.llHolidays);
        rvTimeSlots = findViewById(R.id.rvTimeSlots);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        ivDoctorPic = findViewById(R.id.ivDoctorPic);
        tvLanguages = findViewById(R.id.tvLanguages);
        tvDoctorName = findViewById(R.id.tvDoctorName);
        llAppointment = findViewById(R.id.llAppointment);
        tvStrikePrice = findViewById(R.id.tvStrikePrice);
        llServiceTimings = findViewById(R.id.llServiceTimings);
        tvAppointmentDate = findViewById(R.id.tvAppointmentDate);
        tvDoctorExperience = findViewById(R.id.tvDoctorExperience);
        tvDoctorQualification = findViewById(R.id.tvDoctorQualification);
        tvDoctorSpecialization = findViewById(R.id.tvDoctorSpecialization);

        preferenceManager = new PreferenceManager(this);
        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    private void initializeListeners() {
        tvBookNow.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        llAppointment.setOnClickListener(this);
        tvAppointmentDate.setOnClickListener(this);
    }

    private void prepareDetails() {
        DialogOpener.dialogOpener(this);
        String url = "";
        if (type.equalsIgnoreCase(getString(R.string.hospital))) {
            llHolidays.setVisibility(View.VISIBLE);
            listOfHolidays = new LinkedList<>();
            url = GET_DOCTORS + vendorId + "/" + specialityId + "/" + id;
        } else if (type.equalsIgnoreCase(getString(R.string.on_demand_services))) {
            llHolidays.setVisibility(View.GONE);
            listOfHolidays = new LinkedList<>();
            url = GET_ONDEMAND_SERVICE_PERSONS + vendorId + "/" + specialityId + "/" + id;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                if (type.equalsIgnoreCase(getString(R.string.hospital))) {
                    if (response != null) {
                        DoctorDetailsResponse doctorDetailsResponse = new Gson().fromJson(response, DoctorDetailsResponse.class);
                        if (doctorDetailsResponse != null) {
                            boolean status = doctorDetailsResponse.getStatus();
                            String message = doctorDetailsResponse.getMessage();
                            if (status) {
                                if (doctorDetailsResponse != null) {
                                    Data data = doctorDetailsResponse.getData();
                                    if (data != null) {
                                        fee = data.getFee();
                                        String name = data.getName();
                                        int discount = data.getDiscount();
                                        String imageUrl = data.getImage();
                                        String description = data.getDesc();
                                        double experience = data.getExperience();
                                        String qualification = data.getQualification();
                                        listOfHolidays = data.getListOfHolidays();
                                        LinkedList<String> listOfLanguages = data.getListOfLanguages();
                                        listOfServiceTimings = data.getListOfServiceTimings();

                                        discountPrice = fee * (discount / 100.00);
                                        doctorFee = fee - discountPrice;

                                        Speciality speciality = data.getSpeciality();
                                        String specialityName = "";
                                        if (speciality != null) {
                                            specialityName = speciality.getName();
                                        }

                                        tvDoctorName.setText(name);
                                        tvDoctorExperience.setText(String.valueOf(experience) + "Years of exp.");
                                        tvDoctorQualification.setText(qualification);
                                        tvDoctorSpecialization.setText(specialityName);
                                        tvPrice.setText("₹" + " " + doctorFee);
                                        tvStrikePrice.setText("₹" + " " + fee);


                                        tvSubTotal.setText("₹" + " " + doctorFee);
                                        tvTax.setText("0%");

                                        //double taxAmount = doctorFee * (18 / 100.00);
                                        double taxAmount = 0.0;
                                        totalAmount = doctorFee + taxAmount;
                                        tvTotal.setText("₹" + " " + totalAmount);

                                        if (listOfLanguages != null) {
                                            if (listOfLanguages.size() != 0) {
                                                StringBuilder stringBuilder = new StringBuilder();
                                                for (int languagesIndex = 0; languagesIndex < listOfLanguages.size(); languagesIndex++) {
                                                    stringBuilder.append(listOfLanguages.get(languagesIndex)).append("\n");
                                                }
                                                tvLanguages.setText(stringBuilder.toString());
                                            } else {
                                                tvLanguages.setText("N/A");
                                            }
                                        } else {
                                            tvLanguages.setText("N/A");
                                        }

                                        if (listOfHolidays != null) {
                                            if (listOfHolidays.size() != 0) {
                                                StringBuilder stringBuilder = new StringBuilder();
                                                for (int holidayIndex = 0; holidayIndex < listOfHolidays.size(); holidayIndex++) {
                                                    int holidayInt = listOfHolidays.get(holidayIndex);
                                                    if (holidayInt == 1) {
                                                        stringBuilder.append("Sunday").append("\n");
                                                    } else if (holidayInt == 2) {
                                                        stringBuilder.append("Monday").append("\n");
                                                    } else if (holidayInt == 3) {
                                                        stringBuilder.append("Tuesday").append("\n");
                                                    } else if (holidayInt == 4) {
                                                        stringBuilder.append("Wednesday").append("\n");
                                                    } else if (holidayInt == 5) {
                                                        stringBuilder.append("Thursday").append("\n");
                                                    } else if (holidayInt == 6) {
                                                        stringBuilder.append("Friday").append("\n");
                                                    } else if (holidayInt == 7) {
                                                        stringBuilder.append("Saturday").append("\n");
                                                    }
                                                }
                                                tvHolidays.setText(stringBuilder.toString());
                                            } else {
                                                tvHolidays.setText("N/A");
                                            }
                                        } else {
                                            tvHolidays.setText("N/A");
                                        }

                                        if (listOfServiceTimings != null) {
                                            if (listOfServiceTimings.size() != 0) {
                                                llServiceTimings.setVisibility(View.VISIBLE);
                                                initializeServiceTimingsAdapter();
                                            } else {
                                                llServiceTimings.setVisibility(View.GONE);
                                            }
                                        } else {
                                            llServiceTimings.setVisibility(View.GONE);
                                        }

                                        if (imageUrl != null) {
                                            if (!imageUrl.isEmpty()) {
                                                Picasso.get()
                                                        .load(imageUrl)
                                                        .error(R.drawable.ic_default_place_holder)
                                                        .placeholder(R.drawable.ic_default_place_holder)
                                                        .transform(transformation)
                                                        .fit().centerCrop()
                                                        .into(ivDoctorPic);
                                            } else {
                                                Picasso.get()
                                                        .load(R.drawable.ic_default_place_holder)
                                                        .error(R.drawable.ic_default_place_holder)
                                                        .placeholder(R.drawable.ic_default_place_holder)
                                                        .transform(transformation)
                                                        .fit().centerCrop()
                                                        .into(ivDoctorPic);
                                            }
                                        } else {
                                            Picasso.get()
                                                    .load(R.drawable.ic_default_place_holder)
                                                    .error(R.drawable.ic_default_place_holder)
                                                    .placeholder(R.drawable.ic_default_place_holder)
                                                    .transform(transformation)
                                                    .fit().centerCrop()
                                                    .into(ivDoctorPic);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(GetAppointmentAndGetServiceActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    if (response != null) {
                        OnDemandServicesResponse onDemandServicesResponse = new Gson().fromJson(response, OnDemandServicesResponse.class);
                        if (onDemandServicesResponse != null) {
                            boolean status = onDemandServicesResponse.getStatus();
                            String message = onDemandServicesResponse.getMessage();
                            if (status) {
                                com.apticks.nextclickuser.Pojo.onDemandServicesResponse.Data data = onDemandServicesResponse.getData();
                                if (data != null) {
                                    String name = data.getName();
                                    String desc = data.getDesc();
                                    int serviceDuration = data.getServiceDuration();
                                    String imageUrl = data.getImage();
                                    String categoryName = "";
                                    String categoryImageUrl = "";
                                    String categoryDescription = "";
                                    OdCategory odCategory = data.getOdCategory();
                                    listOfServiceTimings = data.getListOfServiceTiming();
                                    if (odCategory != null) {
                                        categoryName = odCategory.getName();
                                        categoryDescription = odCategory.getDesc();
                                        categoryImageUrl = odCategory.getImage();
                                    }

                                    int discount = data.getDiscount();
                                    fee = data.getPrice();

                                    discountPrice = fee * (discount / 100.00);
                                    doctorFee = fee - discountPrice;


                                    tvDoctorName.setText(name);
                                    tvLanguages.setText("N/A");
                                    tvDoctorExperience.setText(String.valueOf(serviceDuration) + "Day(s).");
                                    tvDoctorQualification.setText(desc);
                                    tvDoctorSpecialization.setText(categoryName);
                                    tvPrice.setText("₹" + " " + doctorFee);
                                    tvStrikePrice.setText("₹" + " " + fee);


                                    tvSubTotal.setText("₹" + " " + doctorFee);
                                    tvTax.setText("0%");


                                    //double taxAmount = doctorFee * (18 / 100.00);
                                    double taxAmount = 0.0;
                                    totalAmount = doctorFee + taxAmount;
                                    tvTotal.setText("₹" + " " + totalAmount);

                                    if (listOfServiceTimings != null) {
                                        if (listOfServiceTimings.size() != 0) {
                                            llServiceTimings.setVisibility(View.VISIBLE);
                                            initializeServiceTimingsAdapter();
                                        } else {
                                            llServiceTimings.setVisibility(View.GONE);
                                        }
                                    } else {
                                        llServiceTimings.setVisibility(View.GONE);
                                    }


                                    if (imageUrl != null) {
                                        if (!imageUrl.isEmpty()) {
                                            Picasso.get()
                                                    .load(imageUrl)
                                                    .error(R.drawable.ic_default_place_holder)
                                                    .placeholder(R.drawable.ic_default_place_holder)
                                                    .transform(transformation)
                                                    .fit().centerCrop()
                                                    .into(ivDoctorPic);
                                        } else {
                                            Picasso.get()
                                                    .load(R.drawable.ic_default_place_holder)
                                                    .error(R.drawable.ic_default_place_holder)
                                                    .placeholder(R.drawable.ic_default_place_holder)
                                                    .transform(transformation)
                                                    .fit().centerCrop()
                                                    .into(ivDoctorPic);
                                        }
                                    } else {
                                        Picasso.get()
                                                .load(R.drawable.ic_default_place_holder)
                                                .error(R.drawable.ic_default_place_holder)
                                                .placeholder(R.drawable.ic_default_place_holder)
                                                .transform(transformation)
                                                .fit().centerCrop()
                                                .into(ivDoctorPic);
                                    }
                                }
                            } else {
                                Toast.makeText(GetAppointmentAndGetServiceActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                DialogOpener.dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void initializeServiceTimingsAdapter() {
        ServiceTimingsAdapter serviceTimingsAdapter = new ServiceTimingsAdapter(this, listOfServiceTimings, selectedServiceTimeId);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvTimeSlots.setLayoutManager(layoutManager);
        rvTimeSlots.setItemAnimator(new DefaultItemAnimator());
        rvTimeSlots.setAdapter(serviceTimingsAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.tvAppointmentDate:
                showDatePicker();
                break;
            case R.id.tvBookNow:
                prepareBookNowDetails();
                break;
            case R.id.llAppointment:
                showDatePicker();
                break;
            default:
                break;
        }
    }

    private void prepareBookNowDetails() {
        if (!appointmentDate.isEmpty()) {
            if (selectedServiceTimeId != -1) {
                serviceCallForBookNowDetails();
            } else {
                Toast.makeText(this, getString(R.string.please_select_time_slot), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_appointment_date), Toast.LENGTH_SHORT).show();
        }
    }

    private void serviceCallForBookNowDetails() {
        DialogOpener.dialogOpener(this);
        token = preferenceManager.getString(TOKEN_KEY);
        String url = BOOKINGS_CHECK_OUT;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service_id", serviceId);
            jsonObject.put("vendor_id", vendorId);
            jsonObject.put("payment_method_id", "1");
            jsonObject.put("sub_total", fee);
            jsonObject.put("promo_id", null);
            jsonObject.put("promo_discount", "");
            jsonObject.put("discount", discountPrice);
            jsonObject.put("tax", "");
            jsonObject.put("used_wallet_amount", "0");
            jsonObject.put("total", totalAmount);

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObjectServices = new JSONObject();
            jsonObjectServices.put("service_id", serviceId);
            jsonObjectServices.put("service_item_id", id);
            jsonObjectServices.put("service_timing_id", selectedServiceTimeId);
            jsonObjectServices.put("booking_date", tvAppointmentDate.getText().toString());
            jsonObjectServices.put("price", doctorFee);
            jsonObjectServices.put("qty", 1);
            jsonObjectServices.put("discount", discountPrice);
            jsonObjectServices.put("total", totalAmount);
            jsonArray.put(jsonObjectServices);
            jsonObject.put("services", jsonArray);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        //{"status":true,"http_code":201,"message":"Service booked..!","data":false}
                        try {
                            boolean status = response.getBoolean("status");
                            String message = response.getString("message");
                            if (status) {
                                Intent intent = new Intent(GetAppointmentAndGetServiceActivity.this, MainCategoriesActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Toast.makeText(GetAppointmentAndGetServiceActivity.this, message, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(GetAppointmentAndGetServiceActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                    DialogOpener.dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DialogOpener.dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("X_AUTH_TOKEN", token);
                    return headers;

                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            RetryPolicy policy = new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void showDatePicker() {
        Calendar calender = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DateAndTimePicker, this, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH));
        if (!datePickerDialog.isShowing())
            datePickerDialog.show();
        else
            datePickerDialog.dismiss();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (datePickerDialog.getWindow() != null)
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    @Override
    public void selectedServiceTimings(int selectedServiceTimeId) {
        this.selectedServiceTimeId = selectedServiceTimeId;
    }

    @Override
    @SuppressLint("SimpleDateFormat")
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String dateString = String.format("%s-%s-%s", String.valueOf(year), String.valueOf(month + 1), String.valueOf(dayOfMonth));
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = inFormat.parse(dateString);
            if (date != null) {
                SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
                String selectedDayName = outFormat.format(date);
                int selectedDayId = 0;
                if (selectedDayName.equalsIgnoreCase("Sunday")) {
                    selectedDayId = 1;
                } else if (selectedDayName.equalsIgnoreCase("Monday")) {
                    selectedDayId = 2;
                } else if (selectedDayName.equalsIgnoreCase("Tuesday")) {
                    selectedDayId = 3;
                } else if (selectedDayName.equalsIgnoreCase("Wednesday")) {
                    selectedDayId = 4;
                } else if (selectedDayName.equalsIgnoreCase("Thursday")) {
                    selectedDayId = 5;
                } else if (selectedDayName.equalsIgnoreCase("Friday")) {
                    selectedDayId = 6;
                } else if (selectedDayName.equalsIgnoreCase("Saturday")) {
                    selectedDayId = 7;
                }
                if (!listOfHolidays.contains(selectedDayId)) {
                    appointmentDate = dateString;
                    tvAppointmentDate.setText(appointmentDate);
                } else {
                    Toast.makeText(this, getString(R.string.service_was_not_available_on_that_day_please_select_another_date), Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}