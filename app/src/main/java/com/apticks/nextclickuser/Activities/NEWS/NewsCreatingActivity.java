package com.apticks.nextclickuser.Activities.NEWS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.Utility;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.NewsPOJO.NewsCatPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS_C;
import static com.apticks.nextclickuser.Config.Config.NEWS_CATEGORIES;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class NewsCreatingActivity extends AppCompatActivity implements LocationListener {

    private CardView upload;
    private ImageView news_image,add_news_image,news_back_img;
    private EditText news_title,news_content,news_video_url;
    private String news_title_str,news_content_str,news_video_url_str,categoryID_str="";
    private Spinner category_spinner;
    private Context mContext;
    private ArrayList<NewsCatPojo> newsCatPojoList;
    private ArrayList<String> newsCategories;
    private char imageSelection;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int CAMERA_REQUEST = 10;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    public int flag = 0;
    public static String newsImageStringURL = "",location_String,token;
    private String userChoosenTask;
    LocationManager locationManager;
    Double lattitude, longitude;
    private PreferenceManager preferenceManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_creating);
        getSupportActionBar().hide();
        mContext = NewsCreatingActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token=preferenceManager.getString(TOKEN_KEY);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();
        newsCategoriesFetcher();
        add_news_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valid()){
                    uploadNews();
                }
            }
        });

        news_back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private boolean valid() {
        boolean valid = false;

        news_title_str = news_title.getText().toString().trim();
        news_content_str = news_content.getText().toString().trim();
        news_video_url_str = news_video_url.getText().toString().trim();
        categoryID_str = newsCatPojoList.get(category_spinner.getSelectedItemPosition()).getId();

        if(newsImageStringURL.length()<10){
            Toast.makeText(mContext, "Please Upload Image", Toast.LENGTH_LONG).show();
        }
        else if(news_title_str.length()<2){

            news_title.requestFocus();
            news_title.setError("Not Allowed");
        }else if(news_content_str.length()<5){

            news_content.setError("Please provide some content");
            news_content.requestFocus();
        }else {
            valid =true;
        }



        return valid;
    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            location_String =address;


        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End

//Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NewsCreatingActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(NewsCreatingActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (imageSelection == 'c') {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                news_image.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) news_image.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                newsImageStringURL = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }

        }

    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            news_image.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) news_image.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            newsImageStringURL = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+newsImageStringURL);

            flag = 1;
        }

    }

    //uploading news
    private void uploadNews(){

        DialogOpener.dialogOpener(mContext);

        Map<Object,Object> uploadMap = new HashMap<>();
        uploadMap.put("title",news_title_str);
        uploadMap.put("category",categoryID_str);
        uploadMap.put("news",news_content_str);
        uploadMap.put("video_link",news_video_url_str);
        uploadMap.put("address",location_String);
        uploadMap.put("latitude",lattitude);
        uploadMap.put("longitude",longitude);
        uploadMap.put("local_news_image",newsImageStringURL);

        JSONObject json = new JSONObject(uploadMap);

        final String data = json.toString();

        Log.d("news req",data);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOCAL_NEWS_C, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response != null){
                    try{

                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if(status && http_code == 201){
                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(mContext, "Created successfully", SUCCESS);
                            onBackPressed();
                        }else{
                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(mContext, "Unable to create", INFO);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        UImsgs.showCustomToast(mContext, "Unable to create", INFO);
                        DialogOpener.dialog.dismiss();
                    }

                }else{
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server under maintenance", INFO);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext, "Unable to create", ERROR);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void newsCategoriesFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NEWS_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("nu",response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {

                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            newsCatPojoList = new ArrayList<>();
                            newsCategories = new ArrayList<>();
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                NewsCatPojo newsCatPojo = new NewsCatPojo();
                                newsCatPojo.setId(dataObject.getString("id"));
                                newsCatPojo.setName(dataObject.getString("name"));
                                newsCatPojo.setImage(dataObject.getString("image"));
                                newsCatPojoList.add(newsCatPojo);
                                newsCategories.add(dataObject.getString("name"));
                            }
                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(NewsCreatingActivity.this,android.R.layout.simple_spinner_item, newsCategories);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            category_spinner.setAdapter(arrayAdapter);


                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    private void init() {
        upload = findViewById(R.id.upload);
        news_image = findViewById(R.id.news_image);
        add_news_image = findViewById(R.id.add_news_image);
        news_back_img = findViewById(R.id.news_back_img);
        news_title = findViewById(R.id.news_title);
        news_content = findViewById(R.id.news_content);
        news_video_url = findViewById(R.id.news_video_url);
        category_spinner = findViewById(R.id.category_spinner);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
