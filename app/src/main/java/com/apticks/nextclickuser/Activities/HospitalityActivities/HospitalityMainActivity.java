package com.apticks.nextclickuser.Activities.HospitalityActivities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.HospitalityAdapters.SpecialitiesAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.SpecialityPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;

public class HospitalityMainActivity extends AppCompatActivity {

    private String vendorId, vendorUserId;
    private Context mContext;
    private ImageView back_imageView;
    private RecyclerView hospitality_speciality_recycler;
    private ArrayList<SpecialityPojo> specialityPojoArrayList;

    private String serviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitality_main);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        try {
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra(VENDOR_ID))
                    vendorId = intent.getStringExtra(VENDOR_ID);
                if (intent.hasExtra("vendor_user_id"))
                    vendorUserId = intent.getStringExtra("vendor_user_id");
                if (intent.hasExtra(getString(R.string.service_id)))
                    serviceId = intent.getStringExtra(getString(R.string.service_id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
        getProfile();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getProfile() {
        RequestQueue requestQueue = Volley.newRequestQueue(HospitalityMainActivity.this);
        Log.d("url", INDIVIDUAL_VENDOR + vendorId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendorId, new Response.Listener<String>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(String response) {
                Log.d("vendor_response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            try {
                                String vendorUserId = dataObject.getString("vendor_user_id");
                                JSONArray specialityArray = dataObject.getJSONArray("specialities");
                                if (specialityArray.length() > 0) {
                                    specialityPojoArrayList = new ArrayList<>();
                                    for (int i = 0; i < specialityArray.length(); i++) {
                                        JSONObject specialityObject = specialityArray.getJSONObject(i);
                                        SpecialityPojo specialityPojo = new SpecialityPojo();
                                        specialityPojo.setCreated_user_id(vendorUserId);
                                        specialityPojo.setId(specialityObject.getString("id"));
                                        specialityPojo.setName(specialityObject.getString("name"));
                                        specialityPojo.setDesc(specialityObject.getString("desc"));
                                        specialityPojo.setImage(specialityObject.getString("image"));
                                        specialityPojoArrayList.add(specialityPojo);
                                    }
                                    SpecialitiesAdapter specialitiesAdapter = new SpecialitiesAdapter(mContext, specialityPojoArrayList, serviceId);
                                    GridLayoutManager layoutManager = new GridLayoutManager(mContext, 3, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }
                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }
                                    };
                                    hospitality_speciality_recycler.setLayoutManager(layoutManager);
                                    hospitality_speciality_recycler.setAdapter(specialitiesAdapter);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UImsgs.showCustomToast(mContext, "No specialities available", WARNING);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext, "Something went wrong", ERROR);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void init() {
        mContext = HospitalityMainActivity.this;
        back_imageView = findViewById(R.id.back_imageView);
        hospitality_speciality_recycler = findViewById(R.id.hospitality_speciality_recycler);
    }


}