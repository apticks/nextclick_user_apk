package com.apticks.nextclickuser.Activities.NEWS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.Activities.ALL_CATEGORIES.AllCategoriesActivity;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.USERPROFILE.UserProfileActivity;
import com.apticks.nextclickuser.Adapters.NewsAdapter;
import com.apticks.nextclickuser.Fragments.NewsFragments.NewsListingFragment;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.NewsData;
import com.apticks.nextclickuser.Pojo.NewsPOJO.NewsCatPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS;

import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS_LOCATION_BASED;
import static com.apticks.nextclickuser.Config.Config.LONGITUDE;
import static com.apticks.nextclickuser.Config.Config.MAIN_NEWS;
import static com.apticks.nextclickuser.Config.Config.NEWS_CATEGORIES;

public class NewsActivity extends AppCompatActivity {

    private Context mContext;
    private ArrayList<NewsCatPojo> newsCatPojoList;
    private TabLayout news_cat_tabs;
    private ViewPager news_cat_Viewpager;
    private FloatingActionButton create_news;
    private Switch change_switch;
    private int type;
    private boolean local = false;
    private PreferenceManager preferenceManager;
    LinearLayout main_news_layout;
    RecyclerView local_news_recycler;
    private ArrayList<NewsData> newsDataList;
    BottomNavigationView navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        getSupportActionBar().hide();
        mContext = NewsActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        type = MAIN_NEWS;
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        init();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        try{
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) navigation.getLayoutParams();
        }catch (Exception e){
            e.printStackTrace();
        }
        navigation.setSelectedItemId(R.id.navigation_news);

        preferenceManager.putInt("newsType",MAIN_NEWS);
        newsCategoriesFetcher(MAIN_NEWS);
        //localNewsFetcher();



        change_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == MAIN_NEWS) {
                    type = LOCAL_NEWS;
                    final ProgressDialog progressDialog = new ProgressDialog(NewsActivity.this);
                    progressDialog.setIcon(R.drawable.nextclick_logo_black);
                    progressDialog.setMessage("Please wait while fetching data.....");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setProgress(0);
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    change_switch.setText("Switch back to main news");
                    /*main_news_layout.setVisibility(View.GONE);
                    local_news_recycler.setVisibility(View.VISIBLE);*/
                    preferenceManager.putInt("newsType",LOCAL_NEWS);
                    newsCategoriesFetcher(LOCAL_NEWS);
                    progressDialog.dismiss();

                } else if (type == LOCAL_NEWS) {
                    type = MAIN_NEWS;
                    local = false;
                    final ProgressDialog progressDialog = new ProgressDialog(NewsActivity.this);
                    progressDialog.setIcon(R.drawable.nextclick_logo_black);
                    progressDialog.setMessage("Please wait while fetching data.....");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setProgress(0);
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    change_switch.setText("Switch to local news");
                    preferenceManager.putInt("newsType",MAIN_NEWS);
                    newsCategoriesFetcher(MAIN_NEWS);

                    /*main_news_layout.setVisibility(View.VISIBLE);
                    local_news_recycler.setVisibility(View.GONE);*/
                    progressDialog.dismiss();
                }


            }
        });

        create_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NewsCreatingActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        change_switch = findViewById(R.id.change_switch);
        news_cat_tabs = findViewById(R.id.news_cat_tabs);
        news_cat_Viewpager = findViewById(R.id.news_cat_viewpager);
        create_news = findViewById(R.id.create_news);
        main_news_layout = findViewById(R.id.main_news_layout);
        local_news_recycler = findViewById(R.id.local_news_recycler);
        navigation = (BottomNavigationView) findViewById(R.id.nav_view);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
           /* LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.custom_tab, null);
            TextView tabName = (TextView) alertLayout.findViewById(R.id.tab_name);
            tabName.setText("OverView");
            CircularImageView tabImage = (CircularImageView) alertLayout.findViewById(R.id.tab_img);
            Picasso.get().load("https://cdn.dribbble.com/users/77098/screenshots/2842978/pbt_categories_800x600.png").into(tabImage);*/
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<NewsCatPojo> subCatList,int newsType) {
        NewsActivity.ViewPagerAdapter adapter = new NewsActivity.ViewPagerAdapter(getSupportFragmentManager());


            for (int i = 0; i < subCatList.size(); i++) {
                Log.d("type list", newsType + "");
                adapter.addFragment(new NewsListingFragment(NewsActivity.this,subCatList.get(i).getId(), newsType), subCatList.get(i).getName());
            }


        /*adapter.addFragment(new OverViewFragment_HMS(vendor_id), "Overiview");

        adapter.addFragment(new DoctorsFragment_HMS(vendor_id), "Doctors");
        adapter.addFragment(new AmenitiesFragment_HMS(vendor_id), "Amenities");
        adapter.addFragment(new Review_Fragment(vendor_id), "Reviews");*/
        viewPager.setAdapter(adapter);

    }

    private void newsCategoriesFetcher(int newsType) {

        final ProgressDialog progressDialog = new ProgressDialog(NewsActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NEWS_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {

                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            newsCatPojoList = new ArrayList<>();
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                NewsCatPojo newsCatPojo = new NewsCatPojo();
                                newsCatPojo.setId(dataObject.getString("id"));
                                newsCatPojo.setName(dataObject.getString("name"));
                                newsCatPojo.setImage(dataObject.getString("image"));
                                newsCatPojoList.add(newsCatPojo);
                            }

                            setupViewPager(news_cat_Viewpager, newsCatPojoList,newsType);
                            news_cat_tabs.setupWithViewPager(news_cat_Viewpager);
                            LayoutInflater inflater = getLayoutInflater();
                            View alertLayout = null;
                            for (int i = 0; i < newsCatPojoList.size(); i++) {
                                alertLayout = inflater.inflate(R.layout.custom_news_tab, null);
                                TextView tabName = (TextView) alertLayout.findViewById(R.id.news_tab_name);
                                tabName.setText(newsCatPojoList.get(i).getName());
                                ImageView tabImage = (ImageView) alertLayout.findViewById(R.id.news_tab_img);
                                Picasso.get().load(newsCatPojoList.get(i).getImage()).into(tabImage);
                                news_cat_tabs.getTabAt(i).setCustomView(alertLayout);
                            }

                        }
                    }
                    progressDialog.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    /*private void localNewsFetcher() {

        String lat, lang;
        lat = preferenceManager.getString("lat");
        lang = preferenceManager.getString("lang");
        String newsUrl = LOCAL_NEWS_LOCATION_BASED + "?latitude" + lat + LONGITUDE + lang;

        Log.d("type", type + "".trim());
        Log.d("news url", newsUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(NewsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, newsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("localnewsresp",response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            //JSONObject dataJson = jsonObject.getJSONObject("data");
                            JSONArray resultArray = jsonObject.getJSONArray("data");
                            if (resultArray.length() > 0) {
                                newsDataList = new ArrayList<>();
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String id = resultObject.getString("id");
                                    String title = resultObject.getString("title");
                                    String video_link = resultObject.getString("video_link");
                                    String news = resultObject.getString("news");
                                    String news_date = resultObject.getString("created_at");
                                    String image = "";
                                    String views_count="0";
                                    String times_ago="";
                                    try {
                                        times_ago= resultObject.getString("times_ago");
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    try {
                                        image = resultObject.getString("image");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try{
                                        views_count = resultObject.getString("views_count");
                                        Log.d(title+"views_count",views_count);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    NewsData newsData = new NewsData();

                                    newsData.setId(id);
                                    newsData.setTitle(title);
                                    newsData.setVideo_link(video_link);
                                    newsData.setNews(news);
                                    newsData.setNews_date(news_date);
                                    newsData.setImage(image);
                                    newsData.setView_count(views_count);
                                    newsData.setTimes_ago(times_ago);
                                    newsDataList.add(newsData);

                                }
                            }
                            Log.d("size",newsDataList.size()+"");
                            NewsAdapter newsAdapter = new NewsAdapter(mContext, newsDataList, 1, newsSubCatID);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }

                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }

                            };
                            local_news_recycler.setLayoutManager(layoutManager);

                            local_news_recycler.setAdapter(newsAdapter);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        UImsgs.showToast(NewsActivity.this,"No news available");

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }*/


    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent mainIntent = new Intent(mContext, MainCategoriesActivity.class);
                    startActivity(mainIntent);
                    return true;
                case R.id.navigation_news:


                    return true;

                case R.id.navigation_category:
                    Intent categoryIntent = new Intent(mContext, AllCategoriesActivity.class);
                    startActivity(categoryIntent);
                    return true;
                case R.id.navigation_profile:
                    Intent profileIntent = new Intent(mContext, UserProfileActivity.class);
                    startActivity(profileIntent);
                    return true;
                case R.id.navigation_cart:
                    Intent intent = new Intent(mContext, CartActivity.class);
                    intent.putExtra("Type", "Food");
                    startActivity(intent);

                    return true;


            }

            return false;
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(R.id.navigation_news);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        navigation.setSelectedItemId(R.id.navigation_news);
    }
}



