package com.apticks.nextclickuser.Activities.PROFILE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.ServiceSelectionAdapter;
import com.apticks.nextclickuser.utils.UserData;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Adapters.MoreImagesAdapter;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.AmenitiesAdapter;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.ServicesAdapter;
import com.apticks.nextclickuser.Adapters.ReviewAdapters.ReviewsAdapter;
import com.apticks.nextclickuser.Adapters.SlidingImage_AdapterTemp;
import com.apticks.nextclickuser.Helpers.MaxHeightNestedScrollView;
import com.apticks.nextclickuser.Helpers.NonScrollListView;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MorePojo;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.AmenitiesPojo;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.Pojo.Review_POJO.ReviewsPojo;
import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.CategoryIDS.lead_management_service_id;
import static com.apticks.nextclickuser.Constants.Constants.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ProfileActivity extends AppCompatActivity {

    private NonScrollListView profile_amenities_recycler, profile_services_recycler, profile_reviews_recycler, more_images_recycler;
    private ArrayList<AmenitiesPojo> amenitiesList;
    private ArrayList<ServicesPojo> servicesList;
    private ArrayList<ReviewsPojo> reviewsList;
    private ArrayList<MorePojo> moreList;

    private String vendor_id, profile_whatsapp, token;
    private TextView profile_name, profile_email, profile_address, profile_website_url,
            profile_contact, profile_landmark, profile_constituency,
            profile_timings, profile_header_name, submit_review, profile_description;
    private ImageView profile_image, vendor_profile_back;
    private TextView shop_now_button, book_appnt_button, buy_appnt_button, no_services, no_amenities, no_reviews;
    private Boolean isFABOpen = false;
    private FloatingActionButton call_fab, whatsapp_fab, enquire_fab;
    private Map<String, Object> leadMap = new HashMap<>();
    PreferenceManager preferenceManager;
    private String tempNameVendor, idStringVendor /*For Passing Vendor Data To next Activity */;
    private Context context;
    private int vendor_activity = 0;//0-not working , 1-working
    private String comingsoon_image_url;
    // private CollapsingToolbarLayout collapsingToolbarLayout;
    private CardView enquiry_card, reviews_card, shop_now_card;
    private MaxHeightNestedScrollView vendor_profile_nestedScrollView;
    private ShimmerFrameLayout vendor_profile_shimmer;
    private AppBarLayout app_bar;
    private LinearLayout main_layout, details_layout_caller, services_layout_caller,
            amenities_layout_caller, reviews_layout_caller, services_layout, amenities_layout,
            reviews_layout, shop_now_button_layout;
    private CardView details_layout;
    private TextView details_text, services_text, amenities_text, reviews_text, review_us, vendor_review_rating, vendor_reviews_count;
    private View details_view, services_view, amenities_view, reviews_view;
    private RatingBar vendor_review_ratingBar;
    private ViewPager profile_banner_pager;
    private CirclePageIndicator profile_banner_pager_indicator;
    private List<SlidersModelClass> slidersModelClassesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();
        preferenceManager = new PreferenceManager(ProfileActivity.this);
        context = ProfileActivity.this;
        token = preferenceManager.getString("token");
        /*getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );*/
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }*/
        init();
        vendor_id = getIntent().getStringExtra(VENDOR_ID);
        profileFetcher();
        reviewsFetcher();

        /*if (listGrid){
            shop_now_button.setVisibility(View.GONE);
            book_appnt_button.setVisibility(View.VISIBLE);
            buy_appnt_button.setVisibility(View.GONE);
        }else *//*if (!listGrid)*//*{*/
        shop_now_button.setVisibility(View.VISIBLE);
        book_appnt_button.setVisibility(GONE);
        buy_appnt_button.setVisibility(GONE);
        //}

        shopNowButtonClick();
        bookAppntButtonClick();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.main_fab);
        call_fab = (FloatingActionButton) findViewById(R.id.call_fab);
        whatsapp_fab = (FloatingActionButton) findViewById(R.id.whatsapp_fab);
        enquire_fab = (FloatingActionButton) findViewById(R.id.enquire_fab);

       /* enquiry_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leadMap.size() > 0) {
                    JSONObject jsonObject = new JSONObject(leadMap);
                    leadGenerator(jsonObject);
                } else {
                    UImsgs.showCustomToast(ProfileActivity.this, "Enquiry Not Possible",ERROR);
                }
            }
        });*/

        whatsapp_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsApp(v, profile_whatsapp);
            }
        });
        call_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDial(v, profile_contact.getText().toString().trim());
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }
            }
        });
       /* profile_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/
        enquire_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leadMap.size() > 0) {
                    JSONObject jsonObject = new JSONObject(leadMap);
                    leadGenerator(jsonObject);
                } else {
                    UImsgs.showCustomToast(ProfileActivity.this, "Enquiry Not Possible", ERROR);
                }

            }
        });
        /*submit_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewDialogOpener();

            }
        });*/


        details_layout_caller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                details_text.setTextColor(getResources().getColor(R.color.orange));
                details_view.setBackgroundColor(getResources().getColor(R.color.orange));

                services_text.setTextColor(getResources().getColor(R.color.black));
                services_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                amenities_text.setTextColor(getResources().getColor(R.color.black));
                amenities_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                reviews_text.setTextColor(getResources().getColor(R.color.black));
                reviews_view.setBackgroundColor(getResources().getColor(R.color.light_gray));

                details_layout.setVisibility(View.VISIBLE);
                services_layout.setVisibility(GONE);
                amenities_layout.setVisibility(GONE);
                reviews_layout.setVisibility(GONE);

            }
        });
        services_layout_caller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                services_text.setTextColor(getResources().getColor(R.color.orange));
                services_view.setBackgroundColor(getResources().getColor(R.color.orange));

                details_text.setTextColor(getResources().getColor(R.color.black));
                details_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                amenities_text.setTextColor(getResources().getColor(R.color.black));
                amenities_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                reviews_text.setTextColor(getResources().getColor(R.color.black));
                reviews_view.setBackgroundColor(getResources().getColor(R.color.light_gray));

                services_layout.setVisibility(View.VISIBLE);
                details_layout.setVisibility(GONE);
                amenities_layout.setVisibility(GONE);
                reviews_layout.setVisibility(GONE);

            }
        });
        amenities_layout_caller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amenities_text.setTextColor(getResources().getColor(R.color.orange));
                amenities_view.setBackgroundColor(getResources().getColor(R.color.orange));

                services_text.setTextColor(getResources().getColor(R.color.black));
                services_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                details_text.setTextColor(getResources().getColor(R.color.black));
                details_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                reviews_text.setTextColor(getResources().getColor(R.color.black));
                reviews_view.setBackgroundColor(getResources().getColor(R.color.light_gray));

                amenities_layout.setVisibility(View.VISIBLE);
                services_layout.setVisibility(GONE);
                details_layout.setVisibility(GONE);
                reviews_layout.setVisibility(GONE);

            }
        });
        reviews_layout_caller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviews_text.setTextColor(getResources().getColor(R.color.orange));
                reviews_view.setBackgroundColor(getResources().getColor(R.color.orange));

                services_text.setTextColor(getResources().getColor(R.color.black));
                services_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                amenities_text.setTextColor(getResources().getColor(R.color.black));
                amenities_view.setBackgroundColor(getResources().getColor(R.color.light_gray));
                details_text.setTextColor(getResources().getColor(R.color.black));
                details_view.setBackgroundColor(getResources().getColor(R.color.light_gray));

                reviews_layout.setVisibility(View.VISIBLE);
                services_layout.setVisibility(GONE);
                amenities_layout.setVisibility(GONE);
                details_layout.setVisibility(GONE);

            }
        });

        review_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewDialogOpener();
            }
        });

        vendor_profile_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    public void openDial(View view, String mobile) {
        Uri u = Uri.parse("tel:" + "+91" + mobile);
        Intent i = new Intent(Intent.ACTION_DIAL, u);

        try {
            // Launch the Phone app's dialer with a phone
            // number to dial a call.
            startActivity(i);
        } catch (SecurityException s) {
            // show() method display the toast with
            // exception message.

        }
    }

    public void openWhatsApp(View view, String profile_whatsapp) {
        try {
            String text = "";// Replace with your message.

            String toNumber = "+91" + profile_whatsapp; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void leadGenerator(JSONObject jsonObject) {
        final String data = jsonObject.toString();
        DialogOpener.dialogOpener(context);

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LEAD_GENERATE_ARRAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 201) {

                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(ProfileActivity.this, "Thank you ! " + jsonObject.getString("message"), SUCCESS);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        DialogOpener.dialog.dismiss();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(context, "Something Went Wrong", ERROR);

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void showFABMenu() {
        isFABOpen = true;
        call_fab.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        whatsapp_fab.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        enquire_fab.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        call_fab.animate().translationY(0);
        whatsapp_fab.animate().translationY(0);
        enquire_fab.animate().translationY(0);
    }

    private void shopNowButtonClick() {
        shop_now_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vendor_activity != 0) {
                    if (shop_now_button.getText().toString().trim().equalsIgnoreCase("Book now")) {
                        if (leadMap.size() > 0) {
                            JSONObject jsonObject = new JSONObject(leadMap);
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            alertDialogBuilder.setTitle("Alert");
                            alertDialogBuilder
                                    .setMessage("Would you like to send your details to " + tempNameVendor + ".So that we can contact you.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            leadGenerator(jsonObject);
                                        }
                                    })
                                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } else {
                            UImsgs.showCustomToast(ProfileActivity.this, "Enquiry Not Possible", ERROR);
                        }
                    } else {
                        /*Intent intent = new Intent(getApplicationContext(), VendorsSubCategoriesActivity.class);
                        intent.putExtra(reastaurantName, tempNameVendor);
                        intent.putExtra(vendorUserId, idStringVendor);
                        intent.putExtra(VENDOR_ID, vendor_id);
                        startActivity(intent);*/

                        if (servicesList != null) {
                            if (!servicesList.isEmpty()) {
                                UserData.getInstance().setServicesList(servicesList);
                                Intent servicesIntent = new Intent(ProfileActivity.this, ProfileServicesActivity.class);
                                //tempNameVendor, idStringVendor, vendor_id
                                Bundle bundle = new Bundle();
                                bundle.putString("tempNameVendor", tempNameVendor);
                                bundle.putString("idStringVendor", idStringVendor);
                                bundle.putString("vendor_id", vendor_id);
                                servicesIntent.putExtras(bundle);
                                startActivity(servicesIntent);

                                /*LayoutInflater inflater = getLayoutInflater();
                                View alertLayout = inflater.inflate(R.layout.service_selection_layout, null);
                                RecyclerView service_selection_recycler = alertLayout.findViewById(R.id.service_selection_recycler);
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.setIcon(R.mipmap.ic_launcher);
                                alertDialog.setView(alertLayout);
                                alertDialog.show();


                                ServiceSelectionAdapter serviceSelectionAdapter = new ServiceSelectionAdapter(context, servicesList, tempNameVendor, idStringVendor, vendor_id);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {
                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                service_selection_recycler.setLayoutManager(layoutManager);
                                service_selection_recycler.setAdapter(serviceSelectionAdapter);*/
                            } else {
                                shop_now_card.setVisibility(GONE);
                                //Toast.makeText(context, "No more categories available", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            shop_now_card.setVisibility(GONE);
                            //Toast.makeText(context, "No more categories available", Toast.LENGTH_SHORT).show();
                        }
                    }
                    /*Intent intent = new Intent(getApplicationContext(), FoodActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    startActivity(intent);*/
                } else {
                    comingSoon(comingsoon_image_url);
                }
            }
        });
    }

    private void bookAppntButtonClick() {
        book_appnt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vendor_activity != 0) {


                    Intent intent = new Intent(getApplicationContext(), VendorsSubCategoriesActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    startActivity(intent);


                } else {
                    comingSoon(comingsoon_image_url);
                }
            }
        });
    }

    private void profileFetcher() {

        //DialogOpener.dialogOpener(context);

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        Log.d("url", INDIVIDUAL_VENDOR + vendor_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(String response) {
                Log.d("vendor_response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        leadMap = new HashMap<>();
                        ArrayList<Object> leadVendorsList = new ArrayList<>();
                        Map<String, String> leadVendorsMapList = new HashMap<>();
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");

                            idStringVendor = dataObject.getString("vendor_user_id");
                            tempNameVendor = dataObject.getString("name");
                            // profile_header_name.setText(dataObject.getString("name"));
                            //collapsingToolbarLayout.setTitle(dataObject.getString("name"));
                            profile_name.setText(tempNameVendor);
                            try {
                                profile_website_url.setText(dataObject.getJSONArray("links").getJSONObject(3).getString("url"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            profile_email.setText(dataObject.getString("email"));

                            try {
                                profile_landmark.setText(dataObject.getString("landmark"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (!dataObject.getString("desc").equalsIgnoreCase("null")
                                        & !dataObject.getString("desc").equalsIgnoreCase("")
                                        & dataObject.get("desc") != null) {
                                    profile_description.setText(dataObject.getString("desc"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                profile_constituency.setText(dataObject.getJSONObject("constituency").getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            leadVendorsMapList.put("vendor_id", idStringVendor);
                            leadVendorsList.add(leadVendorsMapList);
                            leadMap.put("vendors", leadVendorsList);
                            Log.d("leadmap", leadMap.toString());
                            /*Picasso.get()
                                    .load(dataObject.getString("cover"))
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profile_image);*/
                            //multicat_vendor_address.setText(dataObject.getJSONObject("location").getString("address"));

                            //JSONArray timingsArray = dataObject.getJSONArray("timings");


                            try {
                                JSONArray timingsArray = dataObject.getJSONArray("timings");
                                profile_timings.setText(timingsArray.getJSONObject(0).getString("start_time").substring(0, 5) + " - " + timingsArray.getJSONObject(timingsArray.length() - 1).getString("end_time").substring(0, 5));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject jsonObjectLocation = dataObject.getJSONObject("location");
                                profile_address.setText(jsonObjectLocation.getString("address"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONArray contactsArray = dataObject.getJSONArray("contacts");
                                for (int i = 0; i < contactsArray.length(); i++) {
                                    JSONObject contactObject = contactsArray.getJSONObject(i);
                                    try {
                                        if (contactObject.getString("type").equalsIgnoreCase("1")) {
                                            profile_contact.setText(contactObject.getString("number"));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                               /* if (contactObject.getString("type").equalsIgnoreCase("2")) {
                                    multicat_vendor_landline.setText(contactObject.getString("std_code") + " " + contactObject.getString("number"));
                                }*/
                                    try {
                                        if (contactObject.getString("type").equalsIgnoreCase("3")) {
                                            profile_whatsapp = contactObject.getString("number");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                /*
                                if (contactObject.getString("type").equalsIgnoreCase("4")) {
                                    multicat_vendor_helpline.setText(contactObject.getString("number"));
                                }*/

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject category_object = dataObject.getJSONObject("category");
                                vendor_activity = category_object.getInt("status");
                                comingsoon_image_url = category_object.getString("coming_soon_image");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            try {
                                JSONObject amenitiesObject = dataObject.getJSONObject("amenities");
                                Iterator x = amenitiesObject.keys();
                                JSONArray amenitiesArray = new JSONArray();

                                while (x.hasNext()) {
                                    String key = (String) x.next();
                                    amenitiesArray.put(amenitiesObject.get(key));
                                }
                                if (amenitiesArray.length() > 0) {
                                    amenitiesList = new ArrayList<>();
                                    for (int i = 0; i < amenitiesArray.length(); i++) {

                                        JSONObject amenitiesDataObject = amenitiesArray.getJSONObject(i);
                                        AmenitiesPojo amenitiesPojo = new AmenitiesPojo();
                                        amenitiesPojo.setId(amenitiesDataObject.getString("id"));
                                        amenitiesPojo.setList_id(amenitiesDataObject.getString("list_id"));
                                        amenitiesPojo.setName(amenitiesDataObject.getString("name"));
                                        amenitiesList.add(amenitiesPojo);
                                    }

                                    AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(ProfileActivity.this, amenitiesList);
                                    GridLayoutManager layoutManager = new GridLayoutManager(ProfileActivity.this, 2, GridLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(ProfileActivity.this) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    profile_amenities_recycler.setLayoutManager(layoutManager);
                                    profile_amenities_recycler.setAdapter(amenitiesAdapter);
                                    no_amenities.setVisibility(GONE);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                /*JSONObject servicesObject = dataObject.getJSONObject("services");
                                Iterator x1 = servicesObject.keys();*/
                                JSONArray servicesArray = dataObject.getJSONArray("services");

                                /*while (x1.hasNext()) {
                                    String key = (String) x1.next();
                                    servicesArray.put(servicesObject.get(key));
                                }*/
                                if (servicesArray.length() > 0) {
                                    servicesList = new ArrayList<>();
                                    for (int i = 0; i < servicesArray.length(); i++) {

                                        JSONObject servicesDataObject = servicesArray.getJSONObject(i);
                                        ServicesPojo servicesPojo = new ServicesPojo();
                                        servicesPojo.setId(servicesDataObject.getString("id"));
                                        //servicesPojo.setList_id(servicesDataObject.getString("vendor_id"));
                                        servicesPojo.setName(servicesDataObject.getString("name"));
                                        servicesPojo.setImageUrl(servicesDataObject.getString("image"));
                                        if (dataObject.getInt("id") == lead_management_service_id) {
                                            enquire_fab.setVisibility(View.VISIBLE);
                                            shop_now_button.setVisibility(GONE);
                                            shop_now_button_layout.setVisibility(GONE);
                                        }
                                        servicesList.add(servicesPojo);
                                    }

                                    ServicesAdapter servicesAdapter = new ServicesAdapter(ProfileActivity.this, servicesList);
                                    GridLayoutManager layoutManager = new GridLayoutManager(ProfileActivity.this, 2, GridLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(ProfileActivity.this) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    profile_services_recycler.setLayoutManager(layoutManager);
                                    profile_services_recycler.setAdapter(servicesAdapter);

                                    no_services.setVisibility(GONE);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONArray moreImagesArray = dataObject.getJSONArray("banners");
                                if (moreImagesArray.length() > 0) {
                                    moreList = new ArrayList<>();
                                    for (int i = 0; i < moreImagesArray.length(); i++) {
                                        MorePojo morePojo = new MorePojo();
                                        Log.d("img", moreImagesArray.get(i).toString());
                                        morePojo.setImage(moreImagesArray.get(i).toString());
                                        moreList.add(morePojo);

                                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                                        slidersModelClass.setBanners(moreImagesArray.get(i).toString());
                                        slidersModelClassesList.add(slidersModelClass);
                                    }

                                    MoreImagesAdapter moreImagesAdapter = new MoreImagesAdapter(context, moreList, ProfileActivity.this);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    more_images_recycler.setLayoutManager(layoutManager);
                                    more_images_recycler.setAdapter(moreImagesAdapter);


                                    profile_banner_pager.setAdapter(new SlidingImage_AdapterTemp(context, slidersModelClassesList));
                                    profile_banner_pager_indicator.setViewPager(profile_banner_pager);


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            JSONArray jsonArrayFields = dataObject.getJSONArray("fields");
                            if (jsonArrayFields.length() != 0) {
                                JSONObject jsonObjectFields = jsonArrayFields.getJSONObject(0);
                                shop_now_button.setText(jsonObjectFields.getString("name"));
                            }


                            try {
                                JSONArray ratingsArray = dataObject.getJSONArray("ratings");
                                if (ratingsArray.length() > 0) {
                                    double rating = 0.0;
                                    for (int i = 0; i < ratingsArray.length(); i++) {
                                        JSONObject ratingObject = ratingsArray.getJSONObject(i);
                                        rating = rating + ratingObject.getDouble("rating");
                                    }

                                    double avg_rating = rating / ratingsArray.length();

                                    vendor_review_rating.setText((avg_rating) + "");

                                    vendor_review_ratingBar.setRating((float) avg_rating);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                        //DialogOpener.dialog.dismiss();
                        visibilityVisible();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(context, "Something went wrong", ERROR);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void reviewsFetcher() {

        Map<String, String> maptoparse = new HashMap<>();
        maptoparse.put("vendor_id", vendor_id);
        JSONObject jsonObject = new JSONObject(maptoparse);
        final String dataStr = jsonObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_RETRIEVAL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.d("review_response", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray dataArray = jsonObject.getJSONArray("data");

                            if (dataArray.length() > 0) {
                                reviewsList = new ArrayList<>();
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    ReviewsPojo reviewsPojo = new ReviewsPojo();
                                    reviewsPojo.setReview(dataObject.getString("review"));
                                    reviewsPojo.setRating(dataObject.getString("rating"));
                                    if (!dataObject.getJSONObject("user").getString("first_name").equalsIgnoreCase("null") && dataObject.getJSONObject("user").getString("first_name") != null) {
                                        reviewsPojo.setFname(dataObject.getJSONObject("user").getString("first_name"));
                                    }
                                    if (!dataObject.getJSONObject("user").getString("last_name").equalsIgnoreCase("null") && dataObject.getJSONObject("user").getString("last_name") != null) {
                                        reviewsPojo.setLname(dataObject.getJSONObject("user").getString("last_name"));
                                    }
                                    reviewsPojo.setImage(dataObject.getJSONObject("user").getString("image"));
                                    reviewsList.add(reviewsPojo);
                                }

                                vendor_reviews_count.setText("(" + reviewsList.size() + " Reviews)");

                                ReviewsAdapter reviewsAdapter = new ReviewsAdapter(ProfileActivity.this, reviewsList);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(ProfileActivity.this) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                profile_reviews_recycler.setLayoutManager(layoutManager);
                                profile_reviews_recycler.setAdapter(reviewsAdapter);
                                no_reviews.setVisibility(GONE);

                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(ProfileActivity.this, error.toString());
                Log.d("error", error.toString());
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void reviewDialogOpener() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.review_taking_layout, null);

        final EditText reviewData = alertLayout.findViewById(R.id.review);
        final RatingBar reviewrating = alertLayout.findViewById(R.id.review_ratingBar);
        final TextView submit = alertLayout.findViewById(R.id.review_submission);

        AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("NextClick");
        alert.setMessage("Please provide your review");
        AlertDialog dialog = alert.create();
        dialog.show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reviewStr = reviewData.getText().toString().trim();
                float rating = reviewrating.getRating();
                if (reviewStr.length() > 2 && rating >= 1.0) {
                    Map<String, String> maptoparse = new HashMap<>();
                    maptoparse.put("vendor_id", vendor_id);
                    maptoparse.put("rating", rating + "".trim());
                    maptoparse.put("review", reviewStr);
                    JSONObject jsonObject = new JSONObject(maptoparse);
                    reviewSubmission(jsonObject, dialog);
                    reviewsFetcher();

                } else {
                    Toast.makeText(context, "Please provide the data (Review & Rating)", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void reviewSubmission(JSONObject dataObject, AlertDialog dialog) {
        final String dataStr = dataObject.toString();
        DialogOpener.dialogOpener(context);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_CREATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 201) {
                        dialog.dismiss();
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(context, "Submitted Successfully", SUCCESS);
                    } else {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();

                UImsgs.showCustomToast(context, "Sorry for Inconvenience \n Please submit your review again", WARNING);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X_AUTH_TOKEN", token);
                params.put("content-type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void init() {
        profile_amenities_recycler = findViewById(R.id.profile_amenities_recycler);
        profile_services_recycler = findViewById(R.id.profile_services_recycler);
        profile_reviews_recycler = findViewById(R.id.profile_reviews_recycler);
        more_images_recycler = findViewById(R.id.more_images_recycler);

        profile_name = findViewById(R.id.profile_name);
        profile_email = findViewById(R.id.profile_email);
        profile_address = findViewById(R.id.profile_address);
        profile_website_url = findViewById(R.id.profile_website_url);
        profile_contact = findViewById(R.id.profile_contact);
        profile_landmark = findViewById(R.id.profile_landmark);
        profile_constituency = findViewById(R.id.profile_constituency);
        //profile_header_name = findViewById(R.id.profile_header_name);
        submit_review = findViewById(R.id.submit_review);
        profile_timings = findViewById(R.id.profile_timings);
        profile_description = findViewById(R.id.profile_description);

        /*profile_image = findViewById(R.id.profile_image);*/
        vendor_profile_back = findViewById(R.id.vendor_profile_back);

        //Buttons
        shop_now_button = findViewById(R.id.shop_now_button);
        book_appnt_button = findViewById(R.id.book_appnt_button);
        buy_appnt_button = findViewById(R.id.buy_appnt_button);


        no_services = findViewById(R.id.no_services);
        no_amenities = findViewById(R.id.no_amenities);
        no_reviews = findViewById(R.id.no_reviews);

        //enquiry_card = findViewById(R.id.enquiry_card);
        //reviews_card = findViewById(R.id.reviews_card);
        shop_now_card = findViewById(R.id.shop_now_card);


        main_layout = findViewById(R.id.main_layout);


        //app_bar = findViewById(R.id.app_bar);
        vendor_profile_shimmer = findViewById(R.id.vendor_profile_shimmer);


        details_layout_caller = findViewById(R.id.details_layout_caller);
        services_layout_caller = findViewById(R.id.services_layout_caller);
        amenities_layout_caller = findViewById(R.id.amenities_layout_caller);
        reviews_layout_caller = findViewById(R.id.reviews_layout_caller);

        details_text = findViewById(R.id.details_text);
        services_text = findViewById(R.id.services_text);
        amenities_text = findViewById(R.id.amenities_text);
        reviews_text = findViewById(R.id.reviews_text);

        details_view = findViewById(R.id.details_view);
        services_view = findViewById(R.id.services_view);
        amenities_view = findViewById(R.id.amenities_view);
        reviews_view = findViewById(R.id.reviews_view);

        details_layout = findViewById(R.id.details_layout);
        services_layout = findViewById(R.id.services_layout);
        amenities_layout = findViewById(R.id.amenities_layout);
        reviews_layout = findViewById(R.id.reviews_layout);

        shop_now_button_layout = findViewById(R.id.shop_now_button_layout);

        review_us = findViewById(R.id.review_us);

        vendor_review_ratingBar = findViewById(R.id.vendor_review_ratingBar);

        vendor_review_rating = findViewById(R.id.vendor_review_rating);
        vendor_reviews_count = findViewById(R.id.vendor_reviews_count);

        profile_banner_pager = findViewById(R.id.profile_banner_pager);
        profile_banner_pager_indicator = findViewById(R.id.profile_banner_pager_indicator);


        //vendor_profile_nestedScrollView = findViewById(R.id.vendor_profile_nestedScrollView);


       /* collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarVendor);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarVendor);*/


        visibilityGone();


    }


    private void visibilityGone() {

        vendor_profile_shimmer.startShimmer();
        vendor_profile_shimmer.setVisibility(View.VISIBLE);
        main_layout.setVisibility(GONE);
        //app_bar.setVisibility(GONE);
        //collapsingToolbarLayout.setVisibility(GONE);
        //enquiry_card.setVisibility(GONE);
        //reviews_card.setVisibility(GONE);
        shop_now_card.setVisibility(GONE);

    }

    private void visibilityVisible() {
        vendor_profile_shimmer.setVisibility(GONE);
        vendor_profile_shimmer.stopShimmer();

        main_layout.setVisibility(View.VISIBLE);
        //app_bar.setVisibility(View.VISIBLE);
        //collapsingToolbarLayout.setVisibility(View.VISIBLE);
        //enquiry_card.setVisibility(View.VISIBLE);
        //reviews_card.setVisibility(View.VISIBLE);
        shop_now_card.setVisibility(View.VISIBLE);

    }

    //Coming soon
    private void comingSoon(String imageUrl) {
        TimeStamp timeStamp = new TimeStamp();
        ImageView comingsoonImage;
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.coming_soon_layou, null);
        comingsoonImage = alertLayout.findViewById(R.id.comingsoon);
        Picasso.get()
                .load(imageUrl + timeStamp.timeStampFetcher())
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .into(comingsoonImage);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setView(alertLayout);
        alertDialog.show();
    }


}
