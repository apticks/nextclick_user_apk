package com.apticks.nextclickuser.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.apticks.nextclickuser.Activities.ui.home.EcomHomeFragment;
import com.apticks.nextclickuser.Pojo.EcomCategories;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class EcommerceHomeActivity extends AppCompatActivity {

    Fragment selectedFragment=null;
    ArrayList<EcomCategories> ecomCategoriesData = new ArrayList<>();
    RecyclerView ecom_categories_recycler;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecommerce_home);
        getSupportActionBar().hide();

        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.default_));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(EcommerceHomeActivity.this, R.color.colorPrimary));
        }

        selectedFragment = new EcomHomeFragment(getApplicationContext());
        getSupportFragmentManager().beginTransaction().replace(R.id.ecom_home_nav_host_fragment,selectedFragment).commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
