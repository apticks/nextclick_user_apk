package com.apticks.nextclickuser.Activities.HMS_ACTIVITIES;


/*
 * @author : sarath
 *
 * @desc : Single vendor details ...
 *
 * */


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS.AmenitiesFragment_HMS;
import com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS.DoctorsFragment_HMS;
import com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS.OverViewFragment_HMS;
import com.apticks.nextclickuser.Fragments.REVIEW.Review_Fragment;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;
import java.util.List;


public class IndividualVendorActivity_HMS extends AppCompatActivity {



    Fragment selectedFragment = null;
    Context mContext;
    private TabLayout tabLayout;
    private String username;
    private ViewPager viewPager;
    String vendor_id;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_vendor);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        init();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(IndividualVendorActivity_HMS.this, R.color.colorPrimary));
        }

        vendor_id = getIntent().getStringExtra("vendorId");
        setupViewPager(viewPager);



        tabLayout.setupWithViewPager(viewPager);
        /*LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.custom_tab, null);
        TextView tabName = (TextView) alertLayout.findViewById(R.id.tab_name);
        tabName.setText("OverView");
        CircularImageView tabImage = (CircularImageView) alertLayout.findViewById(R.id.tab_img);
        Picasso.get().load("https://cdn.dribbble.com/users/77098/screenshots/2842978/pbt_categories_800x600.png").into(tabImage);
        tabLayout.getTabAt(0).setCustomView(alertLayout);
        tabLayout.getTabAt(1).setCustomView(alertLayout);
        tabLayout.getTabAt(2).setCustomView(alertLayout);*/



    }



    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new OverViewFragment_HMS(vendor_id), "Overiview");

        adapter.addFragment(new DoctorsFragment_HMS(vendor_id), "Doctors");
        adapter.addFragment(new AmenitiesFragment_HMS(vendor_id), "Amenities");
        adapter.addFragment(new Review_Fragment(vendor_id), "Reviews");
        viewPager.setAdapter(adapter);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    public void init(){

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

    }



}
