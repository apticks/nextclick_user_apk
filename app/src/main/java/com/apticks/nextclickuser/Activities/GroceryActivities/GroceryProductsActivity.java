package com.apticks.nextclickuser.Activities.GroceryActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.Adapters.GroceryAdapter.GrocerryCategoryPagerAdapter;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.GROCER_CATEGORIES;

public class GroceryProductsActivity extends AppCompatActivity {

    TabLayout groceryCategoryListTablayout;
    ViewPager viewPagerGrocerySubCategory;
    View view;
    Context mContext;

    String tempId;

    ArrayList<SubCategoryItemPojo> grocerySubCategoriesData = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grocery_product_fragment);
        initView();
        mContext = getApplication();

        Intent intent = getIntent();
        tempId = intent.getStringExtra("grocery_id");

        groceryCategoryTab(tempId);

    }

    private void groceryCategoryTab(String str) {
        GrocerryCategoryPagerAdapter.groceryCategoriesList.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GROCER_CATEGORIES + str, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                try {
                    {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("grocery_sub_categories");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                //String desc = resultObject.getString("desc");
                                SubCategoryItemPojo subCategoryItemPojo = new SubCategoryItemPojo();

                                subCategoryItemPojo.setName(name);
                                subCategoryItemPojo.setImage(image);
                                //subCategoryItemPojo.setImage(image);
                                subCategoryItemPojo.setId(id);
                                //ecomSubCategoriesData.add(subCategoryItemPojo);

                                grocerySubCategoriesData.add(subCategoryItemPojo);
                                GrocerryCategoryPagerAdapter.groceryCategoriesList.add(subCategoryItemPojo);



                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                }

                GrocerryCategoryPagerAdapter sectionsPagerAdapter = new GrocerryCategoryPagerAdapter(getSupportFragmentManager());
                viewPagerGrocerySubCategory.setAdapter(sectionsPagerAdapter);
                //foodiemenutablayout.setupWithViewPager(viewpager);
                groceryCategoryListTablayout.setupWithViewPager(viewPagerGrocerySubCategory);
                        /*ecom_sub_categories_recycler.setLayoutManager(layoutManager);
                        ecom_sub_categories_recycler.setAdapter(ecomSubCategoriesAdapter);*/

                groceryCategoryListTablayout.setScrollX(groceryCategoryListTablayout.getWidth());
//                groceryCategoryListTablayout.getTabAt(groceryTabPosition).select();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void initView() {
        groceryCategoryListTablayout =findViewById(R.id.grocery_category_list_tablayout);
        groceryCategoryListTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPagerGrocerySubCategory =findViewById(R.id.viewpager_grocery_sub_category);

    }



}
