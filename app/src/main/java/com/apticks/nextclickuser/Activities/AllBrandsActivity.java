package com.apticks.nextclickuser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.FeaturedBrandsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.FeaturedBrandsPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.FEATURED_BRANDS;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;

public class AllBrandsActivity extends AppCompatActivity {
    private Context mContext;
    private TextView back_text;
    private RecyclerView featured_brands_recycler;
    private ArrayList<FeaturedBrandsPojo> featuredBrandsPojoArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_brands);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext = AllBrandsActivity.this;
        back_text = findViewById(R.id.back_text);
        featured_brands_recycler = findViewById(R.id.featured_brands_recycler);
        featuredBrandsFetcher();
        back_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


    }
    private void featuredBrandsFetcher(){
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FEATURED_BRANDS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if(status){
                        try{
                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            if(dataArray.length()>0){
                                featuredBrandsPojoArrayList = new ArrayList<>();
                                for(int i =0; i<dataArray.length();i++){
                                    FeaturedBrandsPojo featuredBrandsPojo = new FeaturedBrandsPojo();
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    featuredBrandsPojo.setId(dataObject.getString("id"));
                                    featuredBrandsPojo.setImage(dataObject.getString("image"));
                                    featuredBrandsPojo.setName(dataObject.getString("name"));
                                    featuredBrandsPojoArrayList.add(featuredBrandsPojo);


                                }
                                FeaturedBrandsAdapter brandsAdapter = new FeaturedBrandsAdapter(mContext, featuredBrandsPojoArrayList);
                                GridLayoutManager layoutManager = new GridLayoutManager(mContext,  3, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                            // Change this value (default=25f)
                                            private static final float SPEED = 300f;

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }
                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }
                                };
                                featured_brands_recycler.setLayoutManager(layoutManager);
                                featured_brands_recycler.setAdapter(brandsAdapter);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            UImsgs.showCustomToast(mContext,"No Featured Brands Available",INFO);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    UImsgs.showCustomToast(mContext,"No Featured Brands Available",INFO);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showCustomToast(mContext,"Something went wrong",ERROR);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
