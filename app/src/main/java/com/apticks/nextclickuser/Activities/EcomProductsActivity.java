package com.apticks.nextclickuser.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.apticks.nextclickuser.Fragments.EcommerceProductsFragment;
import com.apticks.nextclickuser.R;

public class EcomProductsActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecom_products);
        getSupportActionBar().hide();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.default_));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(EcomProductsActivity.this, R.color.colorPrimary));
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.ecom_detail_nav_host_fragment,new EcommerceProductsFragment(getApplicationContext(),
                getIntent().getStringExtra("sub_cat_id"),
                getIntent().getStringExtra("ecom_categories"))).commit();
        //getSupportFragmentManager().beginTransaction().replace(R.id.ecom_detail_nav_host_fragment,new IndividualEcommProductActivity(getApplicationContext())).commit();
    }
}
