package com.apticks.nextclickuser.Activities;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.apticks.nextclickuser.Fragments.CartFragment;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.ECOM_CART_R;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ConfirmPayment extends AppCompatActivity {
    //Context
    Context mContext;

    //Fragment
    Fragment selectedFragment = null;



    //Array List ModelData
    ArrayList<CartPojo> data = new ArrayList<>();
    //Boolean
    Boolean storedAddress;
    PreferenceManager preferenceManager;
    String token;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_payment);
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        getSupportActionBar().hide();

        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.default_));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(ConfirmPayment.this, R.color.colorPrimary));
        }
        //getActionBar().hide();

        //Intializing First Fragment
        firstFragment();

        /*
         * On Clicking Of Address Select Or Add Address
         * */


        ecomCartRead();

    }

    private void firstFragment() {
        /*CartFragment cartFragment=new CartFragment(mContext);
        FragmentTransaction ft=getFragmentManager().beginTransaction();
        //SecondFrag secondFrag=new SecondFrag();
        ft.addToBackStack("addAddressFrag");
        ft.replace(R.id.frag_container,cartFragment);
        ft.commit();*/
        selectedFragment = new CartFragment(mContext);
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,selectedFragment).commit();
    }

    /*
     * This MEthod is for calling API of Reading the Cart available Item
     * Returns Void
     * Calls ECOM_CART_R URLs
     * */
    private void ecomCartRead() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_CART_R, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //JSONObject dataJson = jsonObject.getJSONObject("status");
                            //Toast.makeText(mContext, String.valueOf(dataJson), Toast.LENGTH_SHORT).show();
                            JSONArray resultArray = jsonObject.getJSONArray("data");

                            if (resultArray.length() > 0) {
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("user_id");
                                    String image = resultObject.getString("product_id");
                                    String quatity = resultObject.getString("qty");

                                    CartPojo categoriesData = new CartPojo();
                                    categoriesData.setId(String.valueOf(i));
                                    categoriesData.setName("name");
                                    categoriesData.setPrice("250");
                                    categoriesData.setSpecification("jkl");
                                    //categoriesData.setImage(image);
                                    data.add(categoriesData);
                                }
                                /*ItemsCartEcommerceAdapter allCategoriesAdapter = new ItemsCartEcommerceAdapter(mContext, data);
                                recycleCart.setAdapter(allCategoriesAdapter);*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(mContext, error.toString());
                    Log.d("error", error.toString());
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", token);


                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }
}
