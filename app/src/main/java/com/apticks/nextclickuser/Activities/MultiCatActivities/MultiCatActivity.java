package com.apticks.nextclickuser.Activities.MultiCatActivities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.Fragments.MultiCatFragments.MultiCat_VendorsList_Fragment;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.SubCatPOJO;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_LIST;

public class MultiCatActivity extends AppCompatActivity {
    private TabLayout sub_cat_tabs;
    private ViewPager sub_cat_Viewpager;
    Fragment selectedFragment = null;
    private Context mContext;
    private ArrayList<SubCatPOJO> subCatList;
    private String categoryId;
    private TextView multicatname;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_cat);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        categoryId = getIntent().getStringExtra("cat_id");
        //categoryId = "7";
        init();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(MultiCatActivity.this, R.color.colorPrimary));
        }
        subcategoriesFetcher();

        //vendor_id = getIntent().getStringExtra("vendorId");
       /* setupViewPager(sub_cat_Viewpager);


        sub_cat_tabs.setupWithViewPager(sub_cat_Viewpager);*/
    }


    private void init() {
        sub_cat_tabs = findViewById(R.id.sub_cat_tabs);
        sub_cat_Viewpager = findViewById(R.id.sub_cat_viewpager);
        multicatname = findViewById(R.id.multicat_name);
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<SubCatPOJO> subCatList) {
        MultiCatActivity.ViewPagerAdapter adapter = new MultiCatActivity.ViewPagerAdapter(getSupportFragmentManager());

        for(int i=0; i<subCatList.size();i++){
            adapter.addFragment(new MultiCat_VendorsList_Fragment(subCatList.get(i).getId()),subCatList.get(i).getName());
            /*try {
                Thread thread = new Thread();
                thread.sleep(1000);
            }
            catch (Exception e){
                e.printStackTrace();
            }*/
        }

        /*adapter.addFragment(new OverViewFragment_HMS(vendor_id), "Overiview");

        adapter.addFragment(new DoctorsFragment_HMS(vendor_id), "Doctors");
        adapter.addFragment(new AmenitiesFragment_HMS(vendor_id), "Amenities");
        adapter.addFragment(new Review_Fragment(vendor_id), "Reviews");*/
        viewPager.setAdapter(adapter);

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
           /* LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.custom_tab, null);
            TextView tabName = (TextView) alertLayout.findViewById(R.id.tab_name);
            tabName.setText("OverView");
            CircularImageView tabImage = (CircularImageView) alertLayout.findViewById(R.id.tab_img);
            Picasso.get().load("https://cdn.dribbble.com/users/77098/screenshots/2842978/pbt_categories_800x600.png").into(tabImage);*/
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void subcategoriesFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST + categoryId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {
                            if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                                JSONObject dataObject = jsonObject.getJSONObject("data");
                                multicatname.setText(dataObject.getString("name"));
                                JSONArray subCatArray = dataObject.getJSONArray("sub_categories");
                                if (subCatArray.length() > 0) {
                                    subCatList = new ArrayList<>();
                                    for (int i = 0; i < subCatArray.length(); i++) {
                                        JSONObject subCatObject = subCatArray.getJSONObject(i);
                                        SubCatPOJO subCatPOJO = new SubCatPOJO();
                                        subCatPOJO.setId(subCatObject.getString("id"));
                                        subCatPOJO.setName(subCatObject.getString("name"));
                                        subCatPOJO.setImage(subCatObject.getString("image"));
                                        subCatList.add(subCatPOJO);
                                    }
                                    setupViewPager(sub_cat_Viewpager,subCatList);
                                    sub_cat_tabs.setupWithViewPager(sub_cat_Viewpager);
                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = null;
                                    for (int i=0;i<subCatList.size();i++){
                                        alertLayout = inflater.inflate(R.layout.custom_tab, null);
                                        TextView tabName = (TextView) alertLayout.findViewById(R.id.tab_name);
                                        tabName.setText(subCatList.get(i).getName());
                                        CircularImageView tabImage = (CircularImageView) alertLayout.findViewById(R.id.tab_img);
                                        Picasso.get().load(/*subCatList.get(i).getImage()*/"https://cdn.dribbble.com/users/77098/screenshots/2842978/pbt_categories_800x600.png").into(tabImage);
                                        sub_cat_tabs.getTabAt(i).setCustomView(alertLayout);
                                    }

                                }

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }
}
