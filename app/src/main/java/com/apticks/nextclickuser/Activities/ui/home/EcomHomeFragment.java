package com.apticks.nextclickuser.Activities.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.HomeScreenActivity;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Adapters.EcomCategoriesAdapter;
import com.apticks.nextclickuser.Adapters.EcomSubCategoriesAdapter;
import com.apticks.nextclickuser.Adapters.EcomSubCategoryItemsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.OnBackPressedInterface;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.EcomCategories;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.ECOM_CATEGORIES;
import static com.apticks.nextclickuser.Config.Config.ECOM_ALL_SUB_CATEGORIES;

public class EcomHomeFragment extends Fragment implements OnBackPressedInterface {

    private HomeViewModel homeViewModel;
    ListView ecom_categories_listview;
    RecyclerView ecom_sub_categories_recycler;
    ImageView image_back;
    View root;

    ArrayList<EcomCategories> ecomCategoriesData = new ArrayList<>();
    ArrayList<EcomCategories> ecomAllSubCategoriesData = new ArrayList<>();
    ArrayList<SubCategoryItemPojo> ecomSubCategoriesData = new ArrayList<>();

    public EcomHomeFragment(Context applicationContext) {
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);

        initView();

        ecom_categories_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //ecom_categories_listview.setSelector(R.color.MediumPurple);

                //ecom_categories_listview.getSelectedView().setBackgroundColor(Color.CYAN);
                EcomCategories ecomCategoriesSS =ecomCategoriesData.get(position);
                ecomSubCategoriesData.clear();
                ecomSubCategoriesDataFetcher(ecomCategoriesSS.getId());
                EcomSubCategoryItemsAdapter.ecomCategories = ecomCategoriesSS.getId();//List View Categories Item
                TextView ListItemShow =  view.findViewById(R.id.fashion_category_item);
                //ListItemShow.setTextColor(getResources().getColor(R.color.orange));

                //ecom_categories_listview.setSelector(getActivity().getResources().getColor(R.color.gray));

                for (int i = 0; i < ecom_categories_listview.getChildCount(); i++) {
                    if(position == i ){
                        //ecom_categories_listview.getChildAt(i).setBackgroundColor(Color.BLUE);
                        ecom_categories_listview.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.White));
                        //ecom_categories_listview.setSelector(R.color.White);
                    }else{
                        //ecom_categories_listview.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                        //ecom_categories_listview.setSelector(R.color.Ivory);
                        ecom_categories_listview.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.LightGray));
                        //ListItemShow.setTextColor(getResources().getColor(R.color.Orchid));
                        //ListItemShow.setTextColor(getResources().getColor(R.color.Black));
                    }
                }
            }
        });


        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainCategoriesActivity.class);
                startActivity(intent);
                getActivity().finish();

                //getFragmentManager().beginTransaction().remove(new EcomHomeFragment(getActivity())).commitAllowingStateLoss();
                //getChildFragmentManager().beginTransaction().remove(new EcomHomeFragment(getActivity())).commitAllowingStateLoss();
                //fragmentTransaction.remove(yourfragment).commit();
            }
        });

        ecomCategoriesDataFetcher();

        ecomAllSubCategoriesDataFetcher();//All Sub Categories
        return root;
    }

    private void initView() {

        ecom_categories_listview =(ListView) root.findViewById(R.id.ecom_categories_recycler);

        ecom_sub_categories_recycler=(RecyclerView) root.findViewById(R.id.ecom_sub_categories_recycler);

        image_back = root.findViewById(R.id.image_back);
    }


    public void ecomCategoriesDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_CATEGORIES , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        //JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                String desc = resultObject.getString("desc");
                                EcomCategories ecomCategories = new EcomCategories();

                                ecomCategories.setName(name);
                                ecomCategories.setImage(image);
                                ecomCategories.setId(id);
                                ecomCategories.setDesc(desc);
                                ecomCategoriesData.add(ecomCategories);


                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    EcomCategoriesAdapter ecomCategoriesAdapter = new EcomCategoriesAdapter(getActivity(), R.layout.list_view_ecom_category,ecomCategoriesData);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    ecom_categories_listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    ecom_categories_listview.setAdapter(ecomCategoriesAdapter);
                    //ecom_categories_listview.smoothScrollToPosition(0);

                   /* if(ecomCategoriesData.size()>0){
                        EcomCategories ecomCategoriesSSS =ecomCategoriesData.get(0);
                        ecomSubCategoriesDataFetcher(ecomCategoriesSSS.getId());
                    }*/


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(getActivity(), error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void ecomAllSubCategoriesDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_ALL_SUB_CATEGORIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        //JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String catId = resultObject.getString("cat_id");
                                String id = resultObject.getString("id");
                                String desc = resultObject.getString("desc");
                                EcomCategories ecomCategories = new EcomCategories();

                                ecomCategories.setName(name);
                                ecomCategories.setImage(image);
                                ecomCategories.setId(id);
                                ecomCategories.setCatId(catId);
                                ecomCategories.setDesc(desc);
                                ecomAllSubCategoriesData.add(ecomCategories);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    EcomSubCategoriesAdapter ecomSubCategoriesAdapter = new EcomSubCategoriesAdapter(getActivity(), ecomAllSubCategoriesData);
                    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    ecom_sub_categories_recycler.setLayoutManager(layoutManager);
                    ecom_sub_categories_recycler.setAdapter(ecomSubCategoriesAdapter);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(getActivity(), error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    /*
    Eccomerce Sub Categories
    After Clicking List View And
    First Time As first list is being Clicked
    */
    public void ecomSubCategoriesDataFetcher(String id) {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_CATEGORIES+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("ecom_sub_categories");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                //String desc = resultObject.getString("desc");
                                SubCategoryItemPojo subCategoryItemPojo = new SubCategoryItemPojo();

                                subCategoryItemPojo.setName(name);
                                subCategoryItemPojo.setImage(image);
                                //subCategoryItemPojo.setImage(image);
                                subCategoryItemPojo.setId(id);

                                ecomSubCategoriesData.add(subCategoryItemPojo);


                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    EcomSubCategoryItemsAdapter ecomSubCategoriesAdapter = new EcomSubCategoryItemsAdapter(getActivity(), ecomSubCategoriesData);
                    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    ecom_sub_categories_recycler.setLayoutManager(layoutManager);
                    ecom_sub_categories_recycler.setAdapter(ecomSubCategoriesAdapter);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(getActivity(), error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
        startActivity(intent);
        Toast.makeText(getActivity(), "Home", Toast.LENGTH_SHORT).show();
    }
}