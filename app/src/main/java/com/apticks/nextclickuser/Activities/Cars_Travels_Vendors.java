package com.apticks.nextclickuser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.R;

import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;

public class Cars_Travels_Vendors extends AppCompatActivity {

    RecyclerView vendors_recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars_travels_vendors);
        getSupportActionBar().hide();
        init();
    }


    private void vendorsFetcher(){

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }

    private void init(){
        vendors_recycler = findViewById(R.id.cars_travels_completelistrecycler);

    }
}
