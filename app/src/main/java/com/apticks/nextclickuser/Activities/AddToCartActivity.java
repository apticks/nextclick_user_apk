package com.apticks.nextclickuser.Activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.apticks.nextclickuser.Adapters.AddToCartExpandable;
import com.apticks.nextclickuser.Adapters.DynamicListAdapter;
import com.apticks.nextclickuser.Adapters.Features_Expand_Adapter;
import com.apticks.nextclickuser.Adapters.MyCategoriesExpandableListAdapter;
import com.apticks.nextclickuser.Adapters.SectionItemsCheckAdapter;
import com.apticks.nextclickuser.Adapters.SectionItemsModelOuterRecycler;
import com.apticks.nextclickuser.Adapters.SectionItemsRadioAdapter;
import com.apticks.nextclickuser.Constants.Constants;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.*;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.utilities.CustomExpandableListView;
import com.apticks.nextclickuser.utilities.Money;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.IErrors.SELECT_MIM_COUNT;

public class AddToCartActivity extends AppCompatActivity {
    AddToCartExpandable listAdapter;
    CustomExpandableListView cartExpandableListView;

    CartDBHelper cartDBHelper;
    DataBaseHelper dataBaseHelper;

    ExpandableListView expandableListView;
    RelativeLayout transparent_layer, progressDialog, add_to_cart;
    Button minus_btn, plus_btn;
    EditText instructionEditText;
    ImageView clos_menu_items_detail;
    TextView inc_dec_tv, itemCount, checkoutAmount;
    TextView full_pack, small_pack, nameTvFoodSection, descTv, total_price_tv;
    ListView radioSelectListView, checkSelectListView;
    private ExpandableListView lvCategory;
    RelativeLayout relative_required_radio, relative_required_check, n_relative_required_radio, n_relative_required_check;
    View layout_cart;

    private RecyclerView mRecyclerView, mRecyclerViewRadio, mRecyclerViewCheck, mRecyclerViewCombo;
    private LinearLayoutManager mLayoutManager, mLayoutManagerRadio, mLayoutManagerCheck, mLayoutManagerCombo;

    //Progress Bar
    CamomileSpinner addToCartProgress;

    //Double Recycler View
    private DynamicListAdapter mDynamicListAdapter;

    //
    private ArrayList<HashMap<String, String>> parentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> childItems;

    ArrayList<ListObject> mFirstList = new ArrayList<ListObject>();
    ArrayList<ListObject> mSecondList = new ArrayList<ListObject>();
    public List<SectionItemsRadioModel> currentSelectedItems = new ArrayList<>();
    public List<SectionItemsRadioModel> currentSelectedSectionItems = new ArrayList<>();
    HashMap<String, Object> currentSelectedItemsHashMap = new HashMap<>();
    HashMap<String, Object> currentSelectedSectionItemsHashMap = new HashMap<>();
    //Adapter
    private MyCategoriesExpandableListAdapter myCategoriesExpandableListAdapter;

    String restaurant_menu_item_id, restaurantVendorId;
    public static String instructionFoodItems = "", sectionItemsRadioSelect;
    Context mContext;
    //int restaurant_menu_item_id;
    SectionItemsModelOuterRecycler sectionItemsModelOuterRecycler;

    private ArrayList<ArrayList<CartChildModel>> ListChild;
    ArrayList<CartChildModel> listChildData;
    ArrayList<CartParentModel> listDataHeader;

    ArrayList<SectionItemsRadioModel> sectionItemsRadioModelsArrayList = new ArrayList<>();
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayList = new ArrayList<>();
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInnerTemp = new ArrayList<>();

    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListOuter = new ArrayList<>();
    LinkedHashMap<Integer, ArrayList<SectionItemsRadioModel>> sectionItemsCheckModelsLinkedListInner = new LinkedHashMap<Integer, ArrayList<SectionItemsRadioModel>>();


    //Header
    List<String> shownItems = new ArrayList<>();

    //Child Lists
    List<String> childList = new ArrayList<>();

    Map<String, ArrayList<String>> expandableData = new HashMap<String, ArrayList<String>>();

    ArrayList<Integer> arrayList = new ArrayList<>();

    //New Categories Sub Categories
    private ArrayList<DataItem> arCategory;
    private ArrayList<SubCategoryItem> arSubCategory;

    Animation animation;

    int present_count = 1;

    String nameString, imageString, priceString;
    int priceInt = 0;
    public static int vendorId;
    private int quantity = 0;



    /*Button button_remove_item,button_add_item;
    int button_remove_item_int,button_add_item_int;*/


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);
        getSupportActionBar().hide();
        mContext = getApplicationContext();

        initView();

        /*Log.d("drrr",getIntent().getIntExtra("button_add_item",0)+"");
        button_add_item_int = getIntent().getIntExtra("button_add_item",0);
        button_remove_item_int = getIntent().getIntExtra("button_remove_item",0);
        if(button_add_item_int == R.id.button_add_item){
            button_add_item = findViewById(R.id.button_add_item);
        }
        if(button_remove_item_int == R.id.button_remove_item){
            button_remove_item = findViewById(R.id.button_remove_item);
        }*/


        cartDBHelper = new CartDBHelper(mContext);
        dataBaseHelper = new DataBaseHelper(mContext);

        animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        //layout_cart.setAnimation(animation);

        //Set visibility On Progress Bar
        progressDialog.setVisibility(View.VISIBLE);


        checkNumberItemCount();//For Cart View

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(AddToCartActivity.this, R.color.orange));
            }
        }*/

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            restaurant_menu_item_id = getIntent().getStringExtra("food_section");
            restaurantVendorId = getIntent().getStringExtra("food_section");
            quantity = getIntent().getIntExtra("totalQuantity", 0);
            //Toast.makeText(this, String.valueOf(restaurant_menu_item_id), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
        }

        /*if(restaurant_menu_item_id==0){
            transparent_layer.setVisibility(View.GONE);
            progressDialog.setVisibility(View.GONE);
        }*/
//        Toast.makeText(this, restaurant_menu_item_id, Toast.LENGTH_SHORT).show();

        ListChild = new ArrayList<>();
        listChildData = new ArrayList<>();
        listDataHeader = new ArrayList<CartParentModel>();

        singleFoodItem();

        //foodSectionSingle(restaurant_menu_item_id);//Food Section Adapter
        /*UImsgs.showToast(mContext, String.valueOf(restaurant_menu_item_id));*/

        minusBtnOnClick();
        plusBtnOnClick();

        //Add to Cart
        addToCart();

        closeCrossButtonClick();

    }

    private void closeCrossButtonClick() {
        clos_menu_items_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void checkNumberItemCount() {
        dataBaseHelper.getTotalNumberOfFoodItems();
        //Toast.makeText(mContext, String.valueOf(dataBaseHelper.getTotalNumberOfFoodItems()), Toast.LENGTH_SHORT).show();
        if (dataBaseHelper.getTotalNumberOfFoodItems() != 0) {
            itemCount.setText(String.valueOf(dataBaseHelper.getTotalNumberOfFoodItems()));
            //Toast.makeText(mContext, String.valueOf(dataBaseHelper.getTotalCartAmount()), Toast.LENGTH_SHORT).show();
            checkoutAmount.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
            //layout_cart.setVisibility(View.VISIBLE);
            animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
            layout_cart.setAnimation(animation);
        } else {
            //layout_cart.setVisibility(View.GONE);
            animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            layout_cart.setAnimation(animation);
        }
    }

    private void singleFoodItem() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        Log.v("Menu Id", FoodSingleItem + restaurant_menu_item_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FoodSingleItem + restaurant_menu_item_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                try {
                    Log.v("Menu ID Here", "" + FoodSingleItem + restaurant_menu_item_id);
                    JSONObject jsonObject = new JSONObject(response);

                    //jsonObject.getJSONObject("data");
                    //Toast.makeText(mContext, ""+jsonObject.getJSONObject("data"), Toast.LENGTH_SHORT).show();
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                    nameString = jsonObjectData.getString("name");
                    imageString = jsonObjectData.getString("image");
                    if (jsonObjectData.getInt("discount") == 0) {
                        priceString = String.valueOf(jsonObjectData.getInt("price"));
                        priceInt = (jsonObjectData.getInt("price"));
                        //Toast.makeText(mContext, "IF", Toast.LENGTH_SHORT).show();
                    } else {
                        priceString = String.valueOf(jsonObjectData.getInt("discount_price"));
                        priceInt = (jsonObjectData.getInt("discount_price"));
                        //Toast.makeText(mContext, "ELSE", Toast.LENGTH_SHORT).show();
                    }


                    nameTvFoodSection.setText(nameString);
                    //descTv.setText(jsonObjectData.getString("desc"));
                    descTv.setText(Html.fromHtml(jsonObjectData.getString("desc")).toString());


                    /*total_price_tv.setText(checkDiscount(jsonObjectData.getString("discount"),
                            jsonObjectData.getString("desc"),
                            priceString));*/
                    try {
                        checkDiscount(jsonObjectData.getInt("discount"),
                                priceInt,
                                priceString);
                    } catch (NumberFormatException e) {
                        //Toast.makeText(mContext, "Number "+e, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "JSON " + e, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {

                    }
                    foodSectionSingle(restaurant_menu_item_id);//Food Section Adapter
                    progressDialog.setVisibility(View.GONE);


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void checkDiscount(int discount, int desc, String priceString) {

        try {
            if (discount != 0) {//Checking Food Discount


                String sellCostString = Money.rupees(
                        BigDecimal.valueOf(Long.valueOf(desc
                        ))).toString()
                        + "     ";



                /*String sellCostString = Money.rupees(
                        BigDecimal.valueOf(Double.parseDouble("520")));*/

                /*String buyMRP = Money.rupees(
                        BigDecimal.valueOf(Long.valueOf(priceString
                        ))).toString();

                String costString = sellCostString + buyMRP;*/

                //total_price_tv.setText(costString, TextView.BufferType.SPANNABLE);
                total_price_tv.setText(sellCostString, TextView.BufferType.SPANNABLE);


                /*Spannable spannable = (Spannable) total_price_tv.getText();

                spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                        costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);*/
                //return ;
            } else {
                String sellCostString = Money.rupees(
                        BigDecimal.valueOf(Long.valueOf(desc
                        ))).toString()
                        + "     ";
                //total_price_tv.setText(priceString);//Food Price
                total_price_tv.setText(sellCostString, TextView.BufferType.SPANNABLE);
                //return ;
            }
            //return null;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }

    private void addToCart() {
        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateItemCount()) {
                    /*Intent intent = new Intent(mContext, CartActivity.class);
                    intent.putExtra("Type","Food");
                    intent.putExtra("item_id",restaurant_menu_item_id);
                    intent.putExtra("quantity",String.valueOf(present_count));
                    intent.putExtra("name_string",String.valueOf(nameString));
                    intent.putExtra("image_string",String.valueOf(imageString));
                    intent.putExtra("price_string",String.valueOf(priceString));
                    startActivity(intent);*/

                    //Pending Work is to ADD Section Items

                    String currentSelectedItemsHashMapString = "";
                    String currentSelectedItemsSectionHashMapString = "";


                    //For Food Items Radio

                    try {
                        /*if (sectionItemsModelOuterRecycler.currentSelectedItemsRadio.size() > 0) {
                            currentSelectedItems = sectionItemsModelOuterRecycler.currentSelectedItemsRadio;

                       *//* for(int j =0;j<currentSelectedItems.size();j++){

                        }*//*
                            SectionItemsRadioModel sectionItemsRadioModel = currentSelectedItems.get(0);

                            currentSelectedItemsHashMap.put("item_id", restaurant_menu_item_id);
                            currentSelectedItemsHashMap.put("quantity", present_count);
                            //currentSelectedItemsHashMap.put("price",sectionItemsRadioModel.getPrice());Log.e("price","");
                            currentSelectedItemsHashMap.put("price", priceString);
                            Log.e("price", "");
                            currentSelectedItemsHashMap.put("sec_item_id",*//*"7"*//*sectionItemsRadioModel.getId());
                            Log.e("sec_item_id", "");

                            //Toast.makeText(mContext, "In IF "+priceString+sectionItemsRadioModel.getId(), Toast.LENGTH_SHORT).show();
                            JSONObject json = new JSONObject(currentSelectedItemsHashMap);
                            currentSelectedItemsHashMapString = json.toString();
                        } else */
                        {
                            currentSelectedItemsHashMap.put("item_id", restaurant_menu_item_id);
                            currentSelectedItemsHashMap.put("quantity", present_count);
                            currentSelectedItemsHashMap.put("price", priceString);
                            Log.e("price", "");

                            //Toast.makeText(mContext, "In ELSE", Toast.LENGTH_SHORT).show();

                            JSONObject json = new JSONObject(currentSelectedItemsHashMap);
                            currentSelectedItemsHashMapString = json.toString();
                        }

                    } catch (NullPointerException e) {
                        Log.e("NULL_POINTER", String.valueOf(e));
                        e.printStackTrace();
                        currentSelectedItemsHashMapString = "";
                    }

                    //For Food Section
                    try {
                        if ((sectionItemsModelOuterRecycler.currentSelectedSectionItems.size() > 0) || (sectionItemsModelOuterRecycler.currentSelectedItemsRadio.size() > 0)) {


                            //Check Box
                            if ((sectionItemsModelOuterRecycler.currentSelectedSectionItems.size() > 0)) {
                                currentSelectedSectionItems = sectionItemsModelOuterRecycler.currentSelectedSectionItems;
                                for (int i = 0; i < currentSelectedSectionItems.size(); i++) {
                                    //UImsgs.showToast(mContext, String.valueOf(i));
                                    SectionItemsRadioModel sectionItemsRadioModel = currentSelectedSectionItems.get(i);
                                    currentSelectedSectionItemsHashMap.put("sec_item_id", sectionItemsRadioModel.getId());
                                    currentSelectedSectionItemsHashMap.put("sec_quantity", present_count);
                                    currentSelectedSectionItemsHashMap.put("sec_price", sectionItemsRadioModel.getPrice());
                                    currentSelectedSectionItemsHashMap.put("sec_name", sectionItemsRadioModel.getName());


                                    JSONObject json = new JSONObject(currentSelectedSectionItemsHashMap);
                                    currentSelectedItemsSectionHashMapString = json.toString();
                                    if (cartDBHelper.insertFoodSectionItems(sectionItemsRadioModel.getId(), present_count, String.valueOf(sectionItemsRadioModel.getPrice()),
                                            sectionItemsRadioModel.getId(), restaurant_menu_item_id, sectionItemsRadioModel.getName()
                                    )) {
                                        //UImsgs.showToast(mContext, "Inserted"+i);
                                        //UImsgs.showToast(mContext, String.valueOf(sectionItemsRadioModel.getId()));
                                    } else {
                                        //UImsgs.showToast(mContext, "Insertion Failed");
                                    }
                                }
                            }

                            //Radio
                            if ((sectionItemsModelOuterRecycler.currentSelectedItemsRadio.size() > 0)) {
                                currentSelectedSectionItems = sectionItemsModelOuterRecycler.currentSelectedItemsRadio;
                                for (int i = 0; i < currentSelectedSectionItems.size(); i++) {
                                    //UImsgs.showToast(mContext, String.valueOf(i));
                                    SectionItemsRadioModel sectionItemsRadioModel = currentSelectedSectionItems.get(i);
                                    currentSelectedSectionItemsHashMap.put("sec_item_id", sectionItemsRadioModel.getId());
                                    currentSelectedSectionItemsHashMap.put("sec_quantity", present_count);
                                    currentSelectedSectionItemsHashMap.put("sec_price", sectionItemsRadioModel.getPrice());
                                    currentSelectedSectionItemsHashMap.put("sec_name", sectionItemsRadioModel.getName());

                                    JSONObject json = new JSONObject(currentSelectedSectionItemsHashMap);
                                    currentSelectedItemsSectionHashMapString = json.toString();
                                    if (cartDBHelper.insertFoodSectionItems(sectionItemsRadioModel.getId(), present_count, String.valueOf(sectionItemsRadioModel.getPrice()),
                                            sectionItemsRadioModel.getId(), restaurant_menu_item_id, sectionItemsRadioModel.getName()
                                    )) {
                                        //UImsgs.showToast(mContext, "Inserted"+i);
                                        //UImsgs.showToast(mContext, String.valueOf(sectionItemsRadioModel.getId()));
                                    } else {
                                        //UImsgs.showToast(mContext, "Insertion Failed");
                                    }
                                }
                            }
                        } else {
                            currentSelectedItemsSectionHashMapString = "";
                        }
                    } catch (NullPointerException e) {
                        //Toast.makeText(mContext, ""+e, Toast.LENGTH_SHORT).show();
                        Log.e("NULL_POINTER", String.valueOf(e));
                        e.printStackTrace();
                        currentSelectedItemsSectionHashMapString = "";
                    }
                    //Toast.makeText(mContext, ""+currentSelectedSectionItems.size(), Toast.LENGTH_SHORT).show();

                    instructionFoodItems = instructionEditText.getText().toString();
                    //cartDBHelper.insertFoodCart(restaurant_menu_item_id, nameString, imageString, priceString, String.valueOf(present_count),instructionFoodItems);
                    if (dataBaseHelper.getVendorIdPresence(vendorId)) {
                        //Toast.makeText(mContext, "True", Toast.LENGTH_SHORT).show();
                        //UImsgs.showToast(mContext,"Amount Added " + String.valueOf(sectionItemsModelOuterRecycler.priceIntTotal));
                        if (cartDBHelper.insertFoodCart(restaurant_menu_item_id, vendorId, nameString,
                                imageString,
                                //priceString,
                                priceItem(),
                                String.valueOf(present_count),
                                instructionFoodItems,
                                currentSelectedItemsHashMapString, currentSelectedItemsSectionHashMapString, quantity)) {
                            //UImsgs.showToast(mContext,currentSelectedItemsSectionHashMapString);
                            //Toast.makeText(mContext, "Add", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(mContext, "NOt", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //Toast.makeText(mContext, "False", Toast.LENGTH_SHORT).show();
                    }


                    finish();
                } else {
                    UImsgs.showToast(mContext, SELECT_MIM_COUNT);
                }
            }
        });

    }

    private String priceItem() {
        if (sectionItemsModelOuterRecycler != null)
            return String.valueOf(sectionItemsModelOuterRecycler.priceIntTotal);
        else
            return String.valueOf(priceString);

    }

    private boolean validateItemCount() {
        if (present_count <= 0) {
            return false;
        }
        return true;
    }

    private void plusBtnOnClick() {
        plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                present_count++;
                inc_dec_tv.setText(String.valueOf(present_count));
                minus_btn.setClickable(true);
            }
        });
    }

    private void minusBtnOnClick() {
        minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (present_count == 0) {
                    minus_btn.setClickable(false);
                } else {
                    minus_btn.setClickable(true);
                    present_count--;
                    inc_dec_tv.setText(String.valueOf(present_count));
                }
            }
        });
    }

    private void initView() {
        progressDialog = findViewById(R.id.progressDialog);
        transparent_layer = findViewById(R.id.transparent_layer);

        cartExpandableListView = (CustomExpandableListView) findViewById(R.id.item_detail_list);
        cartExpandableListView.setExpanded(true);
        cartExpandableListView.setGroupIndicator(null);

        expandableListView = (ExpandableListView) findViewById(R.id.expandable_facilites);

        //Progress Bar
        addToCartProgress = findViewById(R.id.addToCartProgress);

        //Relative Layout
        add_to_cart = findViewById(R.id.add_to_cart);

        relative_required_radio = findViewById(R.id.relative_required_radio);
        relative_required_check = findViewById(R.id.relative_required_check);
        n_relative_required_radio = findViewById(R.id.n_relative_required_radio);
        n_relative_required_check = findViewById(R.id.n_relative_required_check);

        //List View
        radioSelectListView = findViewById(R.id.list_radio_select);
        checkSelectListView = findViewById(R.id.list_check_select);
        lvCategory = findViewById(R.id.lvCategory);

        //View
        layout_cart = findViewById(R.id.layout_cart);

        //Button
        minus_btn = findViewById(R.id.minus_btn);
        plus_btn = findViewById(R.id.plus_btn);

        //Image View
        clos_menu_items_detail = findViewById(R.id.clos_menu_items_detail);

        //Edit Text
        instructionEditText = findViewById(R.id.inst_text);

        //Array List
        arCategory = new ArrayList<>();
        arSubCategory = new ArrayList<>();

        parentItems = new ArrayList<>();
        childItems = new ArrayList<>();


        mRecyclerView = (RecyclerView) findViewById(R.id.my_list);
        mRecyclerViewRadio = (RecyclerView) findViewById(R.id.my_recycle_radio);
        mRecyclerViewCheck = (RecyclerView) findViewById(R.id.my_recycle_check);
        mRecyclerViewCombo = (RecyclerView) findViewById(R.id.my_recycle_combo);

        //Recycle View Adapter
        // Initialize the list
        mDynamicListAdapter = new DynamicListAdapter();


        mLayoutManager = new LinearLayoutManager(AddToCartActivity.this);
        mLayoutManagerRadio = new LinearLayoutManager(AddToCartActivity.this);
        mLayoutManagerCheck = new LinearLayoutManager(AddToCartActivity.this);
        mLayoutManagerCombo = new LinearLayoutManager(AddToCartActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mDynamicListAdapter);

        mRecyclerViewRadio.setLayoutManager(mLayoutManagerRadio);
        mRecyclerViewRadio.setHasFixedSize(true);
        /*mRecyclerViewRadio.setNestedScrollingEnabled(false);
        mRecyclerViewRadio.hasNestedScrollingParent();*/
        //mRecyclerViewRadio.setAdapter(mDynamicListAdapter);

        mRecyclerViewCheck.setLayoutManager(mLayoutManagerCheck);
        mRecyclerViewCheck.setHasFixedSize(true);
        mRecyclerViewCheck.setNestedScrollingEnabled(false);

        mRecyclerViewCombo.setLayoutManager(mLayoutManagerCombo);
        //mRecyclerViewCombo.setAdapter(mDynamicListAdapter);

        itemCount = findViewById(R.id.item_count);//Bottom Cart
        checkoutAmount = findViewById(R.id.checkout_amount);
        inc_dec_tv = findViewById(R.id.inc_dec_tv);
        full_pack = findViewById(R.id.full_pack);
        small_pack = findViewById(R.id.small_pack);
        nameTvFoodSection = findViewById(R.id.name_tv_food_section);
        descTv = findViewById(R.id.desc_tv);
        total_price_tv = findViewById(R.id.total_price_tv);

        cartExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true;
            }
        });
    }

    private void foodSectionSingle(String id) {
        {
            Log.v("Food Sections", id);
            //Toast.makeText(mContext, " ID "+ id, Toast.LENGTH_SHORT).show();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            Log.d("urlllllllll", FOOD_SECTIONS + id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, FOOD_SECTIONS + /*"1"*/id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject dataJson = jsonObject.getJSONObject("data");
                            JSONArray resultObject0 = dataJson.getJSONArray("result");
                            /*JSONObject resultObject1 = dataJson.getJSONObject("1");
                            int itemFieldIntRadio = resultObject0.getInt("item_field");
                            int itemFieldIntCheckBox = resultObject0.getInt("item_field");*/

                            //Toast.makeText(mContext, String.valueOf(resultObject0), Toast.LENGTH_SHORT).show();

                            /*Intent intent = new Intent(mContext, AddToCartActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);*/

                            JSONObject jo = new JSONObject();
                            JSONArray ja = new JSONArray();
                            // populate the array
                            jo.put("arrayName", ja);
                            HashMap<String, Object> stringObjectOuterHashMap = new HashMap<>();

                            if (resultObject0.length() > 0) {

                                for (int i = 0; i < resultObject0.length(); i++) {
                                    JSONObject resultObject = resultObject0.getJSONObject(i);
                                    CartChildModel menuItemExtraModel = new CartChildModel();
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("name");
                                    String required = String.valueOf(resultObject.getInt("required"));
                                    String itemFieldSymbol = String.valueOf(resultObject.getInt("item_field"));
                                    int secPrice = resultObject.getInt("sec_price");
                                    JSONArray itemFieldSecItems = resultObject.getJSONArray("sec_items");

                                    //Toast.makeText(AddToCartActivity.this, String.valueOf(itemFieldSecItems), Toast.LENGTH_SHORT).show();
                                    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInner = new ArrayList<>();

                                    if (itemFieldSecItems.length() >= 1) {
                                        //sectionItemsCheckModelsArrayListInner = new ArrayList<>();
                                        SectionItemsRadioModel sectionItemsRadioModelOuter = new SectionItemsRadioModel();
                                        sectionItemsRadioModelOuter.setName(name);
                                        sectionItemsRadioModelOuter.setItemField(itemFieldSymbol);
                                        sectionItemsRadioModelOuter.setRequired(required);
                                        sectionItemsRadioModelOuter.setSecPrice(secPrice);
                                        sectionItemsCheckModelsArrayListOuter.add(sectionItemsRadioModelOuter);

                                        stringObjectOuterHashMap.put("name", name);
                                        stringObjectOuterHashMap.put("item_field", itemFieldSymbol);
                                        //sectionItemsCheckModelsArrayListInner.clear();

                                        //inner For loop
                                        for (int j = 0; j < itemFieldSecItems.length(); j++) {
                                            JSONObject jsonObjectInner = itemFieldSecItems.getJSONObject(j);

                                            SectionItemsRadioModel sectionItemsRadioModelInner = new SectionItemsRadioModel();
                                            sectionItemsRadioModelInner.setId(jsonObjectInner.getString("id"));
                                            sectionItemsRadioModelInner.setName(jsonObjectInner.getString("name"));
                                            sectionItemsRadioModelInner.setDesc(jsonObjectInner.getString("desc"));
                                            sectionItemsRadioModelInner.setPrice(jsonObjectInner.getInt("price"));
                                            sectionItemsRadioModelInner.setSecPrice(secPrice);

                                            sectionItemsCheckModelsArrayListInner.add(sectionItemsRadioModelInner);
                                            //Toast.makeText(AddToCartActivity.this, String.valueOf(i), Toast.LENGTH_SHORT).show();
                                        }
                                        sectionItemsCheckModelsLinkedListInner.put(i, sectionItemsCheckModelsArrayListInner);
                                        stringObjectOuterHashMap.put("sec_items", sectionItemsCheckModelsArrayListInner);
                                        stringObjectOuterHashMap.put(String.valueOf(i), sectionItemsCheckModelsArrayListInner);
                                        //sectionItemsCheckModelsArrayListInner.clear();
                                        //Toast.makeText(AddToCartActivity.this, String.valueOf(sectionItemsCheckModelsArrayListInner.size()), Toast.LENGTH_SHORT).show();

                                    } else {

                                    }


                                    //StaticRadio ||Check Box
                                    /*  if (*//*!required.equals("0")&&*//*(required.equals("1"))){//Required Radio // Check
                                        //Sing;e List View Food Selection Items
                                        //foodSecItemRadioCheck(id,itemFieldSymbol,name);

                                        //resultObject.getJSONArray("sec_items");
                                        //Toast.makeText(AddToCartActivity.this, "FIRST "+resultObject.getJSONArray("sec_items"), Toast.LENGTH_SHORT).show();
                                        full_pack.setText(name);
                                        relative_required_radio.setVisibility(View.VISIBLE);
                                        JSONArray jsonArraySecItems = resultObject.getJSONArray("sec_items");
                                        String jsonObjectItemField = String.valueOf(resultObject.getString("item_field"));

                                        if (jsonObjectItemField.equals("2")){
                                            setRadioAdapter(jsonArraySecItems,false);
                                        }else {
                                            //setCheckAdapter(jsonArraySecItems,false);
                                            setRadioAdapter(jsonArraySecItems,true);
                                        }

                                    }else if (required.equals("0")*//*&&(itemFieldSymbol.equals("2"))*//*){//Not Required Radio || Check
                                        //Sing;e List View Food Selection Items
                                        //foodSecItemRadioCheck(id,itemFieldSymbol,name);

                                        //resultObject.getJSONArray("sec_items");
                                        //Toast.makeText(AddToCartActivity.this, "FIRST "+resultObject.getJSONArray("sec_items"), Toast.LENGTH_SHORT).show();
                                        small_pack.setText(name);
                                        relative_required_check.setVisibility(View.VISIBLE);
                                        JSONArray jsonArraySecItems = resultObject.getJSONArray("sec_items");
                                        String jsonObjectItemField = String.valueOf(resultObject.getInt("item_field"));

                                        if (jsonObjectItemField.equals("2")){
                                            //setRadioAdapter(jsonArraySecItems,false);
                                            setCheckAdapter(jsonArraySecItems,true);
                                        }else {
                                            setCheckAdapter(jsonArraySecItems,false);
                                        }
                                    }*/

                                    /*if (!required.equals("0")*//*&&(itemFieldSymbol.equals("1"))*//*){
                                        //Sing;e List View Food Selection Items
                                        //foodSecItemRadioCheck(id,itemFieldSymbol,name);

                                        //resultObject.getJSONArray("sec_items");
                                        Toast.makeText(AddToCartActivity.this, "FIRST "+resultObject.getJSONArray("sec_items"), Toast.LENGTH_SHORT).show();
                                        JSONArray jsonArraySecItems = resultObject.getJSONArray("sec_items");
                                        String jsonObjectItemField = String.valueOf(resultObject.getString("item_field"));

                                        if (jsonObjectItemField.equals("2")){
                                            setRadioAdapter(jsonArraySecItems);
                                        }else {
                                            setCheckAdapter(jsonArraySecItems);
                                        }

                                    }else {

                                    }

                                    if ((itemFieldSymbol.equals("2"))){
                                        Toast.makeText(AddToCartActivity.this, "Second"+resultObject.getJSONArray("sec_items"), Toast.LENGTH_SHORT).show();
                                        JSONArray jsonArraySecItems = resultObject.getJSONArray("sec_items");
                                        setCheckAdapter(jsonArraySecItems);
                                    }else {
                                        Toast.makeText(AddToCartActivity.this, "Else", Toast.LENGTH_SHORT).show();
                                    }*/
                                    menuItemExtraModel.setPos(i);
                                    menuItemExtraModel.setExtra_item_id(id);
                                    menuItemExtraModel.setChild_item_name(name);
                                    menuItemExtraModel.setSymbol(itemFieldSymbol);

                                    listChildData.add(menuItemExtraModel);

                                    CartParentModel cartParentModel = new CartParentModel();
                                    cartParentModel.setParentName(name);
                                    cartParentModel.setRequired(required);

                                    listDataHeader.add(cartParentModel);

                                    shownItems.add(name);
                                    childList.add(name);

                                    //fetcher();
                                }

                                //End Of for Loop Outer
                                Log.d("priceint", priceInt + "");
                                sectionItemsModelOuterRecycler = new SectionItemsModelOuterRecycler(sectionItemsCheckModelsArrayListInnerTemp, stringObjectOuterHashMap, sectionItemsCheckModelsArrayListOuter, sectionItemsCheckModelsLinkedListInner.entrySet(),
                                        priceInt,
                                        total_price_tv);
                                mRecyclerViewRadio.setAdapter(sectionItemsModelOuterRecycler);
                            }

                            ListChild.add(listChildData);


                            //Toast.makeText(mContext, String.valueOf(shownItems.size()), Toast.LENGTH_SHORT).show();

                            transparent_layer.setVisibility(View.GONE);
                            //progressDialog.setVisibility(View.GONE);
                            listAdapter = new AddToCartExpandable(getApplicationContext(), listDataHeader, ListChild);


                            cartExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                @Override
                                public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long id) {
                                    final CartChildModel item = (CartChildModel) listAdapter.getChild(groupPosition, childPosition);

                                    boolean iteIsRequired = item.isCheckRequired();
                                    if (!iteIsRequired) {

                                        CheckBox checkBox = view.findViewById(R.id.check_btn);

                                        if (checkBox != null) {
                                            //  checkBox.toggle();

                                            if (!checkBox.isChecked()) {
                                                //  item.setCheckBoxIsChecked(true);
                                                checkBox.setChecked(true);

                                                //FLAG_ONCE_LOOP_ADD = true;
                                                //addNewNode(item.getExtra_item_id(), item.getChild_item_name(), item.getChild_item_price());
                                                //loadCalculationDetail();
                                            } else if (checkBox.isChecked()) {
                                                //   item.setCheckBoxIsChecked(false);
                                                checkBox.setChecked(false);
                                                //FLAG_ONCE_LOOP_ADD = false;
                                                //deleteNewNode(item.getExtra_item_id());
                                                //   loadCalculationDetail();

                                            }
                                        }
                                    } else {


                                        if (!item.isCheckedddd()) {

                                            String string = arrayList.toString();
                                            ArrayList<CartChildModel> childsList = listAdapter.getChilderns(groupPosition);
                                            for (CartChildModel model : childsList) {
                                                if (model.isCheckedddd()) {
                                                    //previousCheck=model.getExtra_item_id();
                                                    break;
                                                }
                                            }

                                            if (arrayList.contains(groupPosition)) {
                                                //FLAG_CHECKBOX_TOGGLER = true;
                                                //deleteNewNode(previousCheck);
                                                //addNewNode(item.getExtra_item_id(), item.getChild_item_name(), item.getChild_item_price());
                                                //previousCheck = item.getExtra_item_id();


                                            } else {
                                                //FLAG_CHECKBOX_TOGGLER = false;
                                                //addNewNode(item.getExtra_item_id(), item.getChild_item_name(), item.getChild_item_price());
                                                //previousCheck = item.getExtra_item_id();
                                                //loadCalculationDetail();
                                                //required = required + 1;
                                                // previousCheck = String.valueOf(previousValArray.get(groupPosition));
                                            }

                                            if (!arrayList.contains(groupPosition)) {
                                                arrayList.add(groupPosition);
                                                //  arrayPos.add(groupPosition,previousCheck);
                                            }

                                            upDateNotify(listAdapter.getChilderns(groupPosition));
                                            item.setCheckeddd(true);
                                            listAdapter.notifyDataSetChanged();
                                        }


                                    }
                                    return false;

                                }
                            });

                            // setting list adapter
                            cartExpandableListView.setAdapter(listAdapter);
                            if (listAdapter.getGroupCount() == 0) {
                                transparent_layer.setVisibility(View.GONE);
                                //progressDialog.setVisibility(View.GONE);
                            }
                            for (int l = 0; l < listAdapter.getGroupCount(); l++)
                                cartExpandableListView.expandGroup(l);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Catch " + String.valueOf(e), Toast.LENGTH_SHORT).show();

                        }

                        //FoodItemsAdapter foodItemsAdapter = new FoodItemsAdapter(mContext, foodItemData);

                        //foodItemsRecycler.setLayoutManager(new GridLayoutManager(mContext, 2,GridLayoutManager.VERTICAL, false));


                        //foodItemsRecycler.setAdapter(foodItemsAdapter);

                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(mContext, "Error " + error.toString());
                    Log.d("error", error.toString());
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }
    }

    private void setCheckAdapter(JSONArray jsonArraySecItems, Boolean aBoolean) {

        if (jsonArraySecItems.length() >= 0) {
            for (int i = 0; i <= jsonArraySecItems.length(); i++) {

                try {
                    JSONObject jsonObjectSectItem = jsonArraySecItems.getJSONObject(i);
                    jsonObjectSectItem.getString("id");
                    jsonObjectSectItem.getString("name");
                    jsonObjectSectItem.getString("desc");
                    jsonObjectSectItem.getString("price");

                    //Toast.makeText(mContext, ""+jsonObjectSectItem.getString("price"), Toast.LENGTH_SHORT).show();

                    SectionItemsRadioModel sectionItemsRadioModel = new SectionItemsRadioModel();
                    sectionItemsRadioModel.setId(jsonObjectSectItem.getString("id"));
                    sectionItemsRadioModel.setName(jsonObjectSectItem.getString("name"));
                    sectionItemsRadioModel.setDesc(jsonObjectSectItem.getString("desc"));
                    sectionItemsRadioModel.setPrice(jsonObjectSectItem.getInt("price"));
                    //sectionItemsRadioModel.setId();

                    sectionItemsCheckModelsArrayList.add(sectionItemsRadioModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                SectionItemsCheckAdapter sectionItemsCheckAdapter = new SectionItemsCheckAdapter(sectionItemsCheckModelsArrayList, aBoolean);
                mRecyclerViewCheck.setAdapter(sectionItemsCheckAdapter);
            }

        }

    }

    private void setRadioAdapter(JSONArray jsonArraySecItems, Boolean aBoolean) {
        //Toast.makeText(mContext,""+jsonArraySecItems.length(),Toast.LENGTH_SHORT).show();
        if (jsonArraySecItems.length() >= 0) {
            for (int i = 0; i <= jsonArraySecItems.length(); i++) {

                try {
                    JSONObject jsonObjectSectItem = jsonArraySecItems.getJSONObject(i);
                    jsonObjectSectItem.getString("id");
                    jsonObjectSectItem.getString("name");
                    jsonObjectSectItem.getString("desc");
                    jsonObjectSectItem.getString("price");

                    //Toast.makeText(mContext, ""+jsonObjectSectItem.getString("price"), Toast.LENGTH_SHORT).show();

                    SectionItemsRadioModel sectionItemsRadioModel = new SectionItemsRadioModel();
                    sectionItemsRadioModel.setId(jsonObjectSectItem.getString("id"));
                    sectionItemsRadioModel.setName(jsonObjectSectItem.getString("name"));
                    sectionItemsRadioModel.setDesc(jsonObjectSectItem.getString("desc"));
                    sectionItemsRadioModel.setPrice(jsonObjectSectItem.getInt("price"));
                    //sectionItemsRadioModel.setId();

                    sectionItemsRadioModelsArrayList.add(sectionItemsRadioModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                SectionItemsRadioAdapter sectionItemsRadioAdapter = new SectionItemsRadioAdapter(sectionItemsRadioModelsArrayList, aBoolean);
                mRecyclerViewRadio.setAdapter(sectionItemsRadioAdapter);
            }

        }
    }

    private void foodSecItemRadioCheck(String id, String itemFieldSymbol, String name) {
        //Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();

        DataItem dataItem = new DataItem();
        dataItem.setCategoryId(id);
        dataItem.setCategoryName(name);

        RequestQueue requestQueue1 = Volley.newRequestQueue(mContext);

        StringRequest request = new StringRequest(Request.Method.GET, Food_Single_Section_ItemS + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        //Toast.makeText(mContext, String.valueOf(dataJson.length()), Toast.LENGTH_SHORT).show();
                        JSONArray resultResultArray = dataJson.getJSONArray("result");
                        //Toast.makeText(mContext, String.valueOf(resultResultArray.length()), Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < resultResultArray.length(); i++) {
                            JSONObject resultObject = resultResultArray.getJSONObject(i);

                            String name = resultObject.getString("name");
                            String price = resultObject.getString("price");


                            //Toast.makeText(mContext, String.valueOf(name), Toast.LENGTH_SHORT).show();
                            populateFirstList(name, price);
                            populateSecondList(name, price);
                            /*mDynamicListAdapter.setFirstList(populateFirstList(name,price));
                            mDynamicListAdapter.setSecondList(populateSecondList(name, price));*/
                        }
                        mDynamicListAdapter.setFirstList(mFirstList);
                        mDynamicListAdapter.setSecondList(mSecondList);

                        //Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();

                        for (int j = 1; j < 6; j++) {
                            SubCategoryItem subCategoryItem = new SubCategoryItem();
                            subCategoryItem.setCategoryId(String.valueOf(j));
                            subCategoryItem.setIsChecked(Constants.CHECK_BOX_CHECKED_FALSE);
                            subCategoryItem.setSubCategoryName(name);
                            arSubCategory.add(subCategoryItem);
                        }


                        //mDynamicListAdapter.setFirstList(firstList);
                        //mDynamicListAdapter.setSecondList(secondList);
                        dataItem.setSubCategory(arSubCategory);
                        arCategory.add(dataItem);


                        setUPChildRefernces();

                        //Setting Recycle View Adapter
                        settingAdapters();


                        //ListView Adapter
                       /* FoodSectionsAdapterTemp foodSectionsAdapterTemp = new FoodSectionsAdapterTemp(getApplicationContext(),R.layout.row_item_add_to_cart_child,listChildData);
                        radioSelectListView.setAdapter(foodSectionsAdapterTemp);
                        checkSelectListView.setAdapter(foodSectionsAdapterTemp);*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue1.add(request);

    }

    private void settingAdapters() {
        mRecyclerView.setAdapter(mDynamicListAdapter);
    }

    public  /*ArrayList<ListObject>*/ void populateFirstList(String name, String price) {


        //for (int i = 0; i < 5; i++) {
        //ListObject mListObject = new ListObject("Title of first list " + i, "Description here " + i);
        ListObject mListObject = new ListObject(price, name);
        mFirstList.add(mListObject);
        //}

        //return mFirstList;
    }

    public  /*ArrayList<ListObject>*/ void populateSecondList(String name, String price) {


        //for (int i = 0; i < 5; i++) {
        ListObject mListObject = new ListObject(price, name);
        mSecondList.add(mListObject);
        //}

        //return mSecondList;
    }

    private void setUPChildRefernces() {
        for (DataItem data : arCategory) {
//                        Log.i("Item id",item.id);
            ArrayList<HashMap<String, String>> childArrayList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(Constants.CATEGORY_ID, data.getCategoryId());
            mapParent.put(Constants.CATEGORY_NAME, data.getCategoryName());

            int countIsChecked = 0;
            for (SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(Constants.SUB_ID, subCategoryItem.getSubId());
                mapChild.put(Constants.SUB_CATEGORY_NAME, subCategoryItem.getSubCategoryName());
                mapChild.put(Constants.CATEGORY_ID, subCategoryItem.getCategoryId());
                mapChild.put(Constants.IS_CHECKED, subCategoryItem.getIsChecked());

                if (subCategoryItem.getIsChecked().equalsIgnoreCase(Constants.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if (countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(Constants.CHECK_BOX_CHECKED_TRUE);
            } else {
                data.setIsChecked(Constants.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(Constants.IS_CHECKED, data.getIsChecked());
            childItems.add(childArrayList);
            parentItems.add(mapParent);

        }

       /* Constants.parentItems = this.parentItems;
        Constants.childItems = this.childItems;*/

        //Toast.makeText(mContext, String.valueOf(parentItems.size()), Toast.LENGTH_SHORT).show();

        /*myCategoriesExpandableListAdapter = new MyCategoriesExpandableListAdapter(getApplicationContext(),parentItems,childItems,false);
        lvCategory.setAdapter(myCategoriesExpandableListAdapter);*/
    }

    public void upDateNotify(ArrayList<CartChildModel> child) {
        for (int i = 0; i < child.size(); i++) {
            child.get(i).setCheckeddd(false);
        }
    }

    private void fetcher() {
        for (int i = 0; i < shownItems.size(); i++) {
            ArrayList<String> child = new ArrayList<>();
            child.add(childList.get(i));
            expandableData.put(shownItems.get(i), child);
        }

        ExpandableListAdapter expListAdapter = new Features_Expand_Adapter(this, shownItems, expandableData);
        expandableListView.setAdapter(expListAdapter);
    }
}
