package com.apticks.nextclickuser.Activities.OnDemandsActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.OnDemandsAdapters.OnDemandServicePersonsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.OnDemandsPojos.OnDemandServicePersonPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.GET_ONDEMAND_SERVICE_PERSONS;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;

public class OnDemandServicesPersonsActivity extends AppCompatActivity {

    private Context mContext;
    private ImageView back_imageView;
    private RecyclerView ods_persons_recycler;
    private ArrayList<OnDemandServicePersonPojo> onDemandServicePersonPojoArrayList;
    private String vendorId, ods_Name, ods_Id;
    private String serviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_demand_services_persons);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        try {
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra(VENDOR_ID))
                    vendorId = getIntent().getStringExtra(VENDOR_ID);
                if (intent.hasExtra("ods_name"))
                    ods_Name = getIntent().getStringExtra("ods_name");
                if (intent.hasExtra("ods_id"))
                    ods_Id = getIntent().getStringExtra("ods_id");
                if (intent.hasExtra(getString(R.string.service_id)))
                    serviceId = getIntent().getStringExtra(getString(R.string.service_id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getPersons();

    }

    private void getPersons() {
        DialogOpener.dialogOpener(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_ONDEMAND_SERVICE_PERSONS + vendorId + "/" + ods_Id + "/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    DialogOpener.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            try {

                                JSONArray dataArray = jsonObject.getJSONArray("data");
                                if (dataArray.length() > 0) {
                                    onDemandServicePersonPojoArrayList = new ArrayList<>();
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject odsObject = dataArray.getJSONObject(i);
                                        OnDemandServicePersonPojo onDemandServicePersonPojo = new OnDemandServicePersonPojo();
                                        onDemandServicePersonPojo.setId(odsObject.getString("id"));
                                        onDemandServicePersonPojo.setVendor_id(vendorId);
                                        onDemandServicePersonPojo.setName(odsObject.getString("name"));
                                        onDemandServicePersonPojo.setDesc(odsObject.getString("desc"));
                                        try {
                                            onDemandServicePersonPojo.setOd_category_name(odsObject.getJSONObject("od_category").getString("name"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        onDemandServicePersonPojo.setService_duration(odsObject.getString("service_duration"));
                                        onDemandServicePersonPojo.setPrice(odsObject.getInt("price"));
                                        try {
                                            onDemandServicePersonPojo.setDiscount(odsObject.getInt("discount"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            onDemandServicePersonPojo.setStatus(odsObject.getInt("status"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        onDemandServicePersonPojo.setImage(odsObject.getString("image"));

                                        try {
                                            JSONArray timingsArray = odsObject.getJSONArray("service_timings");
                                            ArrayList<String> start_time_list = new ArrayList<>();
                                            ArrayList<String> end_time_list = new ArrayList<>();
                                            if (timingsArray.length() > 0) {
                                                for (int j = 0; j < timingsArray.length(); j++) {
                                                    JSONObject timingObject = timingsArray.getJSONObject(j);
                                                    start_time_list.add(timingObject.getString("start_time"));
                                                    end_time_list.add(timingObject.getString("end_time"));
                                                }
                                                onDemandServicePersonPojo.setService_timing_start(start_time_list);
                                                onDemandServicePersonPojo.setService_timing_end(end_time_list);
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                        onDemandServicePersonPojoArrayList.add(onDemandServicePersonPojo);

                                    }

                                    OnDemandServicePersonsAdapter onDemandServicePersonsAdapter = new OnDemandServicePersonsAdapter(mContext, onDemandServicePersonPojoArrayList, serviceId, ods_Id);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    ods_persons_recycler.setLayoutManager(layoutManager);
                                    ods_persons_recycler.setAdapter(onDemandServicePersonsAdapter);

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UImsgs.showCustomToast(mContext, "No data available", INFO);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server is under maintenance", INFO);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext, "Oops! Something went wrong", ERROR);

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void init() {
        mContext = OnDemandServicesPersonsActivity.this;
        back_imageView = findViewById(R.id.back_imageView);
        ods_persons_recycler = findViewById(R.id.ods_persons_recycler);
    }
}
