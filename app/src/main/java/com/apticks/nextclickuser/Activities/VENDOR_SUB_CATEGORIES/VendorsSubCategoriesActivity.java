package com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.VENDORSADAPTERS.VendorSubCatAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.VendorSubCatPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;

public class VendorsSubCategoriesActivity extends AppCompatActivity {

    private String vendor_id;
    private ArrayList<VendorSubCatPojo> vendorSubCatPojos;
    private RecyclerView vendor_sub_cat_recycler;
    private ImageView back_imageView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors_sub_categories);
        getSupportActionBar().hide();
        vendor_id = getIntent().getStringExtra(VENDOR_ID);
        Log.d("ven subcat ven id",vendor_id);
        mContext = VendorsSubCategoriesActivity.this;
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }*/

        init();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void init() {
        vendor_sub_cat_recycler = findViewById(R.id.vendor_sub_cat_recycler);
        back_imageView = findViewById(R.id.back_imageView);
        profileFetcher();
    }


    private void profileFetcher() {
        DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(VendorsSubCategoriesActivity.this);
        Log.d("url", INDIVIDUAL_VENDOR + vendor_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("vendor response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {


                       try {

                           JSONObject dataObject = jsonObject.getJSONObject("data");
                           try {
                               JSONArray shopByCategoryArray = dataObject.getJSONArray("shop_by_categories");
                               if (shopByCategoryArray.length() > 0) {
                                   vendorSubCatPojos = new ArrayList<>();
                                   for (int i = 0; i < shopByCategoryArray.length(); i++) {
                                       VendorSubCatPojo vendorSubCatPojo = new VendorSubCatPojo();
                                       JSONObject subCatObject = shopByCategoryArray.getJSONObject(i);
                                       vendorSubCatPojo.setId(subCatObject.getString("id"));
                                       vendorSubCatPojo.setList_id(subCatObject.getString("cat_id"));
                                       vendorSubCatPojo.setName(subCatObject.getString("name"));
                                       vendorSubCatPojo.setImage(subCatObject.getString("image"));
                                       vendorSubCatPojo.setVendor_name(dataObject.getString("name"));
                                       vendorSubCatPojos.add(vendorSubCatPojo);
                                   }
                                   VendorSubCatAdapter subCatAdapter = new VendorSubCatAdapter(VendorsSubCategoriesActivity.this, vendorSubCatPojos);
                                   GridLayoutManager layoutManager = new GridLayoutManager(VendorsSubCategoriesActivity.this, 3, GridLayoutManager.VERTICAL, false) {

                                       @Override
                                       public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                           LinearSmoothScroller smoothScroller = new LinearSmoothScroller(VendorsSubCategoriesActivity.this) {

                                               private static final float SPEED = 300f;// Change this value (default=25f)

                                               @Override
                                               protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                   return SPEED / displayMetrics.densityDpi;
                                               }

                                           };
                                           smoothScroller.setTargetPosition(position);
                                           startSmoothScroll(smoothScroller);
                                       }

                                   };
                                   vendor_sub_cat_recycler.setLayoutManager(layoutManager);
                                   vendor_sub_cat_recycler.setAdapter(subCatAdapter);

                               }else{
                                   UImsgs.showCustomToast(mContext,"No category available",WARNING);
                               }
                           }catch (Exception e){
                               e.printStackTrace();
                               UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong with this vendor",WARNING);
                               onBackPressed();
                               finish();
                           }

                       }catch (Exception e){
                           e.printStackTrace();
                       }
                       DialogOpener.dialog.dismiss();
                    }
                    DialogOpener.dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                    UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong with this vendor",WARNING);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong ",ERROR);
            }
        });
        requestQueue.add(stringRequest);
    }

}
