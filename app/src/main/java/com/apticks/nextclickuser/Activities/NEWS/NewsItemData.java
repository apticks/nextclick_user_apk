package com.apticks.nextclickuser.Activities.NEWS;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS;
import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS_SINGLE_ITEM;
import static com.apticks.nextclickuser.Config.Config.MAIN_NEWS;
import static com.apticks.nextclickuser.Config.Config.NEWS_ITEM;
import static com.apticks.nextclickuser.Config.Config.NEWS_VIEW_COUNT_MAKER;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class NewsItemData extends AppCompatActivity {

    private ImageView newsItemImage,news_cancel;
    private TextView newsItemTitle,newsItemContent;
    private String id;
    private Context mContext;
    String token;
    private PreferenceManager preferenceManager;
    int newsType;
    String newsSubCatID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_data_layout);
        getSupportActionBar().hide();
        id = getIntent().getStringExtra("id");
        newsSubCatID = getIntent().getStringExtra("newsSubCatID");
        newsType = Integer.parseInt(getIntent().getStringExtra("type"));
        mContext = NewsItemData.this;
        preferenceManager = new PreferenceManager(mContext);
        token=preferenceManager.getString(TOKEN_KEY);
        init();
        newsFetcher();
        news_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
    }

    private void init(){
        newsItemImage = findViewById(R.id.newsItemImage);
        news_cancel = findViewById(R.id.news_cancel);
        newsItemTitle = findViewById(R.id.newsItem_title);
        newsItemContent = findViewById(R.id.newsItem_data);
    }

    private void newsFetcher(){


        String newsItemurl="";
        if(newsType==LOCAL_NEWS){
            newsItemurl = LOCAL_NEWS_SINGLE_ITEM+newsSubCatID+"/"+id;
        }else{
            newsItemurl = NEWS_ITEM+id;
        }
        Log.d("Single_news_url",newsItemurl);

        DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, newsItemurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if(status && http_code == 200){
                        if(!jsonObject.get("status").getClass().toString().equalsIgnoreCase(jsonObject.get("data").getClass().toString())){
                            JSONObject data = jsonObject.getJSONObject("data");
                            newsItemTitle.setText(data.getString("title"));
                            Picasso.get().load(getIntent().getStringExtra("img")).into(newsItemImage);
                            String news = data.getString("news");
                            news =  Html.fromHtml(news).toString();
                            news = news.replace("\\\\","\\");
                            news = news.replace("\\r\\n","");
                            news = news.replace("\\t","");

                            newsItemContent.setText(news);
                        }

                        viewCountMaker();
                        DialogOpener.dialog.dismiss();

                    }

                }catch (Exception e){
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("Error",error.toString());
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext,"Oops! Something went wrong",ERROR);
            }
        });
        requestQueue.add(stringRequest);
    }

    private void viewCountMaker() {
        Map<String,String> countMap = new HashMap<>();
        countMap.put("device_id", Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        countMap.put("news_id",id);
        if(newsType == MAIN_NEWS){
            countMap.put("type","1");
        }else{
            countMap.put("type","2");
        }
        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NEWS_VIEW_COUNT_MAKER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
