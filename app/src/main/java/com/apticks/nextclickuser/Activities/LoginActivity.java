package com.apticks.nextclickuser.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID_VALUE;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.FCM_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.USER_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


//fb


public class LoginActivity extends AppCompatActivity {

    PreferenceManager preferenceManager;
    Validations validations;
    Animation animation;

    static Context mContext;
    static MainCategoriesActivity mActivity;
    String token;
    private FirebaseAuth mAuth;
    String TAG = "LOGINACTIVITY";
    int RC_SIGN_IN = 101;
    // [END declare_auth]

    public static GoogleSignInClient mGoogleSignInClient;
    private TextView signInButton, registerHere, signUpTextView, click_here, forgot_password, sign_in_button_forget;
    private ImageView gmail_button;

    private EditText userId, password, first_name, last_name, email_id, Mobile_no, reg_password, confirm_password, user_id_forget;
    private RadioButton male_radio_button, female_radio_button;
    private String gender = null;
    private AVLoadingIndicatorView loginProgress, reg_progress;

    private Dialog dialog;
    private static final String EMAIL = "email";

    public static ImageView fb_button;


    //Collections
    Map<String, String> mapToParse = new HashMap<>();


    public LoginActivity(Context mContext) {
        this.mContext = mContext;
        //alertDialogOrderPlaced();
    }

    public LoginActivity(Context mContext, MainCategoriesActivity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        //alertDialogOrderPlaced();
    }


    public void alertDialogLogin(MainCategoriesActivity mActivity, CallbackManager callbackManager) {
        dialog = new Dialog(mActivity);
        Window window = dialog.getWindow();
        /*WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);*/
        preferenceManager = new PreferenceManager(mActivity);
        validations = new Validations();
        animation = AnimationUtils.loadAnimation(mActivity, R.anim.shake);
        dialog.setContentView(R.layout.activity_login);
        //dialog.onBackPressed();
        //dialog.getWindow();
        //dialog.
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        //dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        dialog.setCancelable(true);

        signInButton = dialog.findViewById(R.id.sign_in_button);
        registerHere = dialog.findViewById(R.id.register_here);
        forgot_password = dialog.findViewById(R.id.forgot_password);
        userId = dialog.findViewById(R.id.user_id);
        password = dialog.findViewById(R.id.password);

        loginProgress = dialog.findViewById(R.id.login_progress);
        gmail_button = dialog.findViewById(R.id.gmail_button);


        //fb button


//        AppEventsLogger.activateApp(mActivity);
        // callbackManager = CallbackManager.Factory.create();


        fb_button = dialog.findViewById(R.id.fb_button);
       /* fb_button.setReadPermissions(Arrays.asList(EMAIL));
        // Callback registration

        fb_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "facebook:onCancel");
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "facebook:onError", exception);
            }
        });


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d(TAG, "facebook:loginmanager"+loginResult.toString());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d(TAG, "facebook:loginmanager cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d(TAG, "facebook:loginmanager",exception);
                    }
                });*/

       /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)//request Id Token needed
                .requestEmail()
                .build();
        // [END config_signin]


        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);*/

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);


        // [START initialize_auth]
        //Initialize Firebase Auth
        //mAuth = FirebaseAuth.getInstance();

        signInButtonClick();//Sign IN
        registerHereClick(mActivity, callbackManager);// New Registaration
        //fbButtonClick();
        gmailButtonClick();
        forgetPasswordClick(mActivity, callbackManager);

        dialog.show();
    }

    private void forgetPasswordClick(MainCategoriesActivity mActivity, CallbackManager callbackManager) {

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setContentView(R.layout.dialog_forget_password);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                sign_in_button_forget = dialog.findViewById(R.id.sign_in_button_forget);
                user_id_forget = dialog.findViewById(R.id.user_id_forget);

                signInButtonForget();

            }
        });
    }

    private Map<String, String> fetcherForgetPassword() {
        mapToParse.put("identity", user_id_forget.getText().toString().trim());
        return mapToParse;
    }

    private void signInButtonForget() {
        sign_in_button_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validEmailForget()) {
                    JSONObject jsonObject = new JSONObject(fetcherForgetPassword());
                    final String data = jsonObject.toString();
                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObjectResponce = new JSONObject(response);
                                if (jsonObjectResponce.getBoolean("status")) {
                                    UImsgs.showSnackBar(v, String.valueOf(Html.fromHtml(jsonObjectResponce.getString("message").toString())));
                                } else if (!jsonObjectResponce.getBoolean("status")) {
                                    UImsgs.showSnackBar(v, jsonObjectResponce.getString("data"));
                                }
                            } catch (JSONException e) {
                                UImsgs.showCustomToast(mContext, "Server Under Maintenance ", INFO);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();

                        }
                    }) {

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return data == null ? null : data.getBytes("utf-8");

                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);


                }

            }
        });
    }

    private boolean validEmailForget() {
        if (validations.isValidEmail(user_id_forget.getText().toString())) {
            return false;
        }
        return true;
    }

    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        //showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(mActivity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        // hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_facebook]

    public void dialogDismiss() {
        dialog.dismiss();
    }


    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }
    // [END on_start_check_user]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        /*if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }
        }*/

        /*// Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);*/
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        //showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        //hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]

    // [START signin]
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void registerHereClick(MainCategoriesActivity mActivity, CallbackManager callbackManager) {
        registerHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mContext.startActivity(new Intent(mContext,SignUpActivity.class));

                dialog.setContentView(R.layout.activity_sign_up);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                first_name = dialog.findViewById(R.id.first_name);
                last_name = dialog.findViewById(R.id.last_name);
                email_id = dialog.findViewById(R.id.email_id);
                Mobile_no = dialog.findViewById(R.id.Mobile_no);
                male_radio_button = dialog.findViewById(R.id.male_radio_button);
                female_radio_button = dialog.findViewById(R.id.female_radio_button);
                reg_password = dialog.findViewById(R.id.reg_password);
                confirm_password = dialog.findViewById(R.id.confirm_password);
                signUpTextView = dialog.findViewById(R.id.sign_up_text_view);
                click_here = dialog.findViewById(R.id.click_here);

                reg_progress = dialog.findViewById(R.id.reg_progress);

                signUpTextViewClick();

                clickHereLogin(mActivity, callbackManager);


                //Intent intent = new Intent(get)

            }
        });
    }

    private void clickHereLogin(MainCategoriesActivity mActivity, CallbackManager callbackManager) {
        click_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogLogin(mActivity, callbackManager);
            }
        });
    }

    private void signUpTextViewClick() {
        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reg_progress.setVisibility(View.VISIBLE);
                signUpTextView.setVisibility(View.GONE);
                register(v);
            }
        });
    }

    public void register(View view) {
        if (isValidInputReg()) {
            hitRegistartionAPI(view);
        } else {
            //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
            reg_progress.setVisibility(View.GONE);
            signUpTextView.setVisibility(View.VISIBLE);
        }
    }

    private void hitRegistartionAPI(View view) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        Log.d("reg url", CreateUsers);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CreateUsers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //UImsgs.showToast(mContext, "asd" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status") && jsonObject.getInt("http_code") == 200) {
                        //UImsgs.showToast(mContext, jsonObject.getString("message"));
                        UImsgs.showToast(mContext, jsonObject.getString("message"));
                        reg_progress.setVisibility(View.GONE);
                        signUpTextView.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    } else {
                        JSONObject jsonObject1 = new JSONObject(response);
                        String message = jsonObject1.getString("message");
                        UImsgs.showToast(mContext, message);
                        //UImsgs.showToast(mContext, "Sorry for inconvenience! Registration not possible");
                        reg_progress.setVisibility(View.GONE);
                        signUpTextView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "Unable to register please try after some time");
                    reg_progress.setVisibility(View.GONE);
                    signUpTextView.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, "Unable to register please try after some time");
                reg_progress.setVisibility(View.GONE);
                signUpTextView.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", first_name.getText().toString());
                params.put("last_name", last_name.getText().toString());
                params.put("mobile", Mobile_no.getText().toString());
                params.put("gender", gender);
                params.put("email", email_id.getText().toString());
                params.put("password", reg_password.getText().toString());
                params.put("confirm_password", confirm_password.getText().toString());
                return params;
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private boolean isValidInputReg() {

        if (male_radio_button.isChecked()) {
            gender = "Male";
        }
        if (female_radio_button.isChecked()) {
            gender = "FeMale";
        }

        if (validations.isBlank(first_name)) {
            first_name.startAnimation(animation);
            first_name.setError(IErrors.EMPTY);
            first_name.requestFocus();
            return false;
        } else if (validations.isValidEmail(email_id.getText().toString())) {
            email_id.startAnimation(animation);
            email_id.setError(IErrors.INVALID_EMAIL);
            email_id.requestFocus();
            return false;
        } else if (validations.isValidPhone(Mobile_no.getText().toString())) {
            Mobile_no.startAnimation(animation);
            Mobile_no.setError(IErrors.MOBILE_INVALID);
            Mobile_no.requestFocus();
            return false;
        } else if (validations.isBlank(reg_password.getText().toString())) {
            reg_password.startAnimation(animation);
            reg_password.setError(IErrors.PASSWORD_EMPTY);
            reg_password.requestFocus();
            return false;
        } else if (validations.isBlank(confirm_password.getText().toString())) {
            confirm_password.startAnimation(animation);
            confirm_password.setError(IErrors.PASSWORD_EMPTY);
            confirm_password.requestFocus();
            return false;
        } else if (!validations.isMatching(reg_password, confirm_password)) {
            reg_password.startAnimation(animation);
            reg_password.setError(IErrors.PASSWORD_SAME);
            reg_password.requestFocus();

            confirm_password.startAnimation(animation);
            confirm_password.setError(IErrors.PASSWORD_SAME);
            confirm_password.requestFocus();

            return false;
        } else if (gender == null) {
            UImsgs.showCustomToast(mContext, "Please provide gender", WARNING);
            return false;
        }
        return true;
    }

    private void fbButtonClick() {
        fb_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void gmailButtonClick() {
        gmail_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    private void signInButtonClick() {
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInput()) {
                    JSONObject json = new JSONObject(fetcher());

                    callLoginAPI(json);
                }

            }
        });
    }

    private void callLoginAPI(JSONObject jsonObject) {
        signInButton.setVisibility(View.GONE);
        loginProgress.setVisibility(View.VISIBLE);
        final String data = jsonObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObjectStatus = new JSONObject(response.toString());
                    String status = jsonObjectStatus.getString("status");
                    String messagae = jsonObjectStatus.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Login SuccessFully.!")) {

                        dialog.dismiss();
                        JSONObject dataObject = jsonObjectStatus.getJSONObject("data");
                        String token = dataObject.getString("token");
                        //UImsgs.showToast(mContext, token);
                        preferenceManager.putString(TOKEN_KEY, token);
                        try {
                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                            if (!task.isSuccessful()) {
//To do//
                                                return;
                                            }

// Get the Instance ID token//
                                            String token = task.getResult().getToken();
                                            //String msg = getString(R.string.fcm_token, token);
                                            Log.d("TAG", token);
                                            grantFCMPermission(token);

                                        }
                                    });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        loginProgress.setVisibility(View.GONE);
                        signInButton.setVisibility(View.VISIBLE);


                    } else {
                        //dialog.dismiss();
                        UImsgs.showToast(mContext, messagae);
                        loginProgress.setVisibility(View.GONE);
                        signInButton.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //UImsgs.showToast(mContext,response);
                //dialog.dismiss();
                //dialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, String.valueOf(error));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }


            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void grantFCMPermission(final String msg) {

        DialogOpener.dialogOpener(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("fcm_res", response);
                if (response != null) {
                    try {
                        DialogOpener.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            preferenceManager.putString(FCM_TOKEN, msg);


                            /*startActivity(new Intent(mContext,ServicesActivity.class));
                            finish();*/
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(mContext, "Something went wrong", WARNING);
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server under maintenance", INFO);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private boolean isValidInput() {
        if (validations.isBlank(userId)) {
            userId.startAnimation(animation);
            userId.setError(IErrors.EMPTY);
            userId.requestFocus();
            return false;
        } /*else if (validations.isValidEmail(userId.getText().toString())) {
            userId.startAnimation(animation);
            userId.setError(IErrors.INVALID_EMAIL);
            userId.requestFocus();
            return false;

        } */ else if (validations.isBlank(password)) {
            password.startAnimation(animation);
            password.setError(IErrors.PASSWORD_EMPTY);
            password.requestFocus();
            return false;
        }/*else if (validations.)*/
        return true;
    }

    public Map<String, String> fetcher() {

        mapToParse.put("identity", userId.getText().toString().trim());
        mapToParse.put("password", password.getText().toString().trim());
        return mapToParse;
    }


    public static void logOut(Context context) {
        mGoogleSignInClient.signOut().addOnCompleteListener(mActivity,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        UImsgs.showCustomToast(mActivity, "Successfully Logged Out", INFO);
                    }
                });
        context.startActivity(new Intent(context, MainCategoriesActivity.class));


    }


}
