package com.apticks.nextclickuser.Activities.PROFILE;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.ProfileServicesAdapter;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.interfaces.CloseCallBack;
import com.apticks.nextclickuser.utils.UserData;

import java.util.ArrayList;

public class ProfileServicesActivity extends AppCompatActivity implements View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvServices;

    private ArrayList<ServicesPojo> listOfServicesList;

    private String vendor_id = "";
    private String tempNameVendor = "";
    private String idStringVendor = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile_services);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("tempNameVendor"))
                tempNameVendor = bundle.getString("tempNameVendor");
            if (bundle.containsKey("idStringVendor"))
                idStringVendor = bundle.getString("idStringVendor");
            if (bundle.containsKey("vendor_id"))
                vendor_id = bundle.getString("vendor_id");
        }
    }

    private void initializeUi() {
        rvServices = findViewById(R.id.rvServices);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        listOfServicesList = UserData.getInstance().getServicesList();
        if (listOfServicesList != null) {
            if (listOfServicesList.size() != 0) {
                initializeAdapter();
            }
        }
    }

    private void initializeAdapter() {
        ProfileServicesAdapter profileServicesAdapter = new ProfileServicesAdapter(this, listOfServicesList, tempNameVendor, idStringVendor, vendor_id);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvServices.setLayoutManager(layoutManager);
        rvServices.setItemAnimator(new DefaultItemAnimator());
        rvServices.setAdapter(profileServicesAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void close() {
        finish();
    }
}
