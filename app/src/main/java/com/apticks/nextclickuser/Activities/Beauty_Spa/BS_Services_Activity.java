package com.apticks.nextclickuser.Activities.Beauty_Spa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.BS_Adapters.BS_servicesAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.BSPojo.BSServicesPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.BS_PACKAGES;

public class BS_Services_Activity extends AppCompatActivity {

    RecyclerView bs_services_recycler;
    ArrayList<BSServicesPojo> bsServicesList;
    String vendor_id;
    private Context context;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bs_services);
        getSupportActionBar().hide();
        vendor_id = getIntent().getStringExtra("vendor_id");
        context = getApplicationContext();
        init();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(BS_Services_Activity.this, R.color.colorPrimary));
        }
        packagesFetcher();
    }


    private void packagesFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BS_PACKAGES + "0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        /*JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject amenitiesObject = dataObject.getJSONObject("amenities");
                        Iterator x = amenitiesObject.keys();
                        JSONArray amenitiesArray = new JSONArray();
                        while (x.hasNext()) {
                            String key = (String) x.next();
                            amenitiesArray.put(amenitiesObject.get(key));
                        }
                        bsServicesList = new ArrayList<>();

                        for (int i = 1; i <= amenitiesArray.length(); i++) {
                            JSONObject amenitiesDataObject = amenitiesArray.getJSONObject(i);
                            BSServicesPojo bsServicesPojo = new BSServicesPojo();

                            bsServicesList.add(bsServicesPojo);
                        }

*/
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        if(dataArray.length()>0){
                            bsServicesList = new ArrayList<>();
                            for(int i=0 ; i<dataArray.length() ; i++){
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                BSServicesPojo bsServicesPojo = new BSServicesPojo();
                                bsServicesPojo.setId(dataObject.getString("id"));
                                bsServicesPojo.setVendor_id(dataObject.getString("vendor_id"));
                                bsServicesPojo.setName(dataObject.getString("name"));
                                bsServicesPojo.setPrice(dataObject.getString("price"));
                                bsServicesPojo.setDiscount(dataObject.getString("discount"));
                                bsServicesPojo.setValid_for(dataObject.getString("valid_for"));
                                bsServicesList.add(bsServicesPojo);
                            }

                            BS_servicesAdapter bsVendorAdapter = new BS_servicesAdapter(context, bsServicesList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }

                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }

                            };
                            bs_services_recycler.setLayoutManager(layoutManager);

                            bs_services_recycler.setAdapter(bsVendorAdapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        });

        requestQueue.add(stringRequest);

    }


    private void init() {
        bs_services_recycler = findViewById(R.id.bs_services_recycler);
    }
}
