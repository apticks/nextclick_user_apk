package com.apticks.nextclickuser.interfaces;

public interface CloseCallBack {
    void close();
}
