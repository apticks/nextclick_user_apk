package com.apticks.nextclickuser.interfaces;

public interface SelectedServiceTimingsCallBack {
    void selectedServiceTimings(int selectedServiceTimeId);
}
