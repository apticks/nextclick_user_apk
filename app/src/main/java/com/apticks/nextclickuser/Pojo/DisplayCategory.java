package com.apticks.nextclickuser.Pojo;

public class DisplayCategory {
    private int vendor_user_Id;
    private String id;
    private String name;
    private String image;
    private String location;
    private String location_address;
    private int vender_id;

    public int getVender_id() {
        return vender_id;
    }

    public void setVender_id(int vender_id) {
        this.vender_id = vender_id;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getVendor_user_Id() {
        return vendor_user_Id;
    }

    public void setVendor_user_Id(int vendor_user_Id) {
        this.vendor_user_Id = vendor_user_Id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
