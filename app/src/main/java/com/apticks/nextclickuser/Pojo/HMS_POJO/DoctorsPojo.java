package com.apticks.nextclickuser.Pojo.HMS_POJO;

public class DoctorsPojo {
    String id;
    String name;
    String qualifictaion;
    String startTime;
    String endTime;
    String experience;
    String specialization;
    String image;

    public DoctorsPojo(String id, String name, String qualifictaion, String startTime, String endTime, String experience, String specialization, String image) {
        this.id = id;
        this.name = name;
        this.qualifictaion = qualifictaion;
        this.startTime = startTime;
        this.endTime = endTime;
        this.experience = experience;
        this.specialization = specialization;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQualifictaion() {
        return qualifictaion;
    }

    public void setQualifictaion(String qualifictaion) {
        this.qualifictaion = qualifictaion;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
