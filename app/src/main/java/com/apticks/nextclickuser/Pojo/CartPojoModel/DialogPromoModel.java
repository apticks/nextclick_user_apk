package com.apticks.nextclickuser.Pojo.CartPojoModel;

public class DialogPromoModel {
             int id = 5;
             String promo_title;
             String promo_code;
             int promo_type;
             String promo_label;
             String valid_from;
             String valid_to;
             int discount_type;
             int discount;
             int uses;
             int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPromo_title() {
        return promo_title;
    }

    public void setPromo_title(String promo_title) {
        this.promo_title = promo_title;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public int getPromo_type() {
        return promo_type;
    }

    public void setPromo_type(int promo_type) {
        this.promo_type = promo_type;
    }

    public String getPromo_label() {
        return promo_label;
    }

    public void setPromo_label(String promo_label) {
        this.promo_label = promo_label;
    }

    public String getValid_from() {
        return valid_from;
    }

    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public String getValid_to() {
        return valid_to;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public int getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(int discount_type) {
        this.discount_type = discount_type;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getUses() {
        return uses;
    }

    public void setUses(int uses) {
        this.uses = uses;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
