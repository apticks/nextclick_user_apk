package com.apticks.nextclickuser.Pojo.BSPojo;

public class BSVendorsPojo {
  String id;
  String name;
  String vendor_user_id;
  String image;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVendor_user_id() {
    return vendor_user_id;
  }

  public void setVendor_user_id(String vendor_user_id) {
    this.vendor_user_id = vendor_user_id;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}
