package com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hosp_doctor_id")
    @Expose
    private Integer hospDoctorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("image")
    @Expose
    private String image;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHospDoctorId() {
        return hospDoctorId;
    }

    public void setHospDoctorId(Integer hospDoctorId) {
        this.hospDoctorId = hospDoctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
