package com.apticks.nextclickuser.Pojo.CartPojoModel;

import java.util.ArrayList;

public class CartPojo {

    private String id;
    private String name;
    private String image;
    private String price;
    private String quantity;
    private String specification;
    private String productOfferPrice;
    private ArrayList<CartPojo> secItems;
    private String secName;
    private String totalQuantity;

    public ArrayList<CartPojo> getSecItems() {
        return secItems;
    }

    public void setSecItems(ArrayList<CartPojo> secItems) {
        this.secItems = secItems;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }
//private String secItems;

    public String getProductOfferPrice() {
        return productOfferPrice;
    }

    public void setProductOfferPrice(String productOfferPrice) {
        this.productOfferPrice = productOfferPrice;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}
