package com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ViewDoctorBookingDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private Integer trackId;
    @SerializedName("vendor_id")
    @Expose
    private Integer vendorId;
    @SerializedName("otp")
    @Expose
    private Integer otp;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("sub_total")
    @Expose
    private Integer subTotal;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("promo_discount")
    @Expose
    private Integer promoDiscount;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("used_wallet_amount")
    @Expose
    private Integer usedWalletAmount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("response_message")
    @Expose
    private Object responseMessage;
    @SerializedName("booking_status")
    @Expose
    private Integer bookingStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("booking_items")
    @Expose
    private LinkedList<BookingItemDetails> listOfBookingItemDetails;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTrackId() {
        return trackId;
    }

    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Integer getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Integer promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Integer usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Object getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(Object responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public LinkedList<BookingItemDetails> getListOfBookingItemDetails() {
        return listOfBookingItemDetails;
    }

    public void setListOfBookingItemDetails(LinkedList<BookingItemDetails> listOfBookingItemDetails) {
        this.listOfBookingItemDetails = listOfBookingItemDetails;
    }
}
