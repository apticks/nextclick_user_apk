package com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class DoctorBookingHistoryDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private Integer trackId;
    @SerializedName("sub_total")
    @Expose
    private Double subTotal;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("booking_status")
    @Expose
    private Integer bookingStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("booking_items")
    @Expose
    private LinkedList<BookingItem> listOfBookingItems;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTrackId() {
        return trackId;
    }

    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public LinkedList<BookingItem> getListOfBookingItems() {
        return listOfBookingItems;
    }

    public void setListOfBookingItems(LinkedList<BookingItem> listOfBookingItems) {
        this.listOfBookingItems = listOfBookingItems;
    }
}
