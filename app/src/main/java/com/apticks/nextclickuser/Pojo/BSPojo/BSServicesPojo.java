package com.apticks.nextclickuser.Pojo.BSPojo;

public class BSServicesPojo {

    String id;
    String name;
    String vendor_id;
    String price;
    String valid_for;
    String discount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getValid_for() {
        return valid_for;
    }

    public void setValid_for(String valid_for) {
        this.valid_for = valid_for;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
