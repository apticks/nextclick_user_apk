package com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingItem {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;


    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }
}
