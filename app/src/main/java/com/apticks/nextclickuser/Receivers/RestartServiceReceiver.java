package com.apticks.nextclickuser.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.apticks.nextclickuser.Services.InAppMessagingService;
import com.apticks.nextclickuser.Services.RestartService;


public class RestartServiceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(RestartServiceReceiver.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        context.startService(new Intent(context, RestartService.class));
        context.startService(new Intent(context, InAppMessagingService.class));
    }
}
