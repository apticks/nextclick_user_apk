package com.apticks.nextclickuser.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apticks.nextclickuser.R;

public class SelectyAddressAdapter extends RecyclerView.Adapter<SelectyAddressAdapter.ViewHolder> {



    @NonNull
    @Override
    public SelectyAddressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_selecty_address_adapter, parent, false);
        return new SelectyAddressAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectyAddressAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
