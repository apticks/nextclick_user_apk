package com.apticks.nextclickuser.Adapters.HospitalityAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.GetAppointmentAndGetServiceActivity;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.DoctorPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;

public class HospitalityDoctorsAdapter extends RecyclerView.Adapter<HospitalityDoctorsAdapter.ViewHolder> {

    private Context context;

    private List<DoctorPojo> data;

    private String serviceId = "";
    private String specialityId = "";

    public HospitalityDoctorsAdapter(Context activity, List<DoctorPojo> itemPojos, String specialityId, String serviceId) {
        this.context = activity;
        this.data = itemPojos;
        this.specialityId = specialityId;
        this.serviceId = serviceId;
    }

    @NonNull
    @Override
    public HospitalityDoctorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_hospitality_doctor_item, parent, false);
        return new HospitalityDoctorsAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HospitalityDoctorsAdapter.ViewHolder holder, int position) {
        DoctorPojo doctorPojo = data.get(position);

        holder.doctor_name.setText(doctorPojo.getName());
        holder.doctor_experience.setText(doctorPojo.getExperience() + " years exp");
        holder.doctor_qualification.setText(doctorPojo.getQualification());
        holder.doctor_specialization.setText(doctorPojo.getSpecialization());
        holder.languages.setText(doctorPojo.getLanguages());
        holder.doctor_discount.setText("₹" + doctorPojo.getFee());

        int discount = doctorPojo.getDiscount();
        double discountPrice = doctorPojo.getFee() * (discount / 100.00);
        holder.doctor_fee.setText("₹" + (doctorPojo.getFee() - discountPrice));
        Picasso.get()
                .load(doctorPojo.getImage())
                .placeholder(R.drawable.loader_gif)
                .into(holder.doctor_image);


        holder.get_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GetAppointmentAndGetServiceActivity.class);
                intent.putExtra(VENDOR_ID, doctorPojo.getVendor_id());
                intent.putExtra("s_id", doctorPojo.getId());
                intent.putExtra("od_cat_id", specialityId);
                intent.putExtra(context.getString(R.string.service_id), serviceId);
                intent.putExtra("type", context.getString(R.string.hospital));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView doctor_image;
        private CardView get_appointment;
        private TextView doctor_name, doctor_experience, doctor_discount, doctor_fee, doctor_qualification, doctor_specialization, languages;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            doctor_name = itemView.findViewById(R.id.doctor_name);
            doctor_experience = itemView.findViewById(R.id.doctor_experience);
            doctor_discount = itemView.findViewById(R.id.doctor_discount);
            doctor_fee = itemView.findViewById(R.id.doctor_fee);
            doctor_qualification = itemView.findViewById(R.id.doctor_qualification);
            doctor_specialization = itemView.findViewById(R.id.doctor_specialization);
            languages = itemView.findViewById(R.id.languages);
            doctor_image = itemView.findViewById(R.id.doctor_image);
            get_appointment = itemView.findViewById(R.id.get_appointment);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            DoctorPojo doctorPojo = data.get(getLayoutPosition());
            if (doctorPojo != null) {
                Intent intent = new Intent(context, GetAppointmentAndGetServiceActivity.class);
                intent.putExtra(VENDOR_ID, doctorPojo.getVendor_id());
                intent.putExtra("s_id", doctorPojo.getId());
                intent.putExtra("od_cat_id", specialityId);
                intent.putExtra(context.getString(R.string.service_id), serviceId);
                intent.putExtra("type", context.getString(R.string.hospital));
                context.startActivity(intent);
            }
        }
    }
}

