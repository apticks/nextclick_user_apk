package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.apticks.nextclickuser.Fragments.DoctorsBookingHistoryFragment;
import com.apticks.nextclickuser.Fragments.OnDemandServiceBookingHistoryFragment;
import com.apticks.nextclickuser.utils.TabInfo;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class BookingsTabAdapter implements TabLayout.OnTabSelectedListener {

    private Context context;
    private Fragment fragment;
    private TabLayout tabLayout;
    private int fragmentContainer;
    private FragmentManager fragmentManager;

    private final ArrayList<TabInfo> mTabs = new ArrayList<>();

    public BookingsTabAdapter(Context context, FragmentManager fragmentManager, TabLayout tabLayout, int fragmentContainer) {
        this.context = context;
        this.tabLayout = tabLayout;
        this.fragmentManager = fragmentManager;
        this.fragmentContainer = fragmentContainer;
        tabLayout.addOnTabSelectedListener(this);
    }

    public void addTab(TabLayout.Tab tabSpec, Class<?> clss, Bundle args) {
        String tag = (String) tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        tabLayout.addTab(tabSpec);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        String tabName = (String) tab.getTag();
        TabInfo info = mTabs.get(position);
        switch (position) {
            case 0:
                fragment = new DoctorsBookingHistoryFragment();
                break;
            case 1:
                fragment = new OnDemandServiceBookingHistoryFragment();
                break;
            default:
                break;
        }

        fragmentManager.beginTransaction().replace(fragmentContainer, fragment).commit();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
