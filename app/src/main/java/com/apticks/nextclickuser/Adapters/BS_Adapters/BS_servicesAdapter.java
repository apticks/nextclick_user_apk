package com.apticks.nextclickuser.Adapters.BS_Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Pojo.BSPojo.BSServicesPojo;

import com.apticks.nextclickuser.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.BS_PACKAGE_ORDERING;

public class BS_servicesAdapter  extends RecyclerView.Adapter<BS_servicesAdapter.ViewHolder> {

    List<BSServicesPojo> data;

    LayoutInflater inflter;
    Context context;
    String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public BS_servicesAdapter(Context activity, List<BSServicesPojo> bsServicesPojos) {
        this.context = activity;
        this.data = bsServicesPojos;
    }


    @NonNull
    @Override
    public BS_servicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.bs_items_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new BS_servicesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BS_servicesAdapter.ViewHolder holder, int position) {
        BSServicesPojo bsServicesPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();


        //Picasso.get().load(currentBsVendor.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/

        holder.serviceName.setText(bsServicesPojo.getName());
        holder.servicePrice.setText("₹ "+bsServicesPojo.getPrice());
        holder.serviceDiscountPrice.setText("₹ "+bsServicesPojo.getDiscount());
        holder.validFor.setText("\t"+bsServicesPojo.getValid_for()+" person(s)");
        holder.addPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,String> dataMap = new HashMap<>();
                dataMap.put("package_id",bsServicesPojo.getId());
                dataMap.put("order_id","NCEU-"+generateRandomNumber());
                dataMap.put("qty","1");
                dataMap.put("price",bsServicesPojo.getPrice());
                JSONObject jsonObject = new JSONObject(dataMap);
                packageAdder(jsonObject);


            }
        });





    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName,servicePrice,serviceDiscountPrice,validFor,details,addPackage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            serviceName = itemView.findViewById(R.id.bs_service_name);
            servicePrice = itemView.findViewById(R.id.bs_service_price);
            serviceDiscountPrice = itemView.findViewById(R.id.bs_service_discount_price);
            validFor = itemView.findViewById(R.id.service_valid_for);
            details = itemView.findViewById(R.id.service_details);
            addPackage = itemView.findViewById(R.id.service_add);


        }
    }

    public String generateRandomNumber() {
        int randomNumber;
        int range = 9;  // to generate a single number with this range, by default its 0..9
        int length = 4; // by default length is 4
        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        //randomNumber = Integer.parseInt(s);

        return s;
    }

    private void packageAdder(JSONObject dataMap){

        final String dataStr =dataMap.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BS_PACKAGE_ORDERING, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if(status && http_code==201){
                        Toast.makeText(context, "Package Added Successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X_AUTH_TOKEN", token);
                params.put("content-type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);


    }
}

