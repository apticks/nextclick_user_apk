package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MainBannersSlidingAdapter extends RecyclerView.Adapter<MainBannersSlidingAdapter.ViewHolder> {

    List<SlidersModelClass> data;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public MainBannersSlidingAdapter(Context activity, List<SlidersModelClass> brandsPojos) {
        this.context = activity;
        this.data = brandsPojos;
    }


    @NonNull
    @Override
    public MainBannersSlidingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.slidingimages_layout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new MainBannersSlidingAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainBannersSlidingAdapter.ViewHolder holder, int position) {
        SlidersModelClass brandsPojos = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        try {
            Picasso.get().load(brandsPojos.getBanners()).placeholder(R.drawable.noimage).into(holder.image);
        }catch (Exception e){
            e.printStackTrace();
        }
        /*holder.displayLocation.setText(hospital.getLocation());*/





    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);




        }
    }
}