package com.apticks.nextclickuser.Adapters.Brands;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Pojo.Brands.BrandsPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.ViewHolder> {

    List<BrandsPojo> data;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public BrandsAdapter(Context activity, List<BrandsPojo> brandsPojos) {
        this.context = activity;
        this.data = brandsPojos;
    }


    @NonNull
    @Override
    public BrandsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.brand_image_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new BrandsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandsAdapter.ViewHolder holder, int position) {
        BrandsPojo brandsPojos = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        try {
            Picasso.get().load(brandsPojos.getImage()).placeholder(R.drawable.noimage).into(holder.brandImage);
        }catch (Exception e){
            e.printStackTrace();
        }
        holder.brand_name.setText(brandsPojos.getName());

        holder.brand_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VendorsActivity.class);
                intent.putExtra("brand_id",brandsPojos.getId());//need to pass sub category id
                Log.d("brand_id",brandsPojos.getId());
                intent.putExtra("cat_id",brandsPojos.getCatID());//need to pass sub category id

                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandImage;
        LinearLayout brand_layout;
        TextView brand_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            brandImage = itemView.findViewById(R.id.brand_image);
            brand_layout = itemView.findViewById(R.id.brand_layout);
            brand_name = itemView.findViewById(R.id.brand_name);



        }
    }
}