package com.apticks.nextclickuser.Adapters.UserHistory;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.OrderStatusActivity;
import com.apticks.nextclickuser.Adapters.ReceiptItemsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.OrderHistory.CurrentOrder;
import com.apticks.nextclickuser.Pojo.ReceiptItemPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.CANCEL_ORDER;
import static com.apticks.nextclickuser.Config.Config.OrderInDetail;
import static com.apticks.nextclickuser.Config.Config.SINGLE_ORDER_DETAILS;
import static com.apticks.nextclickuser.Constants.Constants.INFO;

public class PresentOrderOuterAdapter extends RecyclerView.Adapter<PresentOrderOuterAdapter.ViewHolder> {
    ArrayList<CurrentOrder> currentOrdersArrayList = new ArrayList<>();
    private Context mContext;
    RecyclerView recycler_view_receipt_data;
    ImageView cancel_dialog;
    Dialog dialog;
    Window window;
    int taxValue;

    ArrayList<ReceiptItemPojo> itemPojos;
    RecyclerView receipt_items_recycler;
    TextView order_no, ordered_from, timing, sub_total, delivery_charges, grand_total, item_text_tv, qty_text_tv, price_text_tv, tax;
    LinearLayout delivery_charges_layout;

    String type;


    public PresentOrderOuterAdapter(ArrayList<CurrentOrder> currentOrdersArrayList, String type) {
        this.currentOrdersArrayList = currentOrdersArrayList;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.present_order_outer_adapter, parent, false);
        mContext = parent.getContext();
        return new PresentOrderOuterAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CurrentOrder currentOrder = currentOrdersArrayList.get(position);
        holder.text_track.setText(currentOrder.getOrder_track());
        holder.text_discount.setText(String.valueOf(currentOrder.getDiscount()));
        holder.text_total.setText(String.valueOf(currentOrder.getTotal()));
        holder.order_status.setText(currentOrder.getOrderStatus());
        holder.text_date.setText(String.valueOf(currentOrder.getCreated_at()));
        holder.returent_name.setText(currentOrder.getName());

        holder.tvVendorName.setText(currentOrder.getName());
        holder.tvVendorEmail.setText(currentOrder.getVendorEmailId());
        holder.tvVendorMobileNumber.setText(currentOrder.getVendorMobileNumber());

        if (!currentOrder.getOtp().equalsIgnoreCase("null")) {
            holder.otp_layout.setVisibility(View.VISIBLE);
            holder.otp.setText(currentOrder.getOtp());


        }

        //Items
        if (currentOrder.getCurrentOrdersArrayListInnerItems() != null) {
            ArrayList<CurrentOrder> currentOrdersArrayListInnerItems = currentOrder.getCurrentOrdersArrayListInnerItems();

            PresentOrderInnerItemAdapter presentOrderInnerItemAdapter = new PresentOrderInnerItemAdapter(currentOrdersArrayListInnerItems);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                @Override
                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                        private static final float SPEED = 300f;
                        // Change this value (default=25f)

                        @Override
                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                            return SPEED / displayMetrics.densityDpi;
                        }
                    };
                    smoothScroller.setTargetPosition(position);
                    startSmoothScroll(smoothScroller);
                }
            };
            holder.recycler_inner_items.setLayoutManager(linearLayoutManager);
            holder.recycler_inner_items.setAdapter(presentOrderInnerItemAdapter);
        } else {
            //Toast.makeText(mContext, "ELSe", Toast.LENGTH_SHORT).show();
        }
        //Sun Items
        if (currentOrder.getCurrentOrdersArrayListInnerSubItems() != null) {
            //Toast.makeText(mContext, "IF", Toast.LENGTH_SHORT).show();
            ArrayList<CurrentOrder> currentOrdersArrayListInnerItems = currentOrder.getCurrentOrdersArrayListInnerSubItems();
            PresentOrderInnerItemAdapter presentOrderInnerItemAdapter = new PresentOrderInnerItemAdapter(currentOrdersArrayListInnerItems);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                @Override
                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                        private static final float SPEED = 300f;// Change this value (default=25f)

                        @Override
                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                            return SPEED / displayMetrics.densityDpi;
                        }
                    };
                    smoothScroller.setTargetPosition(position);
                    startSmoothScroll(smoothScroller);
                }
            };
            holder.recycler_inner_sub_items.setLayoutManager(linearLayoutManager);
            holder.recycler_inner_sub_items.setAdapter(presentOrderInnerItemAdapter);
        } else {
            Toast.makeText(mContext, "Something is Wrong", Toast.LENGTH_SHORT).show();
        }


        holder.view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.view_details.getText().toString().trim().equalsIgnoreCase("View order Details")) {
                    holder.view_details.setText("Hide order Details");
                    holder.details_card.setVisibility(View.VISIBLE);
                } else {
                    holder.view_details.setText("View order Details");
                    holder.details_card.setVisibility(View.GONE);
                }
            }
        });

        holder.text_recepit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View myView = LayoutInflater.from(mContext).inflate(R.layout.dialog_recepit, null);

                order_no = myView.findViewById(R.id.order_no);
                ordered_from = myView.findViewById(R.id.ordered_from);
                timing = myView.findViewById(R.id.timing);
                sub_total = myView.findViewById(R.id.sub_total);
                grand_total = myView.findViewById(R.id.grand_total);
                delivery_charges = myView.findViewById(R.id.delivery_charges);
                item_text_tv = myView.findViewById(R.id.item_name);
                qty_text_tv = myView.findViewById(R.id.item_quantity);
                price_text_tv = myView.findViewById(R.id.item_price);
                tax = myView.findViewById(R.id.tax);
                receipt_items_recycler = myView.findViewById(R.id.receipt_items_recycler);
                delivery_charges_layout = myView.findViewById(R.id.delivery_charges_layout);


                orderInDetail(currentOrder.getOrder_id());

                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
               /* alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Receipt");*/
                alert.setView(myView);
                alert.show();
            }
        });

        holder.track_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, ""+currentOrder.getOrder_id(), Toast.LENGTH_SHORT).show();
                fetItemTrackDetail(currentOrder.getOrder_id());
            }
        });

        holder.text_track_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderStatusActivity.class);
                intent.putExtra("order_id", currentOrder.getOrder_id());
                intent.putExtra("order_track_id", currentOrder.getOrder_track());
                mContext.startActivity(intent);
            }
        });

        holder.cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setTitle("Cancel Order");
                alertDialogBuilder
                        .setMessage(R.string.order_cancel)
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cancelOrder(currentOrder.getOrder_id());
                                currentOrdersArrayList.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        if (type.equalsIgnoreCase("pending")) {
            holder.text_recepit.setVisibility(View.GONE);
            holder.cancel_order.setVisibility(View.GONE);
        }
    }

    private void cancelOrder(String order_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CANCEL_ORDER + order_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            UImsgs.showCustomToast(mContext, "Order Cancelled", INFO);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    private void fetItemTrackDetail(String order_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SINGLE_ORDER_DETAILS + order_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                DialogTrackPresentOrder dialogTrackPresentOrder = new DialogTrackPresentOrder();
                dialogTrackPresentOrder.trackOrderDialog(response, mContext);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void orderInDetail(String orderId) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, OrderInDetail + orderId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("order_detail_resp", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    order_no.setText(dataObject.getString("order_track"));
                    grand_total.setText(dataObject.getString("total"));
                    if (Integer.parseInt(dataObject.getString("delivery_fee")) > 0) {
                        delivery_charges_layout.setVisibility(View.VISIBLE);
                        delivery_charges.setText(dataObject.getString("delivery_fee"));


                    }

                    timing.setText(dataObject.getString("created_at"));
                    taxValue = dataObject.getInt("tax");
                    try {
                        ordered_from.setText(dataObject.getJSONObject("vendor").getString("name"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String item_text = "", qty_text = "", price_text = "";
                    int price = 0;
                    itemPojos = new ArrayList<>();
                    try {
                        JSONArray order_items_array = dataObject.getJSONArray("order_items");

                        for (int i = 0; i < order_items_array.length(); i++) {

                            ReceiptItemPojo receiptItemPojo = new ReceiptItemPojo();

                            JSONObject orderObject = order_items_array.getJSONObject(i);
                            item_text = item_text + (i + 1) + "." + orderObject.getString("item_name").substring(0, 9) + "..." + "\n\n";
                            qty_text = qty_text + orderObject.getString("quantity") + "\n\n";
                            price = price + (Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price"))));
                            price_text = price_text + (Integer.parseInt(orderObject.getString("price"))) + "\n\n";

                            receiptItemPojo.setSno(i + 1);
                            receiptItemPojo.setItem_name(orderObject.getString("item_name"));
                            receiptItemPojo.setQty(orderObject.getInt("quantity"));
                            receiptItemPojo.setValue((Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price")))));

                            itemPojos.add(receiptItemPojo);


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray sub_order_items_array = dataObject.getJSONArray("sub_order_items");
                        for (int i = 0; i < sub_order_items_array.length(); i++) {
                            ReceiptItemPojo receiptItemPojo = new ReceiptItemPojo();
                            JSONObject orderObject = sub_order_items_array.getJSONObject(i);
                            item_text = item_text + i + 1 + "." + orderObject.getString("sec_item_name").substring(0, 9) + "..." + "\n\n";
                            qty_text = qty_text + orderObject.getString("quantity") + "\n\n";
                            price = price + (Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price"))));
                            price_text = price_text + (Integer.parseInt(orderObject.getString("price"))) + "\n\n";

                            receiptItemPojo.setSno(itemPojos.size() + 1);
                            receiptItemPojo.setItem_name(orderObject.getString("item_name"));
                            receiptItemPojo.setQty(orderObject.getInt("quantity"));
                            receiptItemPojo.setValue((Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price")))));

                            itemPojos.add(receiptItemPojo);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        ReceiptItemsAdapter receiptItemsAdapter = new ReceiptItemsAdapter(mContext, itemPojos);

                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        receipt_items_recycler.setLayoutManager(layoutManager);
                        receipt_items_recycler.setAdapter(receiptItemsAdapter);

                    } catch (Exception e) {

                    }


                    sub_total.setText(price + "".trim());
                    item_text_tv.setText(item_text);
                    tax.setText(Math.ceil(price * (0.01 * taxValue)) + "(" + taxValue + "%)");
                    qty_text_tv.setText(qty_text);
                    price_text_tv.setText(price_text);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);


    }

    @Override
    public int getItemCount() {
        return currentOrdersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_track, text_discount, text_total, order_status, text_date,
                returent_name, text_recepit, track_order, otp, view_details, text_track_now, cancel_order, tvVendorName, tvVendorEmail, tvVendorMobileNumber;
        RecyclerView recycler_inner_items, recycler_inner_sub_items;
        LinearLayout otp_layout;
        CardView details_card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvVendorName = itemView.findViewById(R.id.tvVendorName);
            tvVendorEmail = itemView.findViewById(R.id.tvVendorEmail);
            tvVendorMobileNumber = itemView.findViewById(R.id.tvVendorMobileNumber);
            //Text Views
            text_recepit = itemView.findViewById(R.id.text_recepit);
            track_order = itemView.findViewById(R.id.track_order);
            returent_name = itemView.findViewById(R.id.returent_name);
            text_track = itemView.findViewById(R.id.text_track);
            text_discount = itemView.findViewById(R.id.text_discount);

            text_total = itemView.findViewById(R.id.text_total);
            order_status = itemView.findViewById(R.id.order_status);
            text_date = itemView.findViewById(R.id.text_date);
            otp = itemView.findViewById(R.id.otp);
            otp_layout = itemView.findViewById(R.id.otp_layout);
            details_card = itemView.findViewById(R.id.details_card);
            details_card.setVisibility(View.GONE);
            text_track_now = itemView.findViewById(R.id.text_track_now);
            cancel_order = itemView.findViewById(R.id.cancel_order);
            view_details = itemView.findViewById(R.id.view_details);
            //Recycler Views
            recycler_inner_items = itemView.findViewById(R.id.recycler_inner_items);
            recycler_inner_sub_items = itemView.findViewById(R.id.recycler_inner_sub_items);

        }
    }


}
