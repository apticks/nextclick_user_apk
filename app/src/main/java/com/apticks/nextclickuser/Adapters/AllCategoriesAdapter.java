package com.apticks.nextclickuser.Adapters;



/*
 * @author : sarath
 *
 * @desc : Categories helping adapter....
 *
 * */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.CategoriesData;

import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllCategoriesAdapter extends RecyclerView.Adapter<AllCategoriesAdapter.ViewHolder> {

    List<CategoriesData> data;

    LayoutInflater inflter;
    Context context;

    public AllCategoriesAdapter(Context activity, List<CategoriesData> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public AllCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.categoryitem, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new AllCategoriesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCategoriesAdapter.ViewHolder holder, int position) {
        CategoriesData categoriesData = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(categoriesData.getName());
        Picasso.get().load(categoriesData.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(categoriesData.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.allcategoryimage);
            displayName = itemView.findViewById(R.id.allcategoryname);



       /* cardViewRecentAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecentlyAddedHostelModelData recentlyAddedHostelModelData = dataArrayList.get(getAdapterPosition());
                String tempId = recentlyAddedHostelModelData.getId();

                Intent intent = new Intent(context, SingleViewHostelActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("hostelId",tempId);

                context.startActivity(intent);
            }
        });*/


        }
    }
}

