package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.EcomProductsActivity;
import com.apticks.nextclickuser.Pojo.EcomCategories;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EcomSubCategoriesAdapter extends RecyclerView.Adapter<EcomSubCategoriesAdapter.ViewHolder> {

    List<EcomCategories> data;

    LayoutInflater inflter;
    Context context;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public EcomSubCategoriesAdapter(Context activity, List<EcomCategories> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public EcomSubCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fashionitem, parent, false);

        return new EcomSubCategoriesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EcomSubCategoriesAdapter.ViewHolder holder, int position) {
        EcomCategories ecomCategories = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.itemName.setText(ecomCategories.getName());
        Picasso.get().load(ecomCategories.getImage()).into(holder.itemImage);
        /*holder.displayLocation.setText(displayCategory.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName, displayLocation;
        CircularImageView itemImage;
        LinearLayout listItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.fashionItemImage);
            itemName = itemView.findViewById(R.id.fashionItemName);
            listItem = itemView.findViewById(R.id.ecom_category_layout);
            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EcomCategories subCategoryItemPojo = data.get(getAdapterPosition());

                    Intent intent = new Intent(context, EcomProductsActivity.class);
                    intent.putExtra("sub_cat_id",subCategoryItemPojo.getId());
                    intent.putExtra("ecom_categories",subCategoryItemPojo.getCatId());
                    context.startActivity(intent);

                }
            });
        }
    }
}