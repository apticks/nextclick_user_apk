package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.EcomProductsActivity;
import com.apticks.nextclickuser.Fragments.EcommerceProductsFragment;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EcomSubCategoryItemsAdapter extends RecyclerView.Adapter<EcomSubCategoryItemsAdapter.ViewHolder> {

    List<SubCategoryItemPojo> data;

    LayoutInflater inflter;
    Context context;

    public static String ecomCategories;

    PreferenceManager preferenceManager;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public EcomSubCategoryItemsAdapter(Context activity, List<SubCategoryItemPojo> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public EcomSubCategoryItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.listviewfashionitem, parent, false);

        return new EcomSubCategoryItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EcomSubCategoryItemsAdapter.ViewHolder holder, int position) {
        SubCategoryItemPojo subCategoryItemPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.itemName.setText(subCategoryItemPojo.getName());
        Picasso.get().load(subCategoryItemPojo.getImage()).into(holder.itemImage);
        /*holder.displayLocation.setText(displayCategory.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName, displayLocation;
        //CircularImageView itemImage;
        ImageView itemImage;
        LinearLayout listItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.fashionItemImage);
            itemName = itemView.findViewById(R.id.fashionItemName);
            listItem = itemView.findViewById(R.id.ecom_subcategoryitem_layout);

            preferenceManager = new PreferenceManager(context);

            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubCategoryItemPojo subCategoryItemPojo = data.get(getAdapterPosition());

                    EcommerceProductsFragment.tabPosition = getAdapterPosition();

                    Intent intent = new Intent(context, EcomProductsActivity.class);
                    intent.putExtra("sub_cat_id",subCategoryItemPojo.getId());
                    intent.putExtra("ecom_categories",ecomCategories);
                    context.startActivity(intent);

                }
            });


        }
    }
}