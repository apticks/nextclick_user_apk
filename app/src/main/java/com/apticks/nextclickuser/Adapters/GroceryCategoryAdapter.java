package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.GroceryActivities.GroceryProductsActivity;
import com.apticks.nextclickuser.Pojo.FoodItemPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class GroceryCategoryAdapter extends RecyclerView.Adapter<GroceryCategoryAdapter.ViewHolder> {
    Context mContext;
    ArrayList<FoodItemPojo> foodItemData;
    public GroceryCategoryAdapter(Context mContext, ArrayList<FoodItemPojo> foodItemData) {
        this.mContext = mContext;
        this.foodItemData = foodItemData;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grocery_categries_adapter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new GroceryCategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FoodItemPojo foodItemPojo = foodItemData.get(position);
        //holder.name.setText(foodItemPojo.getName());
        //holder.desc.setText(foodItemPojo.getDesc());

        Picasso.get().load(foodItemPojo.getImage()).into(holder.grocery_cat_logo);

    }

    @Override
    public int getItemCount() {
        return foodItemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, desc;
        ImageView grocery_cat_logo;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.grocery_cat_name);
            desc = itemView.findViewById(R.id.grocerry_cat_desc);
            grocery_cat_logo = itemView.findViewById(R.id.grocerry_cat_Image);

            grocery_cat_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition()>1){
                        FoodItemPojo foodItemPojo = foodItemData.get(getAdapterPosition());
                        String tempId = foodItemPojo.getId();
                        //GroceryProductsFragment.groceryTabPosition = getAdapterPosition()+2;//For Tab Position
                        /*AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        GroceryProductsFragment groceryProductsFragment = new GroceryProductsFragment(tempId);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, groceryProductsFragment).addToBackStack(null).commit();*/

                        Intent intent = new Intent(mContext, GroceryProductsActivity.class);
                        intent.putExtra("grocery_id",tempId);
                        mContext.startActivity(intent);
                    }else{
                        FoodItemPojo foodItemPojo = foodItemData.get(getAdapterPosition());
                        String tempId = foodItemPojo.getId();
                        //GroceryProductsFragment.groceryTabPosition = getAdapterPosition();//For Tab Position
                        /*AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                        GroceryProductsFragment groceryProductsFragment = new GroceryProductsFragment(tempId);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, groceryProductsFragment).addToBackStack(null).commit();*/
                        //ft.replace(R.id.nav_host_fragment, groceryProductsFragment).addToBackStack(null).commit();

                        Intent intent = new Intent(mContext, GroceryProductsActivity.class);
                        intent.putExtra("grocery_id",tempId);
                        mContext.startActivity(intent);
                    }
                }
            });

        }
    }
}
