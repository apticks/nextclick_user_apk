package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.Pojo.DaysPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HolidaysAdapter  extends RecyclerView.Adapter<HolidaysAdapter.ViewHolder> {

    List<DaysPojo> data;

    LayoutInflater inflter;
    Context context;

    public HolidaysAdapter(Context activity, List<DaysPojo> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public HolidaysAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_holiday_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new HolidaysAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HolidaysAdapter.ViewHolder holder, int position) {
        DaysPojo daysPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.day_shrt_name.setText(daysPojo.getShort_name());
        if(daysPojo.isAvailable()){
            holder.chek.setVisibility(View.VISIBLE);
        }else{
            holder.unchek.setVisibility(View.VISIBLE);
        }
        /*holder.displayLocation.setText(categoriesData.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView day_shrt_name;
        ImageView chek,unchek;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            day_shrt_name = itemView.findViewById(R.id.day_shrt_name);
            chek = itemView.findViewById(R.id.chek);
            unchek = itemView.findViewById(R.id.unchek);






        }
    }
}

