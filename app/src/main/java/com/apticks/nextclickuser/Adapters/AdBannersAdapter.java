package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdBannersAdapter extends RecyclerView.Adapter<AdBannersAdapter.ViewHolder> {

    List<String> data;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public AdBannersAdapter(Context activity, List<String> brandsPojos) {
        this.context = activity;
        this.data = brandsPojos;
    }


    @NonNull
    @Override
    public AdBannersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.ad_banners_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new AdBannersAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdBannersAdapter.ViewHolder holder, int position) {
        String imageUrl = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        try {
            Picasso.get().load(imageUrl).placeholder(R.drawable.loader_gif).into(holder.banner_image);
        }catch (Exception e){
            e.printStackTrace();
        }
        /*holder.displayLocation.setText(hospital.getLocation());*/





    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView banner_image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_image = itemView.findViewById(R.id.banner_image);




        }
    }
}