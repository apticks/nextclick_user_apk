package com.apticks.nextclickuser.Adapters.MultiCatAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.MultiCatActivities.MultiCat_Vendor_Overview;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.MultiCatVendorsPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MultiCatVendorsAdapter extends RecyclerView.Adapter<MultiCatVendorsAdapter.ViewHolder> {

    List<MultiCatVendorsPojo> data;

    LayoutInflater inflter;
    Context context;

    public MultiCatVendorsAdapter(Context activity, List<MultiCatVendorsPojo> docList) {
        this.context = activity;
        this.data = docList;
    }


    @NonNull
    @Override
    public MultiCatVendorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.multicat_vendors_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new MultiCatVendorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiCatVendorsAdapter.ViewHolder holder, int position) {

        MultiCatVendorsPojo currentVendor = data.get(position);
        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        // holder.amenityName.setText(data.get(position));

        holder.multicat_vendor_name.setText(currentVendor.getName().toUpperCase());
        holder.multicat_vendor_address.setText(currentVendor.getAddress());
        holder.multicat_vendor_email.setText(currentVendor.getEmail());
        //holder.multicat_vendor_contact.setText(currentVendor.get());
        Picasso.get().load(currentVendor.getImage()).into(holder.multicat_vendor_image);
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MultiCat_Vendor_Overview.class);
                intent.putExtra("vendor_id",currentVendor.getId());
                context.startActivity(intent);
            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MultiCat_Vendor_Overview.class);
                intent.putExtra("vendor_id",currentVendor.getId());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView multicat_vendor_name, multicat_vendor_address,multicat_vendor_email,multicat_vendor_contact,details;
        ImageView multicat_vendor_image;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            multicat_vendor_name = itemView.findViewById(R.id.multicat_vendor_name);
            multicat_vendor_address = itemView.findViewById(R.id.multicat_vendor_address);
            multicat_vendor_email = itemView.findViewById(R.id.multicat_vendor_mail);
            multicat_vendor_contact = itemView.findViewById(R.id.multicat_vendor_contact);
            details = itemView.findViewById(R.id.details);

            multicat_vendor_image = itemView.findViewById(R.id.multicat_vendor_image);
            linearLayout = itemView.findViewById(R.id.layout);


        }
    }
}

