package com.apticks.nextclickuser.Adapters.MultiCatAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.AmenitiesPojo;
import com.apticks.nextclickuser.R;

import java.util.List;

public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.ViewHolder> {

    List<AmenitiesPojo> data;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public AmenitiesAdapter(Context activity, List<AmenitiesPojo> amenitiesPojo) {
        this.context = activity;
        this.data = amenitiesPojo;
    }


    @NonNull
    @Override
    public AmenitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.overview_amenity_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new AmenitiesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AmenitiesAdapter.ViewHolder holder, int position) {
        AmenitiesPojo amenitiesPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();


        //Picasso.get().load(currentBsVendor.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/

        holder.amenityName.setText(amenitiesPojo.getName());



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView amenityName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            amenityName = itemView.findViewById(R.id.overview_amenity_supporter_name);



        }
    }
}