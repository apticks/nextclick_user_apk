package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.AllBrandsActivity;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Pojo.FeaturedBrandsPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FeaturedBrandsAdapter extends RecyclerView.Adapter<FeaturedBrandsAdapter.ViewHolder> {

    List<FeaturedBrandsPojo> data;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public FeaturedBrandsAdapter(Context activity, List<FeaturedBrandsPojo> brandsPojos) {
        this.context = activity;
        this.data = brandsPojos;
    }


    @NonNull
    @Override
    public FeaturedBrandsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.brand_image_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new FeaturedBrandsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeaturedBrandsAdapter.ViewHolder holder, int position) {
        FeaturedBrandsPojo brandsPojos = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();



        if(brandsPojos.getId().equalsIgnoreCase("0")){
            holder.brand_name.setText(brandsPojos.getName());
            holder.brand_name.setVisibility(View.VISIBLE);
            holder.brand_name.setGravity(View.TEXT_ALIGNMENT_CENTER);
            holder.brandImage.setVisibility(View.GONE);
            holder.brand_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AllBrandsActivity.class);
                    context.startActivity(intent);
                }
            });
            try {
                Picasso.get().load(R.drawable.ic_view_all).placeholder(R.drawable.ic_view_all).into(holder.brandImage);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {
                Picasso.get().load(brandsPojos.getImage()).placeholder(R.drawable.loader_gif).into(holder.brandImage);
            }catch (Exception e){
                e.printStackTrace();
            }
            holder.brand_name.setText(brandsPojos.getName());
            /*holder.displayLocation.setText(hospital.getLocation());*/

            holder.brand_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, VendorsActivity.class);
                    intent.putExtra("brand_id",brandsPojos.getId());//need to pass sub category id
                    Log.d("brand_id",brandsPojos.getId());
                    context.startActivity(intent);
                }
            });
        }



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandImage;
        LinearLayout brand_layout;
        TextView brand_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            brandImage = itemView.findViewById(R.id.brand_image);
            brand_layout = itemView.findViewById(R.id.brand_layout);
            brand_name = itemView.findViewById(R.id.brand_name);



        }
    }
}