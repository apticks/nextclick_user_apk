package com.apticks.nextclickuser.Adapters.UserHistory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.OrderHistory.CurrentOrder;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class PresentOrderInnerItemAdapter extends RecyclerView.Adapter<PresentOrderInnerItemAdapter.ViewHolder> {
    ArrayList<CurrentOrder> currentOrdersArrayListInnerItems ;

    public PresentOrderInnerItemAdapter(ArrayList<CurrentOrder> currentOrdersArrayListInnerItems){
        this.currentOrdersArrayListInnerItems= currentOrdersArrayListInnerItems;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.present_order_inner_items_adapter, parent,false);
        return new PresentOrderInnerItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       // CurrentOrder currentOrder = currentOrdersArrayListInnerItems.get(position);
        holder.text_inner_price.setText(String.valueOf(currentOrdersArrayListInnerItems.get(position).getPriceOrderItems()));
        holder.text_quantity.setText(String.valueOf(currentOrdersArrayListInnerItems.get(position).getQuantityOrderItems()));
    }

    @Override
    public int getItemCount() {
        return currentOrdersArrayListInnerItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_inner_price,text_quantity;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_inner_price =itemView.findViewById(R.id.text_inner_price);
            text_quantity =itemView.findViewById(R.id.text_quantity);

        }
    }
}
