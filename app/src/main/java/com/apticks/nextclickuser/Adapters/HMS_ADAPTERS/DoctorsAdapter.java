package com.apticks.nextclickuser.Adapters.HMS_ADAPTERS;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.HMS_POJO.DoctorsPojo;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.ViewHolder> {

    List<DoctorsPojo> data;

    LayoutInflater inflter;
    Context context;

    public DoctorsAdapter(Context activity, List<DoctorsPojo> docList) {
        this.context = activity;
        this.data = docList;
    }


    @NonNull
    @Override
    public DoctorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.doctors_supportter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new DoctorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorsAdapter.ViewHolder holder, int position) {

        DoctorsPojo currentDoctor = data.get(position);
        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        // holder.amenityName.setText(data.get(position));

        holder.doctorName.setText(currentDoctor.getName().toUpperCase());
        holder.doctorSpecialization.setText(currentDoctor.getSpecialization());
        holder.doctor_experience.setText(currentDoctor.getExperience()+"+ year(s)");

        Picasso.get().load(currentDoctor.getImage()).into(holder.doctorImage);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView doctorName, doctorSpecialization, doctor_experience, bookAppointment;
        CircularImageView doctorImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            doctorName = itemView.findViewById(R.id.doctor_name);
            doctorSpecialization = itemView.findViewById(R.id.doctor_specialization);
            doctor_experience = itemView.findViewById(R.id.doctor_experience);
            bookAppointment = itemView.findViewById(R.id.book_doctor_appointment);
            doctorImage = itemView.findViewById(R.id.doctor_image);


        }
    }
}

