package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SlidingImage_AdapterTemp extends PagerAdapter {


    //private String[] urls;
    private List<SlidersModelClass> urls = new ArrayList<>();

    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_AdapterTemp(Context context, List<SlidersModelClass> singleHostelViewData) {
        this.context = context;
        this.urls = singleHostelViewData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);

        SlidersModelClass singleHostelViewData = urls.get(position);

        String imageUrls = singleHostelViewData.getBanners();


        /*Glide.with(context)
                .load(imageUrls)
                .into(imageView);*/
        Picasso.get().load(imageUrls).placeholder(R.drawable.loader_gif).into(imageView);

        view.addView(imageLayout, 0);
        //notifyDataSetChanged();

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}