package com.apticks.nextclickuser.Adapters.BS_Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.Beauty_Spa.BS_Services_Activity;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BS_DashBoardAdapter extends RecyclerView.Adapter<BS_DashBoardAdapter.ViewHolder> {

    List<DisplayCategory> data;

    LayoutInflater inflter;
    Context context;

    public BS_DashBoardAdapter(Context activity, List<DisplayCategory> listRecentHostel) {
        this.context = activity;
        this.data = listRecentHostel;
    }


    @NonNull
    @Override
    public BS_DashBoardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.individualcategorycompletelist, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new BS_DashBoardAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BS_DashBoardAdapter.ViewHolder holder, int position) {
        DisplayCategory currentBS = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(currentBS.getName());
        Picasso.get().load(currentBS.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        LinearLayout item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.displayimage);
            displayName = itemView.findViewById(R.id.displayname);
            displayLocation = itemView.findViewById(R.id.displaylocation);
            item = itemView.findViewById(R.id.item);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DisplayCategory recentlyAddedHostelModelData = data.get(getAdapterPosition());
                    String tempId = recentlyAddedHostelModelData.getId();

                    Intent intent = new Intent(context, BS_Services_Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("vendor_id",tempId);

                    context.startActivity(intent);
                }
            });


        }
    }
}

