package com.apticks.nextclickuser.Adapters.UserHistory;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.DialogTrackOrderAdapter;
import com.apticks.nextclickuser.Pojo.OrderPojo.OrderTrackPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DialogTrackPresentOrder {
    List<OrderTrackPojo> orderTrackPojoList = new ArrayList<>();
    RecyclerView recycler_order_track;


    void trackOrderDialog(String response, Context mContext){
        Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.track_order_adapter_dialog);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        //dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        recycler_order_track =dialog.findViewById(R.id.recycler_order_track);

        dialog.setCancelable(true);

        try{
            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObjectData = jsonObject.getJSONObject("data");
            JSONArray jsonArrayOrderItems = jsonObjectData.getJSONArray("ord_state");

            for (int i =0; i<jsonArrayOrderItems.length();i++){
                JSONObject jsonObjectValue = jsonArrayOrderItems.getJSONObject(i);
                OrderTrackPojo orderTrackPojo = new OrderTrackPojo();
                orderTrackPojo.setName(jsonObjectValue.getString("name"));
                orderTrackPojo.setStatus(jsonObjectValue.getInt("status"));
                orderTrackPojoList.add(orderTrackPojo);
            }

            if (orderTrackPojoList.size()!=0){
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                    @Override
                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                            private static final float SPEED = 300f;// Change this value (default=25f)

                            @Override
                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                return SPEED / displayMetrics.densityDpi;
                            }
                        };
                        smoothScroller.setTargetPosition(position);
                        startSmoothScroll(smoothScroller);
                    }
                };
                recycler_order_track.setLayoutManager(linearLayoutManager);

                DialogTrackOrderAdapter dialogTrackOrderAdapter = new DialogTrackOrderAdapter(orderTrackPojoList, jsonObjectData.getInt("order_status"));
                recycler_order_track.setAdapter(dialogTrackOrderAdapter);
            }else {
                //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            //Toast.makeText(mContext, ""+ex, Toast.LENGTH_SHORT).show();
        } catch (Exception e){

        }


        dialog.show();
    }
}
