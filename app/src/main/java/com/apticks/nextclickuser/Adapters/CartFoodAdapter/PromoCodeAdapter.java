package com.apticks.nextclickuser.Adapters.CartFoodAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.CartPojoModel.DialogPromoModel;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.ViewHolder> {
    public ArrayList<DialogPromoModel> arrayListDialogPromoModel = new ArrayList<>();
    EditText promo_codeEditText;

    public PromoCodeAdapter(ArrayList<DialogPromoModel> arrayListDialogPromoModel, EditText promo_codeEditText){
        this.arrayListDialogPromoModel = arrayListDialogPromoModel;
        this.promo_codeEditText = promo_codeEditText;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_promo_code_adapter,parent,false);

        return new PromoCodeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DialogPromoModel dialogPromoModel = arrayListDialogPromoModel.get(position);
        holder.promo_title.setText(dialogPromoModel.getPromo_title());
        holder.promo_code.setText(dialogPromoModel.getPromo_code());
        holder.promo_label.setText(dialogPromoModel.getPromo_label());
    }

    @Override
    public int getItemCount() {
        return arrayListDialogPromoModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView promo_title,promo_use,promo_code,promo_label;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);



            promo_title = itemView.findViewById(R.id.promo_title);
            promo_use = itemView.findViewById(R.id.promo_use);
            promo_code = itemView.findViewById(R.id.promo_code);
            promo_label = itemView.findViewById(R.id.promo_label);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogPromoModel dialogPromoModel = arrayListDialogPromoModel.get(getAdapterPosition());
                    promo_codeEditText.setText(dialogPromoModel.getPromo_code());
                }
            });
        }
    }
}
