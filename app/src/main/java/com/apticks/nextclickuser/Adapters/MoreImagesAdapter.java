package com.apticks.nextclickuser.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.PROFILE.ProfileActivity;
import com.apticks.nextclickuser.Pojo.MorePojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoreImagesAdapter extends RecyclerView.Adapter<MoreImagesAdapter.ViewHolder> {

    List<MorePojo> data;
    MediaController mc;

    LayoutInflater inflter;
    Context context;
    View itemView;
    ProfileActivity profileActivity;

    public MoreImagesAdapter(Context activity, List<MorePojo> completData, ProfileActivity profileActivity) {
        this.context = activity;
        this.data = completData;
        this.profileActivity = profileActivity;
    }


    @NonNull
    @Override
    public MoreImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.more_image_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new MoreImagesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreImagesAdapter.ViewHolder holder, int position) {


            MorePojo morePojo = data.get(position);

            Picasso.get().load(morePojo.getImage()).into(holder.more_item_image);
            //holder.displayLocation.setText(categoriesData.getLocation());

            /*try{
                if(position==4){
                    holder.more_count_left.setText("+"+(data.size()-4));
                    holder.more_item_image.setAlpha(0.2f);
                }else {
                    holder.more_count_left.setVisibility(View.GONE);
                    //holder.more_count_left.setTextColor(Color.parseColor("#000000"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }*/



        holder.more_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater inflater = profileActivity.getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.image_showing_supporter, null);
                final ImageView image = alertLayout.findViewById(R.id.image);
                Picasso.get().load(morePojo.getImage()).into(image);

                AlertDialog.Builder alert = new AlertDialog.Builder(profileActivity);
                alert.setView(alertLayout);
                alert.setCancelable(true);
                AlertDialog dialog = alert.create();
                dialog.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView more_count_left;
        ImageView more_item_image;
        LinearLayout more_item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            more_count_left = itemView.findViewById(R.id.more_count_left);
            more_item_image = itemView.findViewById(R.id.more_item_image);
            more_item = itemView.findViewById(R.id.more_item);





        }
    }
}