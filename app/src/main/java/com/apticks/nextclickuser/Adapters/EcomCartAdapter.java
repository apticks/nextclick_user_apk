package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EcomCartAdapter  extends RecyclerView.Adapter<EcomCartAdapter.ViewHolder> {



    int count=0;
    CartDBHelper cartDBHelper;

    List<CartPojo> data;
    LayoutInflater inflter;
    Context context;


    public EcomCartAdapter(Context activity, List<CartPojo> cartItems) {
        this.context = activity;
        this.data = cartItems;

    }


    @NonNull
    @Override
    public EcomCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.ecommcartsupporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new EcomCartAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EcomCartAdapter.ViewHolder holder, int position) {
        //cartPojo = dataArrayList.get(position);
        CartPojo cartPojo= new CartPojo();

        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(cartPojo.getName());
        holder.displayprice.setText(cartPojo.getPrice());
        Picasso.get().load(cartPojo.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(restaurants.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayprice, add;
        ImageView displayImage;
        LinearLayout item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.ecomcartItemImage);
            displayName = itemView.findViewById(R.id.ecomcartItemname);
            displayprice = itemView.findViewById(R.id.ecomcartItemprice);
            //discountfooditem = itemView.findViewById(R.id.addfooditem);
            //item = itemView.findViewById(R.id.item);





        }
    }


}