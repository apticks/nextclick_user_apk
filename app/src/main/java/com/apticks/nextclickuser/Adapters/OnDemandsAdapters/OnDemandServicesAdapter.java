package com.apticks.nextclickuser.Adapters.OnDemandsAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityDoctorsActivity;
import com.apticks.nextclickuser.Activities.OnDemandsActivities.OnDemandServicesPersonsActivity;
import com.apticks.nextclickuser.Adapters.HospitalityAdapters.SpecialitiesAdapter;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.SpecialityPojo;
import com.apticks.nextclickuser.Pojo.OnDemandsPojos.OnDemandPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;

public class OnDemandServicesAdapter extends RecyclerView.Adapter<OnDemandServicesAdapter.ViewHolder> {

    private Context context;
    private String serviceId = "";
    private String vendorId = "";

    private List<OnDemandPojo> data;

    public OnDemandServicesAdapter(Context activity, List<OnDemandPojo> itemPojos, String vendorId, String serviceId) {
        this.data = itemPojos;
        this.context = activity;
        this.vendorId = vendorId;
        this.serviceId = serviceId;
    }


    @NonNull
    @Override
    public OnDemandServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_ondemand_service_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new OnDemandServicesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OnDemandServicesAdapter.ViewHolder holder, int position) {
        OnDemandPojo onDemandPojo = data.get(position);

        holder.ondemand_service.setText(onDemandPojo.getName());
        Picasso.get()
                .load(onDemandPojo.getImage())
                /*.networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                .placeholder(R.drawable.loader_gif)
                .into(holder.ondemand_service_image);


        holder.ondemand_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OnDemandServicesPersonsActivity.class);
                intent.putExtra(VENDOR_ID, vendorId);
                intent.putExtra("ods_name", onDemandPojo.getName());
                intent.putExtra("ods_id", onDemandPojo.getId());
                intent.putExtra(context.getString(R.string.service_id), serviceId);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView ondemand_service;
        ImageView ondemand_service_image;
        LinearLayout ondemand_item;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ondemand_service = itemView.findViewById(R.id.ondemand_service);
            ondemand_service_image = itemView.findViewById(R.id.ondemand_service_image);
            ondemand_item = itemView.findViewById(R.id.ondemand_item);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }
}
