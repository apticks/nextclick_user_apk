package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.Beauty_Spa.Vendors_BS;
import com.apticks.nextclickuser.Activities.MultiCatActivities.MultiCatActivity;
import com.apticks.nextclickuser.Fragments.VendorsListFragment;
import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DashBoardCategoriesAdapter extends RecyclerView.Adapter<DashBoardCategoriesAdapter.ViewHolder> {

    List<CategoriesData> data;

    LayoutInflater inflter;
    Context context;
    View itemView;

    public DashBoardCategoriesAdapter(Context activity, List<CategoriesData> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public DashBoardCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.dashboardcategorysupporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new DashBoardCategoriesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DashBoardCategoriesAdapter.ViewHolder holder, int position) {
        CategoriesData categoriesData = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(categoriesData.getName());
        Picasso.get().load(categoriesData.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(categoriesData.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        LinearLayout category_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.dashboard_category_image);
            displayName = itemView.findViewById(R.id.dashboard_category_name);
            category_layout = itemView.findViewById(R.id.category_layout);



       /* cardViewRecentAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecentlyAddedHostelModelData recentlyAddedHostelModelData = dataArrayList.get(getAdapterPosition());
                String tempId = recentlyAddedHostelModelData.getId();

                Intent intent = new Intent(context, SingleViewHostelActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("hostelId",tempId);

                context.startActivity(intent);
            }
        });*/

            category_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CategoriesData categoriesData = data.get(getAdapterPosition());

                    if(categoriesData.getId().equalsIgnoreCase("1")){
                        AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        VendorsListFragment vendorsListFragment = new VendorsListFragment(context,categoriesData.getId());
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, vendorsListFragment).addToBackStack(null).commit();
                    }if(categoriesData.getId().equalsIgnoreCase("4")){
                        AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        VendorsListFragment vendorsListFragment = new VendorsListFragment(context,categoriesData.getId());
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, vendorsListFragment).addToBackStack(null).commit();
                    }if(categoriesData.getId().equalsIgnoreCase("7")){
                        Intent intent = new Intent(context, Vendors_BS.class);
                        context.startActivity(intent);

                    }if(categoriesData.getId().equalsIgnoreCase("6") || categoriesData.getId().equalsIgnoreCase("8") || categoriesData.getId().equalsIgnoreCase("9") || categoriesData.getId().equalsIgnoreCase("61") ){
                        Intent intent = new Intent(context, MultiCatActivity.class);
                        intent.putExtra("cat_id",categoriesData.getId());
                        context.startActivity(intent);

                    }/*if(categoriesData.getId().equalsIgnoreCase("1")){
                        AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
                        VendorsListFragment vendorsListFragment = new VendorsListFragment(context,categoriesData.getId());
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, vendorsListFragment).addToBackStack(null).commit();
                    }*/
                }
            });

        }
    }
}

