package com.apticks.nextclickuser.Adapters.VENDORSADAPTERS;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.PROFILE.ProfileActivity;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.VENDORSPOJO.VendorsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.mapbox.mapboxsdk.Mapbox;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Config.Config.MAP_BOX_ACCESS_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;

public class VendorsAdapter extends RecyclerView.Adapter<VendorsAdapter.ViewHolder>   {

    List<VendorsPojo> data;

    LayoutInflater inflter;
    Context context;
    private CartDBHelper cartDBHelper;

    private int vendor_activity = 0;//0-not working , 1-working
    private String comingsoon_image_url;
    private String tempNameVendor, idStringVendor /*For Passing Vendor Data To next Activity */;
    int SHOP_NOW=1,BOOK_NOW=2;
    int SHOP_BOOK ;

    public VendorsAdapter(Context activity, List<VendorsPojo> itemPojos, int SHOP_BOOK) {
        this.context = activity;
        this.data = itemPojos;
        this.SHOP_BOOK = SHOP_BOOK;
        Mapbox.getInstance(context, MAP_BOX_ACCESS_TOKEN);
        Mapbox.setAccessToken(MAP_BOX_ACCESS_TOKEN);
        cartDBHelper = new CartDBHelper(context);
    }


    @NonNull
    @Override
    public VendorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendor_card_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VendorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsAdapter.ViewHolder holder, int position) {
        VendorsPojo vendorsPojo = data.get(position);

        holder.vendor_name.setText(vendorsPojo.getName());
        holder.customer_address.setText(vendorsPojo.getAddress());
        holder.vendor_landmark.setText(vendorsPojo.getLandmark());
        holder.vendor_email.setText(vendorsPojo.getEmail());
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.vendor_distance.setText(df.format(vendorsPojo.getDistance())+" KM away");
        Picasso.get()
                .load(vendorsPojo.getImage())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.vendor_image);

        /*holder.vendor_location_map;*/

       /* holder.vendor_location_map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {


                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments
                    }
                });
            }
        });*/

       if(SHOP_BOOK==BOOK_NOW){
           holder.vendor_view_shop.setText("Book Now");
           holder.vendor_view_shop_card.setVisibility(View.GONE);
       }

        holder.vendor_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(vendorsPojo.getAvailability()==1) {
                    if (cartDBHelper.numberOfFoodCartRows() > 0) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Cart Alert");
                        alertDialogBuilder
                                .setMessage("You have some cart items. Would you like to check or clear")
                                .setCancelable(false)
                                .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        cartDBHelper.deleteCartTable();
                                        UImsgs.showToast(context, "Cart cleared successfully");
                                    }
                                })
                                .setNegativeButton("Check", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(context, CartActivity.class);
                                        intent.putExtra("Type", "Food");
                                        context.startActivity(intent);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        Intent intent = new Intent(context, ProfileActivity.class);
                        if(SHOP_BOOK==BOOK_NOW) {
                            intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")
                            intent.putExtra("SHOP_BOOK", "BOOK_NOW");
                        }else{
                            intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")
                            intent.putExtra("SHOP_BOOK", "SHOP_NOW");
                        }
                        /*intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")*/
                        AddToCartActivity.vendorId = (vendorsPojo.getVendor_user_id());
                        //Toast.makeText(context, ""+vendorsPojo.getVendor_user_id(), Toast.LENGTH_SHORT).show();
                        context.startActivity(intent);
                    }
                }else{
                    UImsgs.showCustomToast(context,"Sorry! We are closed",INFO);
                }


            }
        });

        holder.vendor_view_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(vendorsPojo.getAvailability()==1) {

                    if (cartDBHelper.numberOfFoodCartRows() > 0) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Cart Alert");
                        alertDialogBuilder
                                .setMessage("You have some cart items. Would you like to check or clear")
                                .setCancelable(false)
                                .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        cartDBHelper.deleteCartTable();
                                        UImsgs.showToast(context, "Cart cleared successfully");
                                    }
                                })
                                .setNegativeButton("Check", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(context, CartActivity.class);
                                        intent.putExtra("Type", "Food");
                                        context.startActivity(intent);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        AddToCartActivity.vendorId = (vendorsPojo.getVendor_user_id());
                        profileFetcher(vendorsPojo.getId());
                    }
                }else{
                    UImsgs.showCustomToast(context,"Sorry! We are closed",INFO);
                }


            }
        });

        if(vendorsPojo.getAvailability()==1){
            holder.vendor_status.setText("Open");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#008000"));
        }else{
            holder.vendor_status.setText("Closed");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#ff0000"));
        }



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView vendor_name, customer_address,vendor_landmark,vendor_email,vendor_view,vendor_view_shop,vendor_distance,vendor_status;
        ImageView vendor_image;
        LinearLayout item_layout;
        CardView vendor_status_card,vendor_view_shop_card;
       // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            vendor_image = itemView.findViewById(R.id.vendor_image);
            vendor_name = itemView.findViewById(R.id.vendor_name);
            customer_address = itemView.findViewById(R.id.customer_address);
            vendor_landmark = itemView.findViewById(R.id.vendor_landmark);
            vendor_email = itemView.findViewById(R.id.vendor_email);
            vendor_view = itemView.findViewById(R.id.vendor_view);

            vendor_view_shop = itemView.findViewById(R.id.vendor_view_shop);
            vendor_view = itemView.findViewById(R.id.vendor_view);
            vendor_distance = itemView.findViewById(R.id.vendor_distance);

            vendor_status = itemView.findViewById(R.id.vendor_status);
            vendor_status_card = itemView.findViewById(R.id.vendor_status_card);
            vendor_view_shop_card = itemView.findViewById(R.id.vendor_view_shop_card);
            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }


    private void profileFetcher(String vendor_id) {

        DialogOpener.dialogOpener(context);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        Log.d("url", INDIVIDUAL_VENDOR + vendor_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("vendor response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                        ArrayList<Object> leadVendorsList = new ArrayList<>();
                        Map<String, String> leadVendorsMapList = new HashMap<>();
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");

                            idStringVendor = dataObject.getString("vendor_user_id");
                            tempNameVendor = dataObject.getString("name");






                            try {
                                JSONObject category_object = dataObject.getJSONObject("category");
                                vendor_activity = category_object.getInt("status");
                                comingsoon_image_url = category_object.getString("coming_soon_image");
                                if (vendor_activity != 0) {
                                    Intent intent = new Intent(context, VendorsSubCategoriesActivity.class);
                                    intent.putExtra(reastaurantName, tempNameVendor);
                                    intent.putExtra(vendorUserId, idStringVendor);
                                    intent.putExtra(VENDOR_ID, vendor_id);

                                    context.startActivity(intent);
                    /*Intent intent = new Intent(getApplicationContext(), FoodActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    startActivity(intent);*/
                                } else {
                                    comingSoon(comingsoon_image_url);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }






                            }


                        }


                    DialogOpener.dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    //Coming soon
    private void comingSoon(String imageUrl) {
        TimeStamp timeStamp = new TimeStamp();
        ImageView comingsoonImage;
        LayoutInflater inflater = ((VendorsActivity)context).getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.coming_soon_layou, null);
        comingsoonImage = alertLayout.findViewById(R.id.comingsoon);
        Picasso.get()
                .load(imageUrl+timeStamp.timeStampFetcher())
                .into(comingsoonImage);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setView(alertLayout);
        alertDialog.show();
    }


}
