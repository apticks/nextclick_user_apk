package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.IndividualEcommProductActivity;
import com.apticks.nextclickuser.Pojo.ProductsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.utilities.Money;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {


    int count = 0;
    CartDBHelper cartDBHelper;

    List<ProductsPojo> data;
    LayoutInflater inflter;
    Context context;

    public ProductListAdapter(Context activity, List<ProductsPojo> products) {
        this.context = activity;
        this.data = products;
        cartDBHelper = new CartDBHelper(context);
    }


    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.ecommerceitemsupporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ProductListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListAdapter.ViewHolder holder, int position) {
        ProductsPojo productsPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        if (!productsPojo.getOffer_price().equals("0")){
            String sellCostString = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getOffer_price()
                    ))).toString()
                    + "      ";

            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getMrp()
                    ))).toString();

            String costString = sellCostString + buyMRP;

            holder.mrp.setText(costString, TextView.BufferType.SPANNABLE);


            Spannable spannable = (Spannable) holder.mrp.getText();
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            spannable.setSpan(boldSpan, 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                    costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }else {
            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getMrp()
                    ))).toString();
            holder.mrp.setText(buyMRP);

        }


        /*SpannableStringBuilder builder=new SpannableStringBuilder();
        SpannableString txtSpannable= new SpannableString(buyMRP);
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        txtSpannable.setSpan(boldSpan, 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpannable.setSpan(new StrikethroughSpan(), sellCostString.length(), costString.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(txtSpannable);
        builder.append(buyMRP);*/

        //holder.mrp.setText(builder, TextView.BufferType.SPANNABLE);



        //holder.mrp.setText(productsPojo.getMrp());
        //holder.offerprice.setText(productsPojo.getOffer_price());
        holder.name.setText(productsPojo.getName());
        Picasso.get().load(productsPojo.getImage()).into(holder.image);
        /*holder.displayLocation.setText(restaurants.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, mrp, offerprice;
        ImageView image;
        LinearLayout item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ecom_product_itemname);
            mrp = itemView.findViewById(R.id.ecom_product_itemMrpPrice);
            offerprice = itemView.findViewById(R.id.ecom_product_itemOfferPrice);
            image = itemView.findViewById(R.id.ecom_product_itemImage);
            item = itemView.findViewById(R.id.products_layout);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductsPojo productsPojo = data.get(getAdapterPosition());
                    Intent intent = new Intent(context, IndividualEcommProductActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("product_id",productsPojo.getId());
                    context.startActivity(intent);
                }
            });




        }
    }

}