package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apticks.nextclickuser.Pojo.EcomCategories;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class EcomCategoriesAdapter extends ArrayAdapter {

    List<EcomCategories> data;

    LayoutInflater inflter;
    Context context;
    LinearLayout ecomcategoryLayout;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;




    public EcomCategoriesAdapter(@NonNull Context context, int resource,List<EcomCategories> completData) {
        super(context, resource,completData);
        this.context = context;
        this.data = completData;
    }


    /*@NonNull
    @Override
    public EcomCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fashionitem, parent, false);

        return new EcomCategoriesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EcomCategoriesAdapter.ViewHolder holder, int position) {
        EcomCategories ecomCategories = dataArrayList.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.itemName.setText(ecomCategories.getName());
        Picasso.get().load(ecomCategories.getImage()).into(holder.itemImage);
        *//*holder.displayLocation.setText(displayCategory.getLocation());*//*


    }*/


    @Override
    public int getCount() {
        return data.size();
    }

    /*public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName, displayLocation;
        CircularImageView itemImage;
        LinearLayout listItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.fashionItemImage);
            itemName = itemView.findViewById(R.id.fashionItemName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });





        }
    }*/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_view_ecom_category, null);
        }

        /*if (position == mSelectedItem) {
            // set your color
        }*/



        TextView itemName, displayLocation;
        CircularImageView itemImage;
        //itemImage = v.findViewById(R.id.fashionItemImage);
        itemName = v.findViewById(R.id.fashion_category_item);
        EcomCategories ecomCategories = data.get(position);

        itemName.setText(ecomCategories.getName());
        //itemImage.setVisibility(View.GONE);
        //Picasso.get().load(ecomCategories.getImage()).into(itemImage);
        /*v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        return v;

    }
}