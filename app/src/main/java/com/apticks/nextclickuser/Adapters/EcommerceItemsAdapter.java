package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.EcommerceItemsPojo;
import com.apticks.nextclickuser.R;

import java.util.List;

public class EcommerceItemsAdapter extends RecyclerView.Adapter<EcommerceItemsAdapter.ViewHolder> {

    List<EcommerceItemsPojo> data;

    LayoutInflater inflter;
    Context context;

    public EcommerceItemsAdapter(Context activity, List<EcommerceItemsPojo> listRecentHostel) {
        this.context = activity;
        this.data = listRecentHostel;
    }


    @NonNull
    @Override
    public EcommerceItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fooditemsupporter_grid, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new EcommerceItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EcommerceItemsAdapter.ViewHolder holder, int position) {
        EcommerceItemsPojo ecommerceItemsPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.itemName.setText(ecommerceItemsPojo.getName());
        holder.itemPrice.setText(ecommerceItemsPojo.getPrice());
        //holder.itemName.setText(ecommerceItemsPojo.getName());
        holder.itemType.setText(ecommerceItemsPojo.getType());
        //holder.itemName.setText(ecommerceItemsPojo.getName());
       // Picasso.get().load(ecommerceItemsPojo.getImage()).into(holder.itemImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName,itemPrice,add,itemType;
        ImageView itemImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.fooditemImage);
            itemName = itemView.findViewById(R.id.fooditemname);
            itemPrice = itemView.findViewById(R.id.fooditemPrice);
            //itemType = itemView.findViewById(R.id.itemtype);
            add = itemView.findViewById(R.id.addfooditem);


       /* cardViewRecentAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecentlyAddedHostelModelData recentlyAddedHostelModelData = dataArrayList.get(getAdapterPosition());
                String tempId = recentlyAddedHostelModelData.getId();

                Intent intent = new Intent(context, SingleViewHostelActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("hostelId",tempId);

                context.startActivity(intent);
            }
        });*/


        }
    }
}

