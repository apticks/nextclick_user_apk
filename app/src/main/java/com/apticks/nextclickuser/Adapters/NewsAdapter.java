package com.apticks.nextclickuser.Adapters;

import android.content.Context;

import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.Activities.NEWS.NewsItemData;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Pojo.NewsData;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    List<NewsData> data;
    MediaController mc;

    LayoutInflater inflter;
    Context context;
    View itemView;
    public int LOCAL_NEWS = 1;
    public int MAIN_NEWS = 2;
    int type;
    String newsSubCatID;

    public NewsAdapter(Context activity, List<NewsData> completData, int type, String newsSubCatID) {
        this.context = activity;
        this.data = completData;
        this.type = type;
        this.newsSubCatID = newsSubCatID;
    }


    @NonNull
    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.newssupporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new NewsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.ViewHolder holder, int position) {
        NewsData newsData = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.title.setText(newsData.getTitle());

        holder.news.setText(Html.fromHtml(newsData.getNews()).toString());
        holder.date.setText(newsData.getTimes_ago());
        String timeago = newsData.getTimes_ago();
        String viewcount = newsData.getView_count();

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            mc = new MediaController(context);
            mc.setAnchorView(itemView);
        }*/
       // final MediaController controllers = new MediaController(context);
      /*  controllers.setAnchorView(holder.newsVideo);
        holder.newsVideo.setMediaController(controllers);

       *//* holder.newsVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                controllers.show();
            }
        });*//*
        //specify the location of media file
        Uri uri= Uri.parse(newsData.getVideo_link());
        holder.newsVideo.setVideoURI(uri);
        //holder.newsVideo.setMediaController(mc);
        holder.newsVideo.start();
*/
        /*holder.newsVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //controllers.show();
            }
        });
*/

        try{
            holder.news_view_count.setText(newsData.getView_count()+" view(s)");
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            TimeStamp timeStamp = new TimeStamp();
            Picasso.get().load(newsData.getImage()+ timeStamp.timeStampFetcher()).into(holder.newsImage);
        }catch (Exception e){
            e.printStackTrace();
        }
        //holder.displayLocation.setText(categoriesData.getLocation());
        holder.newsItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(type != 1) {*/
                    Intent intent = new Intent(context, NewsItemData.class);
                    intent.putExtra("id", newsData.getId());
                    intent.putExtra("img", newsData.getImage());
                    intent.putExtra("type", type+"");
                    intent.putExtra("newsSubCatID", newsSubCatID);
                    context.startActivity(intent);
                /*}*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, news,date,news_view_count;
        ImageView newsImage;
        LinearLayout newsItemLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.newstitle);
            news = itemView.findViewById(R.id.newsdata);
            date = itemView.findViewById(R.id.newsdate);
            newsImage = itemView.findViewById(R.id.newsimage);
            news_view_count = itemView.findViewById(R.id.news_view_count);
            newsItemLayout = itemView.findViewById(R.id.news_item_layout);





        }
    }
}

