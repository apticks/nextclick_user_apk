package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apticks.nextclickuser.Pojo.SubSubCategoryPojo;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class SubSubCategoriesAdapter  extends ArrayAdapter {

    List<SubSubCategoryPojo> data;

    LayoutInflater inflter;
    Context context;





    public SubSubCategoriesAdapter(@NonNull Context context, int resource, List<SubSubCategoryPojo> completData) {
        super(context, resource,completData);
        this.context = context;
        this.data = completData;
    }




    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.sub_sub_category_supporter, null);
        }


        TextView itemName, displayLocation;
        CircularImageView itemImage;
        //itemImage = v.findViewById(R.id.fashionItemImage);
        itemName = v.findViewById(R.id.sub_sub_categroy_item);
        SubSubCategoryPojo subSubCategoryPojo = data.get(position);

        itemName.setText(subSubCategoryPojo.getName());
        //Picasso.get().load(foodMenuPojo.getImage()).into(itemImage);


        return v;

    }
}
