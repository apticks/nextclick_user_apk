package com.apticks.nextclickuser.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.GetAppointmentAndGetServiceActivity;
import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.ServiceTiming;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.interfaces.SelectedServiceTimingsCallBack;

import java.util.LinkedList;

public class ServiceTimingsAdapter extends RecyclerView.Adapter<ServiceTimingsAdapter.ViewHolder> {

    private Context context;
    private int selectedServiceTimeId = 0;
    private LinkedList<ServiceTiming> listOfServiceTimings;

    private int selectedPosition = -1;

    public ServiceTimingsAdapter(Context context, LinkedList<ServiceTiming> listOfServiceTimings, int selectedServiceTimeId) {
        this.context = context;
        this.listOfServiceTimings = listOfServiceTimings;
        this.selectedServiceTimeId = selectedServiceTimeId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_service_timings_items, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ServiceTiming serviceTiming = listOfServiceTimings.get(position);
        int serviceTimeId = serviceTiming.getId();
        String endTime = serviceTiming.getEndTime();
        String startTime = serviceTiming.getStartTime();

        holder.tvServiceTimings.setText(startTime + " to " + endTime);

        if (selectedServiceTimeId != -1) {
            if (serviceTimeId == selectedServiceTimeId) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfServiceTimings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private CheckBox checkBox;
        private TextView tvServiceTimings;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.checkBox);
            tvServiceTimings = itemView.findViewById(R.id.tvServiceTimings);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ServiceTiming serviceTiming = listOfServiceTimings.get(getLayoutPosition());
            if (isChecked) {
                selectedServiceTimeId = serviceTiming.getId();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }

            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedServiceTimeId = -1;
                    selectedPosition = -1;
                }
            }

            SelectedServiceTimingsCallBack selectedServiceTimingsCallBack = (SelectedServiceTimingsCallBack) context;
            selectedServiceTimingsCallBack.selectedServiceTimings(selectedServiceTimeId);
        }
    }
}
