package com.apticks.nextclickuser.Adapters.EcommAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.apticks.nextclickuser.R;

public class EcommDescExpandableListAdapter extends BaseExpandableListAdapter {
    String descParent,descChild;
    private LayoutInflater inflater;
    Context mContext;

    public EcommDescExpandableListAdapter(Context activity, String descParent, String descChild) {
        this.descParent = descParent;
        this.descChild = descChild;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = activity;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ViewHolderParent viewHolderParent;
        if (convertView == null) {


            convertView = inflater.inflate(R.layout.ecomm_single_desc_parent_expand, null);

            viewHolderParent = new ViewHolderParent();

            viewHolderParent.parentExpand = convertView.findViewById(R.id.paren_expand);
            convertView.setTag(viewHolderParent);
        } else {
            viewHolderParent = (ViewHolderParent) convertView.getTag();
        }
        viewHolderParent.parentExpand.setText(descParent);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ViewHolderChild viewHolderChild;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ecomm_single_desc_child_expand, null);
            viewHolderChild = new ViewHolderChild();

            viewHolderChild.childExpand = convertView.findViewById(R.id.child_expand);
            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }
        viewHolderChild.childExpand.setText(descChild);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        //Toast.makeText(mContext, ""+groupPosition, Toast.LENGTH_SHORT).show();
    }

    private class ViewHolderParent {

        TextView parentExpand;
        CheckBox cbMainCategory;
        ImageView ivCategory;
    }

    private class ViewHolderChild {

        TextView childExpand;
        CheckBox cbSubCategory;
        View viewDivider;
    }
}
