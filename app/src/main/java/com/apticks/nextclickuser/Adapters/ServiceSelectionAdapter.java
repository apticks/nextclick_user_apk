package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityMainActivity;
import com.apticks.nextclickuser.Activities.OnDemandsActivities.OnDemandsMainActivity;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.ServicesAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;
import static com.apticks.nextclickuser.R.drawable.displayborder;

public class ServiceSelectionAdapter extends RecyclerView.Adapter<ServiceSelectionAdapter.ViewHolder> {

    List<ServicesPojo> data;
    String tempNameVendor,  idStringVendor,  vendor_id;

    LayoutInflater inflter;
    Context context;
    //String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzU5NjQxMTZ9.uHvvnPUPkWxxBM35nBwNN90m5Ci0h1nXCRmbUqdVvnQ";

    public ServiceSelectionAdapter(Context activity, List<ServicesPojo> servicesPojos, String tempNameVendor, String idStringVendor, String vendor_id) {
        this.context = activity;
        this.data = servicesPojos;
        this.tempNameVendor = tempNameVendor;
        this.idStringVendor = idStringVendor;
        this.vendor_id = vendor_id;


    }


    @NonNull
    @Override
    public ServiceSelectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.selection_service_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ServiceSelectionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceSelectionAdapter.ViewHolder holder, int position) {
        ServicesPojo servicesPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();


        //Picasso.get().load(currentBsVendor.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/

        holder.serviceName.setText(servicesPojo.getName());
        holder.serviceName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = null;
                if(servicesPojo.getId().equalsIgnoreCase("2")){

                    intent = new Intent(context, VendorsSubCategoriesActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    context.startActivity(intent);

                }else if(servicesPojo.getId().equalsIgnoreCase("8")) {
                    intent = new Intent(context, OnDemandsMainActivity.class);
                    /*intent.putExtra(reastaurantName, tempNameVendor);*/
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    context.startActivity(intent);
                }else if(servicesPojo.getId().equalsIgnoreCase("11")) {
                    intent = new Intent(context, HospitalityMainActivity.class);
                    /*intent.putExtra(reastaurantName, tempNameVendor);*/
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    context.startActivity(intent);
                }
                else {
                    UImsgs.showCustomToast(context,"Service will be available soon",INFO);
                }

            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            serviceName = itemView.findViewById(R.id.selection_service_supporter_name);



        }
    }
}