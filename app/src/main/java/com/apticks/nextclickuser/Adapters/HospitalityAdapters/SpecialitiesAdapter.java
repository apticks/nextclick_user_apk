package com.apticks.nextclickuser.Adapters.HospitalityAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityDoctorsActivity;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.SpecialityPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;

public class SpecialitiesAdapter extends RecyclerView.Adapter<SpecialitiesAdapter.ViewHolder> {

    private Context context;

    private List<SpecialityPojo> data;

    private String serviceId = "";

    public SpecialitiesAdapter(Context activity, List<SpecialityPojo> itemPojos, String serviceId) {
        this.context = activity;
        this.data = itemPojos;
        this.serviceId = serviceId;
    }


    @NonNull
    @Override
    public SpecialitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_hospitality_speciality, parent, false);
        return new SpecialitiesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SpecialitiesAdapter.ViewHolder holder, int position) {
        SpecialityPojo specialityPojo = data.get(position);

        holder.speciality.setText(specialityPojo.getName());
        Picasso.get()
                .load(specialityPojo.getImage())
                .placeholder(R.drawable.loader_gif)
                .into(holder.speciality_image);


        holder.speciality_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HospitalityDoctorsActivity.class);
                intent.putExtra(VENDOR_ID, specialityPojo.getCreated_user_id());
                intent.putExtra("spec_name", specialityPojo.getName());
                intent.putExtra("spec_id", specialityPojo.getId());
                intent.putExtra(context.getString(R.string.service_id), serviceId);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView speciality;
        ImageView speciality_image;
        LinearLayout speciality_item;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            speciality = itemView.findViewById(R.id.speciality);
            speciality_image = itemView.findViewById(R.id.speciality_image);
            speciality_item = itemView.findViewById(R.id.speciality_item);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }
}
