package com.apticks.nextclickuser.Adapters.BS_Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.Beauty_Spa.BS_Services_Activity;
import com.apticks.nextclickuser.Pojo.BSPojo.BSVendorsPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BSVendorAdapter extends RecyclerView.Adapter<BSVendorAdapter.ViewHolder> {

    List<BSVendorsPojo> data;

    LayoutInflater inflter;
    Context context;

    public BSVendorAdapter(Context activity, List<BSVendorsPojo> listRecentHostel) {
        this.context = activity;
        this.data = listRecentHostel;
    }


    @NonNull
    @Override
    public BSVendorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.bs_vendor_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new BSVendorAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BSVendorAdapter.ViewHolder holder, int position) {
        BSVendorsPojo currentBsVendor = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(currentBsVendor.getName());
        Picasso.get().load(currentBsVendor.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/




    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName,view;
        ImageView displayImage;
        LinearLayout item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.bs_vendor_image);
            displayName = itemView.findViewById(R.id.bs_vendor_name);
            view = itemView.findViewById(R.id.bs_view);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, BS_Services_Activity.class);
                    intent.putExtra("vendor_id",data.get(getAdapterPosition()).getVendor_user_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });



        }
    }
}

