package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.ReceiptItemPojo;
import com.apticks.nextclickuser.R;

import java.util.List;

public class ReceiptItemsAdapter extends RecyclerView.Adapter<ReceiptItemsAdapter.ViewHolder> {

    List<ReceiptItemPojo> data;

    LayoutInflater inflter;
    Context context;
    View itemView;

    public ReceiptItemsAdapter(Context activity, List<ReceiptItemPojo> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public ReceiptItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.receipt_item_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ReceiptItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceiptItemsAdapter.ViewHolder holder, int position) {
        ReceiptItemPojo receiptItem = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.receipt_item_sno.setText(receiptItem.getSno()+".");
        holder.receipt_item_name.setText(receiptItem.getItem_name());
        holder.receipt_item_qty.setText(receiptItem.getQty()+"");
        holder.receipt_item_value.setText(receiptItem.getValue()+"");


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView receipt_item_sno,receipt_item_name,receipt_item_qty,receipt_item_value;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            receipt_item_sno = itemView.findViewById(R.id.receipt_item_sno);
            receipt_item_name = itemView.findViewById(R.id.receipt_item_name);
            receipt_item_qty = itemView.findViewById(R.id.receipt_item_qty);
            receipt_item_value = itemView.findViewById(R.id.receipt_item_value);



       /* cardViewRecentAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecentlyAddedHostelModelData recentlyAddedHostelModelData = dataArrayList.get(getAdapterPosition());
                String tempId = recentlyAddedHostelModelData.getId();

                Intent intent = new Intent(context, SingleViewHostelActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("hostelId",tempId);

                context.startActivity(intent);
            }
        });*/



        }
    }
}

