package com.apticks.nextclickuser.utils;

import android.content.Context;
import androidx.fragment.app.Fragment;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.Pojo.getServicesResponse.GetServiceDetails;

import java.util.ArrayList;
import java.util.LinkedList;

public class UserData {


    private static UserData userData = null;

    private Context context;
    private Fragment fragment;

    private ArrayList<ServicesPojo> servicesList;
    private LinkedList<GetServiceDetails> listOfGetServiceDetails;

    private UserData() {
    }

    public static UserData getInstance() {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public ArrayList<ServicesPojo> getServicesList() {
        return servicesList;
    }

    public void setServicesList(ArrayList<ServicesPojo> servicesList) {
        this.servicesList = servicesList;
    }

    public LinkedList<GetServiceDetails> getListOfGetServiceDetails() {
        return listOfGetServiceDetails;
    }

    public void setListOfGetServiceDetails(LinkedList<GetServiceDetails> listOfGetServiceDetails) {
        this.listOfGetServiceDetails = listOfGetServiceDetails;
    }
}
