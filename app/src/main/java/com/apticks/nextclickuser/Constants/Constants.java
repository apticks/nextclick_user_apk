package com.apticks.nextclickuser.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public interface Constants {


    String USER_TOKEN ="user_token";
    String AUTH_TOKEN ="X_AUTH_TOKEN";
    String FCM_TOKEN ="fcm_token";
    String CONNECTION ="connection";
    String CHARGE ="subscription_charge";
    String APP_ID = "APP_id";
    String APP_ID_VALUE = "TVE9PQ==";
    String UNIQUE_ID ="unique_id";
    String WALLET ="wallet";
    String EMAIL ="email";
    String MOBILE ="mobile";
    String NAME ="name";


    public static final String IS_CHECKED = "is_checked";
    public static final String SUB_CATEGORY_NAME = "sub_category_name";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_ID = "category_id";
    public static final String SUB_ID = "sub_id";
    public static final String PAYMENT = "Payment";
    public static final String CAT_ID = "cat_id";// For using Intent Put and Get Extra KEY checking
    public static final String VENDOR_ID = "id";// For using Intent Put and Get Extra KEY checking
    public static final String reastaurantName = "nameReastaurant";// For using Intent Put and Get Extra KEY checking
    public static final String vendorUserId = "vendor_user_id";// For using Intent Put and Get Extra KEY checking




    public static final String CHECK_BOX_CHECKED_TRUE = "YES";
    public static final String CHECK_BOX_CHECKED_FALSE = "NO";

    public static ArrayList<ArrayList<HashMap<String, String>>> childItems = new ArrayList<>();
    public static ArrayList<HashMap<String, String>> parentItems = new ArrayList<>();


    public static String SUCCESS= "S";
    public static String WARNING= "W";
    public static String ERROR= "E";
    public static String INFO= "I";
    public static String NORMAL= "N";


}
