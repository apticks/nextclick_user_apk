package com.apticks.nextclickuser.Constants;

public interface CategoryIDS {
    final String groceriesCategoryId = "1";
    final String cabsCategoryId = "13";
    final String medicinesCategoryId = "2";
    final String beautyCategoryId = "4";
    final String foodCategoryId = "6";
    final String hospitalCategoryId = "3";
    final String agricultureCategoryId = "5";
    final String eventsCategoryId = "20";
    final String onDemandsCategoryId = "8";
    final String busTicketsCategoryId = "12";
    final String courierCategoryId = "14";
    final String hiringCategoryId = "24";
    final String eventMgmntCategoryId = "20";
    final String autoMobilesCategoryId = "51";
    final String packersMoversCategoryId = "45";
    final String diagnosticsCategoryId = "16";
    final String flowersCategoryId = "22";
    final String cabServicesCategoryId = "13";


    public int manageaccount_service_id=2;
    public int lead_management_service_id=4;



}
